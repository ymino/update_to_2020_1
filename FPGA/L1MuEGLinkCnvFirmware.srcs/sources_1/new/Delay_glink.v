`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2017/02/13 11:01:19
// Design Name: 
// Module Name: Delay_glink
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Delay_glink(
    input wire CLK_in,
    input wire [20:0] data_in,
    input wire [7:0] delaynum_in,
    input wire reset_in,
    output wire [20:0] data_out
    );


wire wr_enb=1'b1;
reg [6:0] wr_addr = 7'b0;
reg [20:0] dina_pos;
reg [20:0] dina_neg;
wire [20:0] dina;

reg [6:0] rd_addr = 7'b0;
wire [20:0] doutb;
assign dina = (delaynum_in[0]) ? dina_pos : dina_neg;
assign data_out = doutb;

always @ (posedge CLK_in) begin
    if (reset_in) begin
        wr_addr <= 7'b1 + (delaynum_in)/2;
        rd_addr <= 7'b0;        
    end
    else begin
        wr_addr <= wr_addr + 7'b1;
        rd_addr <= rd_addr + 7'b1;    
    end
end

always @ (posedge CLK_in) begin
    if (reset_in) begin
    end
    else begin
        dina_pos <= data_in;
    end
end

always @ (negedge CLK_in) begin
    if (reset_in) begin
    end
    else begin
        dina_neg <= data_in;
    end
end


delay_mem_glink delay_mem_glink (
  .clka(CLK_in),    // input wire clka
  .wea(wr_enb),      // input wire [0 : 0] wea
  .addra(wr_addr),  // input wire [6 : 0] addra
  .dina(dina),    // input wire [20 : 0] dina
  .clkb(CLK_in),    // input wire clkb
  .rstb(reset_in),    // input wire rstb
  .addrb(rd_addr),  // input wire [6 : 0] addrb
  .doutb(doutb)  // output wire [20 : 0] doutb
);

endmodule
