`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/07/11 21:31:37
// Design Name: 
// Module Name: Monitoring
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Monitoring(
    input wire CLK_in,
    input wire [255:0] data_in,
    input wire RST_in,
    input wire write_enb,
    input wire read_enb,    
    output wire [255:0] FIFO_data_out
    );
    /////////// Monitoring FIFO ////////////////////
    
    // 160 bit RX data is put into 256bit -> 256 bit FIFO.
    // write enable and read enable signals are controlled by VME (comment by akatsuka)
    
    wire wr_en;
    wire rd_en;
    wire full;
    wire almost_full;
    wire empty;

   //FIFO WRITE ENABLE
     wire FIFO_read_enb;
     reg [2:0] shift_reg;
     reg read_enb_reg;
     assign FIFO_read_enb = (!shift_reg[2] && shift_reg[1]);
     always @(posedge CLK_in) begin
         shift_reg[2:0] <= {shift_reg[1:0], read_enb};
     end
  
    assign wr_en = write_enb && !almost_full;
    assign rd_en = FIFO_read_enb && !empty;

    
    monitoring_fifo monitoring_fifo(
      .clk(CLK_in),                  // inpsut wire clk
      .srst(RST_in),                // input wire srst
      .din(data_in),                  // input wire [255 : 0] din
      .wr_en(wr_en),              // input wire wr_en
      .rd_en(rd_en),              // input wire rd_en
      .dout(FIFO_data_out),                // output wire [255 : 0] dout
      .full(full),                // output wire full
      .almost_full(almost_full),  // output wire almost_full
      .empty(empty)              // output wire empty
    );
endmodule
