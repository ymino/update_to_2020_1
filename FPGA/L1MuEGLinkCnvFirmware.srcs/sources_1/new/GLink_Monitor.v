`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2018/08/06 13:37:56
// Design Name: 
// Module Name: GLink_Monitor
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module GLink_Monitor(
    input wire CLK_in,
    input wire Reset_in,
    input wire Reset_GLinkMonitor_in,
    
    input wire [3:0] GL0_monitor_in,
    input wire [3:0] GL1_monitor_in,
    input wire [3:0] GL2_monitor_in,
    input wire [3:0] GL3_monitor_in,
    input wire [3:0] GL4_monitor_in,
    input wire [3:0] GL5_monitor_in,
    input wire [3:0] GL6_monitor_in,
    input wire [3:0] GL7_monitor_in,
    input wire [3:0] GL8_monitor_in,
    input wire [3:0] GL9_monitor_in,
    input wire [3:0] GL10_monitor_in,
    input wire [3:0] GL11_monitor_in,
    input wire [3:0] GL12_monitor_in,
    input wire [3:0] GL13_monitor_in,
    
    input wire GL0_State_Reset_in, // from VME
    input wire GL1_State_Reset_in, // from VME
    input wire GL2_State_Reset_in, // from VME
    input wire GL3_State_Reset_in, // from VME
    input wire GL4_State_Reset_in, // from VME
    input wire GL5_State_Reset_in, // from VME
    input wire GL6_State_Reset_in, // from VME
    input wire GL7_State_Reset_in, // from VME
    input wire GL8_State_Reset_in, // from VME
    input wire GL9_State_Reset_in, // from VME
    input wire GL10_State_Reset_in, // from VME
    input wire GL11_State_Reset_in, // from VME
    input wire GL12_State_Reset_in, // from VME
    input wire GL13_State_Reset_in, // from VME
    
    input wire GL0_ErrorCounter_Reset_in, // from VME    
    input wire GL1_ErrorCounter_Reset_in, // from VME    
    input wire GL2_ErrorCounter_Reset_in, // from VME    
    input wire GL3_ErrorCounter_Reset_in, // from VME    
    input wire GL4_ErrorCounter_Reset_in, // from VME    
    input wire GL5_ErrorCounter_Reset_in, // from VME    
    input wire GL6_ErrorCounter_Reset_in, // from VME    
    input wire GL7_ErrorCounter_Reset_in, // from VME    
    input wire GL8_ErrorCounter_Reset_in, // from VME    
    input wire GL9_ErrorCounter_Reset_in, // from VME    
    input wire GL10_ErrorCounter_Reset_in, // from VME    
    input wire GL11_ErrorCounter_Reset_in, // from VME    
    input wire GL12_ErrorCounter_Reset_in, // from VME    
    input wire GL13_ErrorCounter_Reset_in, // from VME     
    
    input wire RX_DIVen_in, //RXDIV from VME
    input wire [1:0] GL0_RX_DIV_vme_in, // Set value of RX_DIV from VME 
    input wire [1:0] GL1_RX_DIV_vme_in, // Set value of RX_DIV from VME 
    input wire [1:0] GL2_RX_DIV_vme_in, // Set value of RX_DIV from VME 
    input wire [1:0] GL3_RX_DIV_vme_in, // Set value of RX_DIV from VME 
    input wire [1:0] GL4_RX_DIV_vme_in, // Set value of RX_DIV from VME 
    input wire [1:0] GL5_RX_DIV_vme_in, // Set value of RX_DIV from VME 
    input wire [1:0] GL6_RX_DIV_vme_in, // Set value of RX_DIV from VME 
    input wire [1:0] GL7_RX_DIV_vme_in, // Set value of RX_DIV from VME 
    input wire [1:0] GL8_RX_DIV_vme_in, // Set value of RX_DIV from VME 
    input wire [1:0] GL9_RX_DIV_vme_in, // Set value of RX_DIV from VME 
    input wire [1:0] GL10_RX_DIV_vme_in, // Set value of RX_DIV from VME 
    input wire [1:0] GL11_RX_DIV_vme_in, // Set value of RX_DIV from VME 
    input wire [1:0] GL12_RX_DIV_vme_in, // Set value of RX_DIV from VME 
    input wire [1:0] GL13_RX_DIV_vme_in, // Set value of RX_DIV from VME 
     
    output wire [15:0] GL0_State_out,
    output wire [15:0] GL1_State_out,
    output wire [15:0] GL2_State_out,
    output wire [15:0] GL3_State_out,
    output wire [15:0] GL4_State_out,
    output wire [15:0] GL5_State_out,
    output wire [15:0] GL6_State_out,
    output wire [15:0] GL7_State_out,
    output wire [15:0] GL8_State_out,
    output wire [15:0] GL9_State_out,
    output wire [15:0] GL10_State_out,
    output wire [15:0] GL11_State_out,
    output wire [15:0] GL12_State_out,
    output wire [15:0] GL13_State_out,
    
    output wire [15:0] GL0_ErrorCounter_out,  
    output wire [15:0] GL1_ErrorCounter_out,  
    output wire [15:0] GL2_ErrorCounter_out,  
    output wire [15:0] GL3_ErrorCounter_out,  
    output wire [15:0] GL4_ErrorCounter_out,  
    output wire [15:0] GL5_ErrorCounter_out,  
    output wire [15:0] GL6_ErrorCounter_out,  
    output wire [15:0] GL7_ErrorCounter_out,  
    output wire [15:0] GL8_ErrorCounter_out,  
    output wire [15:0] GL9_ErrorCounter_out,  
    output wire [15:0] GL10_ErrorCounter_out,  
    output wire [15:0] GL11_ErrorCounter_out,  
    output wire [15:0] GL12_ErrorCounter_out,  
    output wire [15:0] GL13_ErrorCounter_out,  
    output wire [1:0] GLinkOK_out,
    
    output wire [13:0] RX_DIV0_out,
    output wire [13:0] RX_DIV1_out

    );
    
wire RST_in = Reset_in || Reset_GLinkMonitor_in;    

wire GL0_OK, GL1_OK, GL2_OK, GL3_OK,
      GL4_OK, GL5_OK, GL6_OK, GL7_OK,
      GL8_OK, GL9_OK, GL10_OK, GL11_OK,
      GL12_OK, GL13_OK;  
      
//wire [3:0] EIFI_Mask; for Run2
// Do we need the masks? EI coincidence can be disabled when EI signals are not used.
        
assign GLinkOK_out[0] = GL0_OK & GL1_OK & GL2_OK & GL3_OK & GL4_OK & GL5_OK & GL6_OK; //Sector0 GlinkOK 
assign GLinkOK_out[1] = GL7_OK & GL8_OK & GL9_OK & GL10_OK & GL11_OK & GL11_OK & GL12_OK & GL13_OK;//Sector1 GlinkOK 
// Run2
//assign GlinkOK[0]={GlinkOK0&GlinkOK1&GlinkOK2&GlinkOK3&GlinkOK4&GlinkOK5}&{!EIFI_Mask[3]|GlinkOK15}&{!EIFI_Mask[2]|GlinkOK14}&{!EIFI_Mask[1]|GlinkOK13}&{!EIFI_Mask[0]|GlinkOK12}; //Sector0 GlinkOK Use EIFI ver06
//assign GlinkOK[1]={GlinkOK6&GlinkOK7&GlinkOK8&GlinkOK9&GlinkOK10&GlinkOK11}&{!EIFI_Mask[3]|GlinkOK15}&{!EIFI_Mask[2]|GlinkOK14}&{!EIFI_Mask[1]|GlinkOK13}&{!EIFI_Mask[0]|GlinkOK12};//Sector1 GlinkOK Use EIFI ver06

    
GLinkChecker GLinkChecker0(
        .CLK_in(CLK_in),
        .RESET_in(RST_in),
        .RXDATA_in(GL0_monitor_in[0]), 
        .RXCNTL_in(GL0_monitor_in[1]),
        .RXERROR_in(GL0_monitor_in[3]),
        .RXREADY_in(GL0_monitor_in[2]),
        .StateRESET_in(GL0_State_Reset_in),
        .RX_DIV_vme_in(GL0_RX_DIV_vme_in),
        .RX_DIVen_in(RX_DIVen_in),
        .ErrorCounterReset_in(GL0_ErrorCounter_Reset_in),        
        
        .GlinkState_out(GL0_State_out),
        .ErrorCounter_out(GL0_ErrorCounter_out),
        .GlinkOK_out(GL0_OK), // to Decorder       
        .RX_DIV_out({RX_DIV1_out[0], RX_DIV0_out[0]})        
       // .DebugMode(DebugMode)
        );

GLinkChecker GLinkChecker1(
        .CLK_in(CLK_in),
        .RESET_in(RST_in),
        .RXDATA_in(GL1_monitor_in[0]), 
        .RXCNTL_in(GL1_monitor_in[1]),
        .RXERROR_in(GL1_monitor_in[3]),
        .RXREADY_in(GL1_monitor_in[2]),
        .StateRESET_in(GL1_State_Reset_in),
        .RX_DIV_vme_in(GL1_RX_DIV_vme_in),
        .RX_DIVen_in(RX_DIVen_in),
        .ErrorCounterReset_in(GL1_ErrorCounter_Reset_in),        
        
        .GlinkState_out(GL1_State_out),
        .ErrorCounter_out(GL1_ErrorCounter_out),
        .GlinkOK_out(GL1_OK), // to Decorder       
        .RX_DIV_out({RX_DIV1_out[1], RX_DIV0_out[1]})        
       // .DebugMode(DebugMode)
        );   

GLinkChecker GLinkChecker2(
        .CLK_in(CLK_in),
        .RESET_in(RST_in),
        .RXDATA_in(GL2_monitor_in[0]), 
        .RXCNTL_in(GL2_monitor_in[1]),
        .RXERROR_in(GL2_monitor_in[3]),
        .RXREADY_in(GL2_monitor_in[2]),
        .StateRESET_in(GL2_State_Reset_in),
        .RX_DIV_vme_in(GL2_RX_DIV_vme_in),
        .RX_DIVen_in(RX_DIVen_in),
        .ErrorCounterReset_in(GL2_ErrorCounter_Reset_in),        
        
        .GlinkState_out(GL2_State_out),
        .ErrorCounter_out(GL2_ErrorCounter_out),
        .GlinkOK_out(GL2_OK), // to Decorder       
        .RX_DIV_out({RX_DIV1_out[2], RX_DIV0_out[2]})        
       // .DebugMode(DebugMode)
        );

GLinkChecker GLinkChecker3(
        .CLK_in(CLK_in),
        .RESET_in(RST_in),
        .RXDATA_in(GL3_monitor_in[0]), 
        .RXCNTL_in(GL3_monitor_in[1]),
        .RXERROR_in(GL3_monitor_in[3]),
        .RXREADY_in(GL3_monitor_in[2]),
        .StateRESET_in(GL3_State_Reset_in),
        .RX_DIV_vme_in(GL3_RX_DIV_vme_in),
        .RX_DIVen_in(RX_DIVen_in),
        .ErrorCounterReset_in(GL3_ErrorCounter_Reset_in),        
        
        .GlinkState_out(GL3_State_out),
        .ErrorCounter_out(GL3_ErrorCounter_out),
        .GlinkOK_out(GL3_OK), // to Decorder       
        .RX_DIV_out({RX_DIV1_out[3], RX_DIV0_out[3]})        
       // .DebugMode(DebugMode)
        );        

GLinkChecker GLinkChecker4(
        .CLK_in(CLK_in),
        .RESET_in(RST_in),
        .RXDATA_in(GL4_monitor_in[0]), 
        .RXCNTL_in(GL4_monitor_in[1]),
        .RXERROR_in(GL4_monitor_in[3]),
        .RXREADY_in(GL4_monitor_in[2]),
        .StateRESET_in(GL4_State_Reset_in),
        .RX_DIV_vme_in(GL4_RX_DIV_vme_in),
        .RX_DIVen_in(RX_DIVen_in),
        .ErrorCounterReset_in(GL4_ErrorCounter_Reset_in),        
        
        .GlinkState_out(GL4_State_out),
        .ErrorCounter_out(GL4_ErrorCounter_out),
        .GlinkOK_out(GL4_OK), // to Decorder       
        .RX_DIV_out({RX_DIV1_out[4], RX_DIV0_out[4]})        
       // .DebugMode(DebugMode)
        );     

GLinkChecker GLinkChecker5(
        .CLK_in(CLK_in),
        .RESET_in(RST_in),
        .RXDATA_in(GL5_monitor_in[0]), 
        .RXCNTL_in(GL5_monitor_in[1]),
        .RXERROR_in(GL5_monitor_in[3]),
        .RXREADY_in(GL5_monitor_in[2]),
        .StateRESET_in(GL5_State_Reset_in),
        .RX_DIV_vme_in(GL5_RX_DIV_vme_in),
        .RX_DIVen_in(RX_DIVen_in),
        .ErrorCounterReset_in(GL5_ErrorCounter_Reset_in),        
        
        .GlinkState_out(GL5_State_out),
        .ErrorCounter_out(GL5_ErrorCounter_out),
        .GlinkOK_out(GL5_OK), // to Decorder       
        .RX_DIV_out({RX_DIV1_out[5], RX_DIV0_out[5]})        
       // .DebugMode(DebugMode)
        );  

GLinkChecker GLinkChecker6(
        .CLK_in(CLK_in),
        .RESET_in(RST_in),
        .RXDATA_in(GL6_monitor_in[0]), 
        .RXCNTL_in(GL6_monitor_in[1]),
        .RXERROR_in(GL6_monitor_in[3]),
        .RXREADY_in(GL6_monitor_in[2]),
        .StateRESET_in(GL6_State_Reset_in),
        .RX_DIV_vme_in(GL6_RX_DIV_vme_in),
        .RX_DIVen_in(RX_DIVen_in),
        .ErrorCounterReset_in(GL6_ErrorCounter_Reset_in),        
        
        .GlinkState_out(GL6_State_out),
        .ErrorCounter_out(GL6_ErrorCounter_out),
        .GlinkOK_out(GL6_OK), // to Decorder       
        .RX_DIV_out({RX_DIV1_out[6], RX_DIV0_out[6]})        
       // .DebugMode(DebugMode)
        );  

GLinkChecker GLinkChecker7(
        .CLK_in(CLK_in),
        .RESET_in(RST_in),
        .RXDATA_in(GL7_monitor_in[0]), 
        .RXCNTL_in(GL7_monitor_in[1]),
        .RXERROR_in(GL7_monitor_in[3]),
        .RXREADY_in(GL7_monitor_in[2]),
        .StateRESET_in(GL7_State_Reset_in),
        .RX_DIV_vme_in(GL7_RX_DIV_vme_in),
        .RX_DIVen_in(RX_DIVen_in),
        .ErrorCounterReset_in(GL7_ErrorCounter_Reset_in),        
        
        .GlinkState_out(GL7_State_out),
        .ErrorCounter_out(GL7_ErrorCounter_out),
        .GlinkOK_out(GL7_OK), // to Decorder       
        .RX_DIV_out({RX_DIV1_out[7], RX_DIV0_out[7]})        
       // .DebugMode(DebugMode)
        );

GLinkChecker GLinkChecker8(
        .CLK_in(CLK_in),
        .RESET_in(RST_in),
        .RXDATA_in(GL8_monitor_in[0]), 
        .RXCNTL_in(GL8_monitor_in[1]),
        .RXERROR_in(GL8_monitor_in[3]),
        .RXREADY_in(GL8_monitor_in[2]),
        .StateRESET_in(GL8_State_Reset_in),
        .RX_DIV_vme_in(GL8_RX_DIV_vme_in),
        .RX_DIVen_in(RX_DIVen_in),
        .ErrorCounterReset_in(GL8_ErrorCounter_Reset_in),        
        
        .GlinkState_out(GL8_State_out),
        .ErrorCounter_out(GL8_ErrorCounter_out),
        .GlinkOK_out(GL8_OK), // to Decorder       
        .RX_DIV_out({RX_DIV1_out[8], RX_DIV0_out[8]})        
       // .DebugMode(DebugMode)
        ); 

GLinkChecker GLinkChecker9(
        .CLK_in(CLK_in),
        .RESET_in(RST_in),
        .RXDATA_in(GL9_monitor_in[0]), 
        .RXCNTL_in(GL9_monitor_in[1]),
        .RXERROR_in(GL9_monitor_in[3]),
        .RXREADY_in(GL9_monitor_in[2]),
        .StateRESET_in(GL9_State_Reset_in),
        .RX_DIV_vme_in(GL9_RX_DIV_vme_in),
        .RX_DIVen_in(RX_DIVen_in),
        .ErrorCounterReset_in(GL9_ErrorCounter_Reset_in),        
        
        .GlinkState_out(GL9_State_out),
        .ErrorCounter_out(GL9_ErrorCounter_out),
        .GlinkOK_out(GL9_OK), // to Decorder       
        .RX_DIV_out({RX_DIV1_out[9], RX_DIV0_out[9]})        
       // .DebugMode(DebugMode)
        );

GLinkChecker GLinkChecker10(
        .CLK_in(CLK_in),
        .RESET_in(RST_in),
        .RXDATA_in(GL10_monitor_in[0]), 
        .RXCNTL_in(GL10_monitor_in[1]),
        .RXERROR_in(GL10_monitor_in[3]),
        .RXREADY_in(GL10_monitor_in[2]),
        .StateRESET_in(GL10_State_Reset_in),
        .RX_DIV_vme_in(GL10_RX_DIV_vme_in),
        .RX_DIVen_in(RX_DIVen_in),
        .ErrorCounterReset_in(GL10_ErrorCounter_Reset_in),        
        
        .GlinkState_out(GL10_State_out),
        .ErrorCounter_out(GL10_ErrorCounter_out),
        .GlinkOK_out(GL10_OK), // to Decorder       
        .RX_DIV_out({RX_DIV1_out[10], RX_DIV0_out[10]})        
       // .DebugMode(DebugMode)
        );

GLinkChecker GLinkChecker11(
        .CLK_in(CLK_in),
        .RESET_in(RST_in),
        .RXDATA_in(GL11_monitor_in[0]), 
        .RXCNTL_in(GL11_monitor_in[1]),
        .RXERROR_in(GL11_monitor_in[3]),
        .RXREADY_in(GL11_monitor_in[2]),
        .StateRESET_in(GL11_State_Reset_in),
        .RX_DIV_vme_in(GL11_RX_DIV_vme_in),
        .RX_DIVen_in(RX_DIVen_in),
        .ErrorCounterReset_in(GL11_ErrorCounter_Reset_in),        
        
        .GlinkState_out(GL11_State_out),
        .ErrorCounter_out(GL11_ErrorCounter_out),
        .GlinkOK_out(GL11_OK), // to Decorder       
        .RX_DIV_out({RX_DIV1_out[11], RX_DIV0_out[11]})        
       // .DebugMode(DebugMode)
        );

GLinkChecker GLinkChecker12(
        .CLK_in(CLK_in),
        .RESET_in(RST_in),
        .RXDATA_in(GL12_monitor_in[0]), 
        .RXCNTL_in(GL12_monitor_in[1]),
        .RXERROR_in(GL12_monitor_in[3]),
        .RXREADY_in(GL12_monitor_in[2]),
        .StateRESET_in(GL12_State_Reset_in),
        .RX_DIV_vme_in(GL12_RX_DIV_vme_in),
        .RX_DIVen_in(RX_DIVen_in),
        .ErrorCounterReset_in(GL12_ErrorCounter_Reset_in),        
        
        .GlinkState_out(GL12_State_out),
        .ErrorCounter_out(GL12_ErrorCounter_out),
        .GlinkOK_out(GL12_OK), // to Decorder       
        .RX_DIV_out({RX_DIV1_out[12], RX_DIV0_out[12]})        
       // .DebugMode(DebugMode)
        );

GLinkChecker GLinkChecker13(
        .CLK_in(CLK_in),
        .RESET_in(RST_in),
        .RXDATA_in(GL13_monitor_in[0]), 
        .RXCNTL_in(GL13_monitor_in[1]),
        .RXERROR_in(GL13_monitor_in[3]),
        .RXREADY_in(GL13_monitor_in[2]),
        .StateRESET_in(GL13_State_Reset_in),
        .RX_DIV_vme_in(GL13_RX_DIV_vme_in),
        .RX_DIVen_in(RX_DIVen_in),
        .ErrorCounterReset_in(GL13_ErrorCounter_Reset_in),        
        
        .GlinkState_out(GL13_State_out),
        .ErrorCounter_out(GL13_ErrorCounter_out),
        .GlinkOK_out(GL13_OK), // to Decorder       
        .RX_DIV_out({RX_DIV1_out[13], RX_DIV0_out[13]})        
       // .DebugMode(DebugMode)
        );
   
endmodule
