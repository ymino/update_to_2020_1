`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2018/07/05 16:56:17
// Design Name: 
// Module Name: TrackOutputGenerator
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module TrackOutputGenerator(

    input wire CLK,
    input wire [7:0] trackgenerator0_S0_ROI_in,
    input wire [7:0] trackgenerator1_S0_ROI_in,
    input wire [7:0] trackgenerator2_S0_ROI_in,
    input wire [7:0] trackgenerator3_S0_ROI_in, 
    input wire [3:0] trackgenerator0_S0_pT_in,
    input wire [3:0] trackgenerator1_S0_pT_in,
    input wire [3:0] trackgenerator2_S0_pT_in,
    input wire [3:0] trackgenerator3_S0_pT_in,
    input wire       trackgenerator0_S0_sign_in,
    input wire       trackgenerator1_S0_sign_in,
    input wire       trackgenerator2_S0_sign_in,
    input wire       trackgenerator3_S0_sign_in,
    input wire [2:0] trackgenerator0_S0_InnerCoincidenceFlag_in,
    input wire [2:0] trackgenerator1_S0_InnerCoincidenceFlag_in,
    input wire [2:0] trackgenerator2_S0_InnerCoincidenceFlag_in,
    input wire [2:0] trackgenerator3_S0_InnerCoincidenceFlag_in,       
    input wire [7:0] trackgenerator0_S1_ROI_in,
    input wire [7:0] trackgenerator1_S1_ROI_in,
    input wire [7:0] trackgenerator2_S1_ROI_in,
    input wire [7:0] trackgenerator3_S1_ROI_in, 
    input wire [3:0] trackgenerator0_S1_pT_in,
    input wire [3:0] trackgenerator1_S1_pT_in,
    input wire [3:0] trackgenerator2_S1_pT_in,
    input wire [3:0] trackgenerator3_S1_pT_in,
    input wire       trackgenerator0_S1_sign_in,
    input wire       trackgenerator1_S1_sign_in,
    input wire       trackgenerator2_S1_sign_in,
    input wire       trackgenerator3_S1_sign_in,
    input wire [2:0] trackgenerator0_S1_InnerCoincidenceFlag_in,
    input wire [2:0] trackgenerator1_S1_InnerCoincidenceFlag_in,
    input wire [2:0] trackgenerator2_S1_InnerCoincidenceFlag_in,
    input wire [2:0] trackgenerator3_S1_InnerCoincidenceFlag_in,                
             
    output reg [15:0] trackgenerator0_S0_out,
    output reg [15:0] trackgenerator1_S0_out,
    output reg [15:0] trackgenerator2_S0_out,
    output reg [15:0] trackgenerator3_S0_out,
    output reg [15:0] trackgenerator0_S1_out,
    output reg [15:0] trackgenerator1_S1_out,
    output reg [15:0] trackgenerator2_S1_out,
    output reg [15:0] trackgenerator3_S1_out
   
       );
       
  //parameter BCR_DEPTH = 4095;
  //parameter BCR_LENGTH = 12;
     
wire [15:0] track0_S0;
wire [15:0] track1_S0;
wire [15:0] track2_S0;
wire [15:0] track3_S0;
wire [15:0] track0_S1;
wire [15:0] track1_S1;
wire [15:0] track2_S1;
wire [15:0] track3_S1;


// It might be better to have a option to output a single pulse, for example at the timing of BCR, instead of 40MHz output, in future.
always @ (posedge CLK) begin
   trackgenerator0_S0_out <= track0_S0; 
   trackgenerator1_S0_out <= track1_S0; 
   trackgenerator2_S0_out <= track2_S0; 
   trackgenerator3_S0_out <= track3_S0; 
   trackgenerator0_S1_out <= track0_S1; 
   trackgenerator1_S1_out <= track1_S1; 
   trackgenerator2_S1_out <= track2_S1; 
   trackgenerator3_S1_out <= track3_S1; 
end

assign track0_S0 = {trackgenerator0_S0_InnerCoincidenceFlag_in, trackgenerator0_S0_sign_in, trackgenerator0_S0_pT_in, trackgenerator0_S0_ROI_in};
assign track1_S0 = {trackgenerator1_S0_InnerCoincidenceFlag_in, trackgenerator1_S0_sign_in, trackgenerator1_S0_pT_in, trackgenerator1_S0_ROI_in};
assign track2_S0 = {trackgenerator2_S0_InnerCoincidenceFlag_in, trackgenerator2_S0_sign_in, trackgenerator2_S0_pT_in, trackgenerator2_S0_ROI_in};
assign track3_S0 = {trackgenerator3_S0_InnerCoincidenceFlag_in, trackgenerator3_S0_sign_in, trackgenerator3_S0_pT_in, trackgenerator3_S0_ROI_in};
assign track0_S1 = {trackgenerator0_S1_InnerCoincidenceFlag_in, trackgenerator0_S1_sign_in, trackgenerator0_S1_pT_in, trackgenerator0_S1_ROI_in};
assign track1_S1 = {trackgenerator1_S1_InnerCoincidenceFlag_in, trackgenerator1_S1_sign_in, trackgenerator1_S1_pT_in, trackgenerator1_S1_ROI_in};
assign track2_S1 = {trackgenerator2_S1_InnerCoincidenceFlag_in, trackgenerator2_S1_sign_in, trackgenerator2_S1_pT_in, trackgenerator2_S1_ROI_in};
assign track3_S1 = {trackgenerator3_S1_InnerCoincidenceFlag_in, trackgenerator3_S1_sign_in, trackgenerator3_S1_pT_in, trackgenerator3_S1_ROI_in};
       
endmodule