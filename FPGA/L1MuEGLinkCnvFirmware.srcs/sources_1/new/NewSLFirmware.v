`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Kyoto University
// Engineer: 
// 
// Create Date: 2016/02/10 12:00:00
// Design Name: 
// Module Name: NewSLFirmware 
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module NewSLFirmware
(
// Common Ports
    input wire  FPGA_CLK,   // Clock chosen by jumper pin: EXT, INT or TTC 
    input wire FPGA_CLK40,  // Clock from INT; Quartz on the board

// TTC Signals
    input wire L1A,
    input wire BCR,
    input wire ECR,
    input wire TTC_RESET,
    input wire TEST_PULSE_TRIGGER,
    output wire BUSY,

// GTX 
    input wire  Q2_CLK0_GTREFCLK_PAD_N_IN,
    input wire  Q2_CLK0_GTREFCLK_PAD_P_IN,
    input  wire [11:0]  RXN_IN,
    input  wire [11:0]  RXP_IN,
    output wire [11:0]  TXN_OUT,
    output wire [11:0]  TXP_OUT,
    output wire TX_DISABLE0,
    output wire TX_DISABLE1,
    output wire TX_DISABLE2,

// G-Link
    input wire [20:0] GL0,
    input wire [20:0] GL1,
    input wire [20:0] GL2,
    input wire [20:0] GL3,
    input wire [20:0] GL4,
    input wire [20:0] GL5,
    input wire [20:0] GL6,
    input wire [20:0] GL7,
    input wire [20:0] GL8,
    input wire [20:0] GL9,
    input wire [20:0] GL10,
    input wire [20:0] GL11,
    input wire [20:0] GL12,
    input wire [20:0] GL13,
    
    output wire [13:0] GLink_RX_DIV,//RX_DIV

// VME    
    input wire OE,
    input wire CE,
    input wire WE,
    input wire [11:0] VME_A,
    inout wire [15:0] VME_D,

    input  wire FPGA_RESET_B,

// Test Pins
    output wire  [15:0] TESTPIN,
    
// NIM outs    
    output wire NIMOUT_0,
    output wire NIMOUT_1,
    output wire NIMOUT_2,
    output wire NIMOUT_3
);

// Wire Declarations

// Clock wires
wire TTC_CLK;

// GTX latch clocks
// These clockes differ by 90 degs in phase
wire CLK_40_0;
wire CLK_40_1;
wire CLK_40_2;
wire CLK_40_3;

wire CLK_40_VMEWRITE;
wire CLK_160;    
wire locked;



// Reset
wire RESET_TTC;
wire RESET_160;
wire RESET_TX;
wire Scaler_reset;
wire TX_reset;
wire RX_reset;
wire Delay_reset;
wire CLK_reset;
wire FIFO_reset;
wire TX_Logic_reset;
wire L1A_manual_reset;

wire gt0_rx_reset;
wire gt1_rx_reset;
wire gt2_rx_reset;
wire gt3_rx_reset;
wire gt4_rx_reset;
wire gt5_rx_reset;
wire gt6_rx_reset;
wire gt7_rx_reset;
wire gt8_rx_reset;
wire gt9_rx_reset;
wire gt10_rx_reset;
wire gt11_rx_reset;

// G-Link
//G-Link IFD
wire [20:0] GL0_ifd;
wire [20:0] GL1_ifd;
wire [20:0] GL2_ifd;
wire [20:0] GL3_ifd;
wire [20:0] GL4_ifd;
wire [20:0] GL5_ifd;
wire [20:0] GL6_ifd;
wire [20:0] GL7_ifd;
wire [20:0] GL8_ifd;
wire [20:0] GL9_ifd;
wire [20:0] GL10_ifd;
wire [20:0] GL11_ifd;
wire [20:0] GL12_ifd;
wire [20:0] GL13_ifd;
wire [83:0] GL0_ifd_all;
wire [83:0] GL1_ifd_all;
wire [83:0] GL2_ifd_all;
wire [83:0] GL3_ifd_all;
wire [83:0] GL4_ifd_all;
wire [83:0] GL5_ifd_all;
wire [83:0] GL6_ifd_all;
wire [83:0] GL7_ifd_all;
wire [83:0] GL8_ifd_all;
wire [83:0] GL9_ifd_all;
wire [83:0] GL10_ifd_all;
wire [83:0] GL11_ifd_all;
wire [83:0] GL12_ifd_all;
wire [83:0] GL13_ifd_all;

// G-Link Phase selection
wire [3:0] Phase_glink0;
wire [3:0] Phase_glink1;
wire [3:0] Phase_glink2;
wire [3:0] Phase_glink3;
wire [3:0] Phase_glink4;
wire [3:0] Phase_glink5;
wire [3:0] Phase_glink6;
wire [3:0] Phase_glink7;
wire [3:0] Phase_glink8;
wire [3:0] Phase_glink9;
wire [3:0] Phase_glink10;
wire [3:0] Phase_glink11;
wire [3:0] Phase_glink12;
wire [3:0] Phase_glink13;

// G-Link Delay parameters
wire [7:0] Delay_glink0;
wire [7:0] Delay_glink1;
wire [7:0] Delay_glink2;
wire [7:0] Delay_glink3;
wire [7:0] Delay_glink4;
wire [7:0] Delay_glink5;
wire [7:0] Delay_glink6;
wire [7:0] Delay_glink7;
wire [7:0] Delay_glink8;
wire [7:0] Delay_glink9;
wire [7:0] Delay_glink10;
wire [7:0] Delay_glink11;
wire [7:0] Delay_glink12;
wire [7:0] Delay_glink13;

wire [16:0] glink0_delayed;
wire [16:0] glink1_delayed;
wire [16:0] glink2_delayed;
wire [16:0] glink3_delayed;
wire [16:0] glink4_delayed;
wire [16:0] glink5_delayed;
wire [16:0] glink6_delayed;
wire [16:0] glink7_delayed;
wire [16:0] glink8_delayed;
wire [16:0] glink9_delayed;
wire [16:0] glink10_delayed;
wire [16:0] glink11_delayed;
wire [16:0] glink12_delayed;
wire [16:0] glink13_delayed;

wire [1:0]      Mask_glink0;
wire [1:0]      Mask_glink1;
wire [1:0]      Mask_glink2;
wire [1:0]      Mask_glink3;
wire [1:0]      Mask_glink4;
wire [1:0]      Mask_glink5;
wire [1:0]      Mask_glink6;
wire [1:0]      Mask_glink7;
wire [1:0]      Mask_glink8;
wire [1:0]      Mask_glink9;
wire [1:0]      Mask_glink10;
wire [1:0]      Mask_glink11;
wire [1:0]      Mask_glink12;
wire [1:0]      Mask_glink13;



wire [47:0]     converted_glink_out0;
wire [47:0]     converted_glink_out1;
wire [47:0]     converted_glink_out2;
wire [47:0]     converted_glink_out3;
wire [47:0]     converted_glink_out4;
wire [47:0]     converted_glink_out5;
wire [47:0]     converted_glink_out6;
wire [47:0]     converted_glink_out7;
wire [47:0]     converted_glink_out8;
wire [47:0]     converted_glink_out9;
wire [47:0]     converted_glink_out10;
wire [47:0]     converted_glink_out11;



wire [16:0] glink0_masked;
wire [16:0] glink1_masked;
wire [16:0] glink2_masked;
wire [16:0] glink3_masked;
wire [16:0] glink4_masked;
wire [16:0] glink5_masked;
wire [16:0] glink6_masked;
wire [16:0] glink7_masked;
wire [16:0] glink8_masked;
wire [16:0] glink9_masked;
wire [16:0] glink10_masked;
wire [16:0] glink11_masked;
wire [16:0] glink12_masked;
wire [16:0] glink13_masked;

wire [16:0] trig_glink0_masked;
wire [16:0] trig_glink1_masked;
wire [16:0] trig_glink2_masked;
wire [16:0] trig_glink3_masked;
wire [16:0] trig_glink4_masked;
wire [16:0] trig_glink5_masked;
wire [16:0] trig_glink6_masked;
wire [16:0] trig_glink7_masked;
wire [16:0] trig_glink8_masked;
wire [16:0] trig_glink9_masked;
wire [16:0] trig_glink10_masked;
wire [16:0] trig_glink11_masked;
wire [16:0] trig_glink12_masked;
wire [16:0] trig_glink13_masked;



// GTX 

// GTX TX interface to IP
wire [31:0] gt0_txdata;
wire [3:0]  gt0_txcharisk;
wire [31:0] gt1_txdata;
wire [3:0]  gt1_txcharisk;
wire [31:0] gt2_txdata;
wire [3:0]  gt2_txcharisk;
wire [31:0] gt3_txdata;
wire [3:0]  gt3_txcharisk;
wire [31:0] gt4_txdata;
wire [3:0]  gt4_txcharisk;
wire [31:0] gt5_txdata;
wire [3:0]  gt5_txcharisk;
wire [31:0] gt6_txdata;
wire [3:0]  gt6_txcharisk;
wire [31:0] gt7_txdata;
wire [3:0]  gt7_txcharisk;
wire [31:0] gt8_txdata;
wire [3:0]  gt8_txcharisk;
wire [31:0] gt9_txdata;
wire [3:0]  gt9_txcharisk;
wire [31:0] gt10_txdata;
wire [3:0]  gt10_txcharisk;
wire [31:0] gt11_txdata;
wire [3:0]  gt11_txcharisk;
wire gt_txusrclk2;

// GTX RX interface to IP
wire [31:0] gt0_rxdata;
wire [3:0]  gt0_rxcharisk;
wire [31:0] gt1_rxdata;
wire [3:0]  gt1_rxcharisk;
wire [31:0] gt2_rxdata;
wire [3:0]  gt2_rxcharisk;
wire [31:0] gt3_rxdata;
wire [3:0]  gt3_rxcharisk;
wire [31:0] gt4_rxdata;
wire [3:0]  gt4_rxcharisk;
wire [31:0] gt5_rxdata;
wire [3:0]  gt5_rxcharisk;
wire [31:0] gt6_rxdata;
wire [3:0]  gt6_rxcharisk;
wire [31:0] gt7_rxdata;
wire [3:0]  gt7_rxcharisk;
wire [31:0] gt8_rxdata;
wire [3:0]  gt8_rxcharisk;
wire [31:0] gt9_rxdata;
wire [3:0]  gt9_rxcharisk;
wire [31:0] gt10_rxdata;
wire [3:0]  gt10_rxcharisk;
wire [31:0] gt11_rxdata;
wire [3:0]  gt11_rxcharisk;

wire gt0_rxusrclk;
wire gt1_rxusrclk;
wire gt2_rxusrclk;
wire gt3_rxusrclk;
wire gt4_rxusrclk;
wire gt5_rxusrclk;
wire gt6_rxusrclk;
wire gt7_rxusrclk;
wire gt8_rxusrclk;
wire gt9_rxusrclk;
wire gt10_rxusrclk;
wire gt11_rxusrclk;

// GTX Status and debugs
wire [5:0] gt0_status;
wire [5:0] gt1_status;
wire [5:0] gt2_status;
wire [5:0] gt3_status;
wire [5:0] gt4_status;
wire [5:0] gt5_status;
wire [5:0] gt6_status;
wire [5:0] gt7_status;
wire [5:0] gt8_status;
wire [5:0] gt9_status;
wire [5:0] gt10_status;
wire [5:0] gt11_status;

wire [15:0] gt0_error_scaler;
wire [15:0] gt1_error_scaler;
wire [15:0] gt2_error_scaler;
wire [15:0] gt3_error_scaler;
wire [15:0] gt4_error_scaler;
wire [15:0] gt5_error_scaler;
wire [15:0] gt6_error_scaler;
wire [15:0] gt7_error_scaler;
wire [15:0] gt8_error_scaler;
wire [15:0] gt9_error_scaler;
wire [15:0] gt10_error_scaler;
wire [15:0] gt11_error_scaler;

wire [3:0]      lane_selector;
wire [2:0]      loopback_mode;
wire [3:0]	glink_lane_selector;


// De-serialized GTX data
wire [111:0]    gtx0_data;
wire [111:0]    gtx1_data;
wire [111:0]    gtx2_data;
wire [111:0]    gtx3_data;
wire [111:0]    gtx4_data;
wire [111:0]    gtx5_data;
wire [111:0]    gtx6_data;
wire [111:0]    gtx7_data;
wire [111:0]    gtx8_data;
wire [111:0]    gtx9_data;
wire [111:0]    gtx10_data;
wire [111:0]    gtx11_data;

// GTX Delay parameters
wire [6:0]      Delay_gtx0;
wire [6:0]      Delay_gtx1;
wire [6:0]      Delay_gtx2;
wire [6:0]      Delay_gtx3;
wire [6:0]      Delay_gtx4;
wire [6:0]      Delay_gtx5;
wire [6:0]      Delay_gtx6;
wire [6:0]      Delay_gtx7;
wire [6:0]      Delay_gtx8;
wire [6:0]      Delay_gtx9;
wire [6:0]      Delay_gtx10;
wire [6:0]      Delay_gtx11;

// GTX deleyed signals
wire [111:0]    gtx0_delayed;
wire [111:0]    gtx1_delayed;
wire [111:0]    gtx2_delayed;
wire [111:0]    gtx3_delayed;
wire [111:0]    gtx4_delayed;
wire [111:0]    gtx5_delayed;
wire [111:0]    gtx6_delayed;
wire [111:0]    gtx7_delayed;
wire [111:0]    gtx8_delayed;
wire [111:0]    gtx9_delayed;
wire [111:0]    gtx10_delayed;
wire [111:0]    gtx11_delayed;

// GTX phase selector
wire [3:0]	gtx0_phase_select;
wire [3:0]	gtx1_phase_select;
wire [3:0]	gtx2_phase_select;
wire [3:0]	gtx3_phase_select;
wire [3:0]	gtx4_phase_select;
wire [3:0]	gtx5_phase_select;
wire [3:0]	gtx6_phase_select;
wire [3:0]	gtx7_phase_select;
wire [3:0]	gtx8_phase_select;
wire [3:0]	gtx9_phase_select;
wire [3:0]	gtx10_phase_select;
wire [3:0]	gtx11_phase_select;

// GTX phase monitor 
wire [5:0]	gtx0_phase_monitor_0, gtx0_phase_monitor_1, gtx0_phase_monitor_2, gtx0_phase_monitor_3;
wire [5:0]	gtx1_phase_monitor_0, gtx1_phase_monitor_1, gtx1_phase_monitor_2, gtx1_phase_monitor_3;
wire [5:0]	gtx2_phase_monitor_0, gtx2_phase_monitor_1, gtx2_phase_monitor_2, gtx2_phase_monitor_3;
wire [5:0]	gtx3_phase_monitor_0, gtx3_phase_monitor_1, gtx3_phase_monitor_2, gtx3_phase_monitor_3;
wire [5:0]	gtx4_phase_monitor_0, gtx4_phase_monitor_1, gtx4_phase_monitor_2, gtx4_phase_monitor_3;
wire [5:0]	gtx5_phase_monitor_0, gtx5_phase_monitor_1, gtx5_phase_monitor_2, gtx5_phase_monitor_3;
wire [5:0]	gtx6_phase_monitor_0, gtx6_phase_monitor_1, gtx6_phase_monitor_2, gtx6_phase_monitor_3;
wire [5:0]	gtx7_phase_monitor_0, gtx7_phase_monitor_1, gtx7_phase_monitor_2, gtx7_phase_monitor_3;
wire [5:0]	gtx8_phase_monitor_0, gtx8_phase_monitor_1, gtx8_phase_monitor_2, gtx8_phase_monitor_3;
wire [5:0]	gtx9_phase_monitor_0, gtx9_phase_monitor_1, gtx9_phase_monitor_2, gtx9_phase_monitor_3;
wire [5:0]	gtx10_phase_monitor_0, gtx10_phase_monitor_1, gtx10_phase_monitor_2, gtx10_phase_monitor_3;
wire [5:0]	gtx11_phase_monitor_0, gtx11_phase_monitor_1, gtx11_phase_monitor_2, gtx11_phase_monitor_3;

// GTX Mask parameters
wire [1:0]      Mask_gtx0;
wire [1:0]      Mask_gtx1;
wire [1:0]      Mask_gtx2;
wire [1:0]      Mask_gtx3;
wire [1:0]      Mask_gtx4;
wire [1:0]      Mask_gtx5;
wire [1:0]      Mask_gtx6;
wire [1:0]      Mask_gtx7;
wire [1:0]      Mask_gtx8;
wire [1:0]      Mask_gtx9;
wire [1:0]      Mask_gtx10;
wire [1:0]      Mask_gtx11;

// GTX masked read-out signals
wire [111:0]    gtx0_masked;
wire [111:0]    gtx1_masked;
wire [111:0]    gtx2_masked;
wire [111:0]    gtx3_masked;
wire [111:0]    gtx4_masked;
wire [111:0]    gtx5_masked;
wire [111:0]    gtx6_masked;
wire [111:0]    gtx7_masked;
wire [111:0]    gtx8_masked;
wire [111:0]    gtx9_masked;
wire [111:0]    gtx10_masked;
wire [111:0]    gtx11_masked;

// GTX masked trigger signals
wire [111:0]    trig_gtx0_masked;
wire [111:0]    trig_gtx1_masked;
wire [111:0]    trig_gtx2_masked;
wire [111:0]    trig_gtx3_masked;
wire [111:0]    trig_gtx4_masked;
wire [111:0]    trig_gtx5_masked;
wire [111:0]    trig_gtx6_masked;
wire [111:0]    trig_gtx7_masked;
wire [111:0]    trig_gtx8_masked;
wire [111:0]    trig_gtx9_masked;
wire [111:0]    trig_gtx10_masked;
wire [111:0]    trig_gtx11_masked;

// wires for readout path 
wire [1581:0]   rx_data_masked;
wire [159:0]    Trig_data_out;

// TTC signals

// TTC Delay Parameters
wire [7:0] Delay_L1A;
wire [7:0] Delay_BCR;
wire [7:0] Delay_trig_BCR;
wire [7:0] Delay_ECR;
wire [7:0] Delay_TTC_RESET;
wire [7:0] Delay_TEST_PULSE;

// TTC delayed signals
wire L1A_delayed;
wire BCR_delayed;
wire trig_BCR_delayed;
wire ECR_delayed;
wire TTC_RESET_delayed;    
wire TEST_PULSE_delayed;

// TTC Mask Parameters
wire Mask_L1A;
wire Mask_BCR;
wire Mask_trig_BCR;
wire Mask_ECR;
wire Mask_TTC_RESET;
wire Mask_TEST_PULSE;

// TTC masked signals
wire L1A_masked;
wire BCR_masked;
wire trig_BCR_masked;
wire ECR_masked;
wire TEST_PULSE_masked;
wire TTC_RESET_masked;    

// ID's
wire [11:0] L1ID;
wire [11:0] BCID;
wire [11:0] trig_BCID;
wire [11:0] SLID;
wire ConverterID;
wire [31:0] L1A_Counter;


wire [15:0] track0_S0;
wire [15:0] track1_S0;
wire [15:0] track2_S0;
wire [15:0] track3_S0;
wire [3:0] flag_S0;

wire [15:0] track0_S1;
wire [15:0] track1_S1;
wire [15:0] track2_S1;
wire [15:0] track3_S1;
wire [3:0] flag_S1;

// Output trigger info. via GTX_TX
wire [15:0] track0_S0_out;
wire [15:0] track1_S0_out;
wire [15:0] track2_S0_out;
wire [15:0] track3_S0_out;
wire [3:0] flag_S0_out;

wire [15:0] track0_S1_out;
wire [15:0] track1_S1_out;
wire [15:0] track2_S1_out;
wire [15:0] track3_S1_out;
wire [3:0] flag_S1_out;

// TrackOutputGenerator for commissioning
wire TrackOutputGenerator_enb;

wire [15:0] trackgenerator0_S0;
wire [15:0] trackgenerator1_S0;
wire [15:0] trackgenerator2_S0;
wire [15:0] trackgenerator3_S0;
wire [3:0] flag_trackgenerator_S0;

wire [15:0] trackgenerator0_S1;
wire [15:0] trackgenerator1_S1;
wire [15:0] trackgenerator2_S1;
wire [15:0] trackgenerator3_S1;
wire [3:0] flag_trackgenerator_S1;

// Flags related on InnerCoincidence
wire [13:0] FIdisableSSC_S0;
wire [4:0] EIdisableSSC_S0;
wire [13:0] FIdisableSSC_S1;
wire [4:0] EIdisableSSC_S1;

// monitoring FIFO
wire            write_enb;
wire            read_enb;
wire [255:0]    FIFO_data;

// temporal wires
wire [2:0] dummy;

// wires for Test Pulse 
wire [15:0] Test_Pulse_Length;
wire [15:0] Test_Pulse_Wait_Length;
wire        Test_Pulse_Enable;
wire        test_pulse;
wire [15:0] gtx0_test1, gtx0_test2, gtx0_test3, gtx0_test4, gtx0_test5, gtx0_test6, gtx0_test7,
            gtx1_test1, gtx1_test2, gtx1_test3, gtx1_test4, gtx1_test5, gtx1_test6, gtx1_test7,
            gtx2_test1, gtx2_test2, gtx2_test3, gtx2_test4, gtx2_test5, gtx2_test6, gtx2_test7,
            gtx3_test1, gtx3_test2, gtx3_test3, gtx3_test4, gtx3_test5, gtx3_test6, gtx3_test7,
            gtx4_test1, gtx4_test2, gtx4_test3, gtx4_test4, gtx4_test5, gtx4_test6, gtx4_test7,
            gtx5_test1, gtx5_test2, gtx5_test3, gtx5_test4, gtx5_test5, gtx5_test6, gtx5_test7,
            gtx6_test1, gtx6_test2, gtx6_test3, gtx6_test4, gtx6_test5, gtx6_test6, gtx6_test7,
            gtx7_test1, gtx7_test2, gtx7_test3, gtx7_test4, gtx7_test5, gtx7_test6, gtx7_test7,
            gtx8_test1, gtx8_test2, gtx8_test3, gtx8_test4, gtx8_test5, gtx8_test6, gtx8_test7,
            gtx9_test1, gtx9_test2, gtx9_test3, gtx9_test4, gtx9_test5, gtx9_test6, gtx9_test7,
            gtx10_test1, gtx10_test2, gtx10_test3, gtx10_test4, gtx10_test5, gtx10_test6, gtx10_test7,
            gtx11_test1, gtx11_test2, gtx11_test3, gtx11_test4, gtx11_test5, gtx11_test6, gtx11_test7;

wire [111:0] gtx0_test_data,
             gtx1_test_data,
             gtx2_test_data,
             gtx3_test_data,
             gtx4_test_data,
             gtx5_test_data,
             gtx6_test_data,
             gtx7_test_data,
             gtx8_test_data,
             gtx9_test_data,
             gtx10_test_data,
             gtx11_test_data;
            
wire [15:0] glink0_test1,
            glink1_test1,
            glink2_test1,
            glink3_test1,
            glink4_test1,
            glink5_test1,
            glink6_test1,
            glink7_test1,
            glink8_test1,
            glink9_test1,
            glink10_test1,
            glink11_test1,
            glink12_test1,
            glink13_test1;
            
wire [3:0] glink0_test2,
            glink1_test2,
            glink2_test2,
            glink3_test2,
            glink4_test2,
            glink5_test2,
            glink6_test2,
            glink7_test2,
            glink8_test2,
            glink9_test2,
            glink10_test2,
            glink11_test2,
            glink12_test2,
            glink13_test2;      
            
wire [16:0] glink0_test_data,
            glink1_test_data,            
            glink2_test_data,            
            glink3_test_data,            
            glink4_test_data,            
            glink5_test_data,            
            glink6_test_data,            
            glink7_test_data,            
            glink8_test_data,            
            glink9_test_data,            
            glink10_test_data,            
            glink11_test_data,            
            glink12_test_data,            
            glink13_test_data;            

// Signal assignments

// G-Link
wire [13:0] GLink_RX_DIV0;
//assign GLink_RX_DIV[13:0] = 14'b0;
assign BUSY = 1'b0;

// Masekd data
assign rx_data_masked ={gtx0_masked, gtx1_masked, gtx2_masked, gtx3_masked, 
                        gtx4_masked, gtx5_masked, gtx6_masked, gtx7_masked, 
                        gtx8_masked, gtx9_masked, gtx10_masked, gtx11_masked,
                        glink0_masked, glink1_masked, glink2_masked, glink3_masked, 
                        glink4_masked, glink5_masked, glink6_masked, glink7_masked, 
                        glink8_masked, glink9_masked, glink10_masked, glink11_masked, 
                        glink12_masked, glink13_masked};

// Trigger data
assign Trig_data_out[15:0] = track0_S0;
assign Trig_data_out[31:16] = track1_S0;
assign Trig_data_out[47:32] = track2_S0;
assign Trig_data_out[63:48] = track3_S0;
assign Trig_data_out[79:64] = track0_S1;
assign Trig_data_out[95:80] = track1_S1;
assign Trig_data_out[111:96] = track2_S1;
assign Trig_data_out[127:112] = track3_S1;
assign Trig_data_out[131:128] = flag_S0;
assign Trig_data_out[135:132] = flag_S1;
assign Trig_data_out[159:136] = 24'h0;

// Test data
assign gtx0_test_data[111:0] = {gtx0_test1[15:0], gtx0_test2[15:0], gtx0_test3[15:0], gtx0_test4[15:0], gtx0_test5[15:0], gtx0_test6[15:0], gtx0_test7[15:0]};
assign gtx1_test_data[111:0] = {gtx1_test1[15:0], gtx1_test2[15:0], gtx1_test3[15:0], gtx1_test4[15:0], gtx1_test5[15:0], gtx1_test6[15:0], gtx1_test7[15:0]};
assign gtx2_test_data[111:0] = {gtx2_test1[15:0], gtx2_test2[15:0], gtx2_test3[15:0], gtx2_test4[15:0], gtx2_test5[15:0], gtx2_test6[15:0], gtx2_test7[15:0]};
assign gtx3_test_data[111:0] = {gtx3_test1[15:0], gtx3_test2[15:0], gtx3_test3[15:0], gtx3_test4[15:0], gtx3_test5[15:0], gtx3_test6[15:0], gtx3_test7[15:0]};
assign gtx4_test_data[111:0] = {gtx4_test1[15:0], gtx4_test2[15:0], gtx4_test3[15:0], gtx4_test4[15:0], gtx4_test5[15:0], gtx4_test6[15:0], gtx4_test7[15:0]};
assign gtx5_test_data[111:0] = {gtx5_test1[15:0], gtx5_test2[15:0], gtx5_test3[15:0], gtx5_test4[15:0], gtx5_test5[15:0], gtx5_test6[15:0], gtx5_test7[15:0]};
assign gtx6_test_data[111:0] = {gtx6_test1[15:0], gtx6_test2[15:0], gtx6_test3[15:0], gtx6_test4[15:0], gtx6_test5[15:0], gtx6_test6[15:0], gtx6_test7[15:0]};
assign gtx7_test_data[111:0] = {gtx7_test1[15:0], gtx7_test2[15:0], gtx7_test3[15:0], gtx7_test4[15:0], gtx7_test5[15:0], gtx7_test6[15:0], gtx7_test7[15:0]};
assign gtx8_test_data[111:0] = {gtx8_test1[15:0], gtx8_test2[15:0], gtx8_test3[15:0], gtx8_test4[15:0], gtx8_test5[15:0], gtx8_test6[15:0], gtx8_test7[15:0]};
assign gtx9_test_data[111:0] = {gtx9_test1[15:0], gtx9_test2[15:0], gtx9_test3[15:0], gtx9_test4[15:0], gtx9_test5[15:0], gtx9_test6[15:0], gtx9_test7[15:0]};
assign gtx10_test_data[111:0] = {gtx10_test1[15:0], gtx10_test2[15:0], gtx10_test3[15:0], gtx10_test4[15:0], gtx10_test5[15:0], gtx10_test6[15:0], gtx10_test7[15:0]};
assign gtx11_test_data[111:0] = {gtx11_test1[15:0], gtx11_test2[15:0], gtx11_test3[15:0], gtx11_test4[15:0], gtx11_test5[15:0], gtx11_test6[15:0], gtx11_test7[15:0]};

assign glink0_test_data[16:0] = {glink0_test2[0], glink0_test1[15:0]};
assign glink1_test_data[16:0] = {glink1_test2[0], glink1_test1[15:0]};
assign glink2_test_data[16:0] = {glink2_test2[0], glink2_test1[15:0]};
assign glink3_test_data[16:0] = {glink3_test2[0], glink3_test1[15:0]};
assign glink4_test_data[16:0] = {glink4_test2[0], glink4_test1[15:0]};
assign glink5_test_data[16:0] = {glink5_test2[0], glink5_test1[15:0]};
assign glink6_test_data[16:0] = {glink6_test2[0], glink6_test1[15:0]};
assign glink7_test_data[16:0] = {glink7_test2[0], glink7_test1[15:0]};
assign glink8_test_data[16:0] = {glink8_test2[0], glink8_test1[15:0]};
assign glink9_test_data[16:0] = {glink9_test2[0], glink9_test1[15:0]};
assign glink10_test_data[16:0] = {glink10_test2[0], glink10_test1[15:0]};
assign glink11_test_data[16:0] = {glink11_test2[0], glink11_test1[15:0]};
assign glink12_test_data[16:0] = {glink12_test2[0], glink12_test1[15:0]};
assign glink13_test_data[16:0] = {glink13_test2[0], glink13_test1[15:0]};

// Test pins
assign TESTPIN[0] = TTC_CLK;
assign TESTPIN[1] = gt_txusrclk2;
assign TESTPIN[2] = gt0_rxusrclk;
assign TESTPIN[3] = gt1_rxusrclk;
assign TESTPIN[7:4] = gt0_txdata[11:8];
assign TESTPIN[11:8] = gt1_txdata[11:8];
assign TESTPIN[15:12] = gt0_rxdata[11:8];

// NIM output
//assign NIMOUT_0 = BUSY;
assign NIMOUT_0 = FPGA_CLK;
assign NIMOUT_1 = FPGA_CLK;
assign NIMOUT_2 = FPGA_CLK;
assign NIMOUT_3 = gt_txusrclk2;
//assign NIMOUT_3 = FPGA_CLK40;

///////////////////////////////////////////////////////////////////////////////
//================================ Main Logic ===============================//
///////////////////////////////////////////////////////////////////////////////



//************************ tmp L1A generator **************************//
reg [11:0] L1A_Generate_Count;
reg L1A_manual;
wire [11:0] L1A_reset_value;
always @ (posedge TTC_CLK) begin
    if (RESET_TTC||L1A_manual_reset) begin
        L1A_manual <= 1'b0;
        L1A_Generate_Count <= 12'h7ff;
    end
    else if (L1A_Generate_Count == 12'h0)begin
        L1A_manual <= 1'b1;
        L1A_Generate_Count <= L1A_reset_value[11:0];
    end
    else begin 
        L1A_manual <= 1'b0;
        L1A_Generate_Count <= L1A_Generate_Count - 12'b1;    
    end        
end
//*********************************************************************//


// GTX IP Core

GTX_12lane_320MHz_exdes GTX_12lane
(
    .refclk_n_in        (Q2_CLK0_GTREFCLK_PAD_N_IN),
    .refclk_p_in        (Q2_CLK0_GTREFCLK_PAD_P_IN),
    .drpclk_in          (TTC_CLK),
    .reset_in           (RESET_TTC),
    .TX_reset_in        (TX_reset),
    .RX_reset_in        (RX_reset),
    .gt0_rx_reset_in    (gt0_rx_reset),
    .gt1_rx_reset_in    (gt1_rx_reset),
    .gt2_rx_reset_in    (gt2_rx_reset),
    .gt3_rx_reset_in    (gt3_rx_reset),
    .gt4_rx_reset_in    (gt4_rx_reset),
    .gt5_rx_reset_in    (gt5_rx_reset),
    .gt6_rx_reset_in    (gt6_rx_reset),
    .gt7_rx_reset_in    (gt7_rx_reset),
    .gt8_rx_reset_in    (gt8_rx_reset),
    .gt9_rx_reset_in    (gt9_rx_reset),
    .gt10_rx_reset_in   (gt10_rx_reset),
    .gt11_rx_reset_in   (gt11_rx_reset),
    .Scaler_reset_in    (Scaler_reset),

    .RXN_IN             (RXN_IN),
    .RXP_IN             (RXP_IN),
    .TXN_OUT            (TXN_OUT),
    .TXP_OUT            (TXP_OUT),

// tx data 
    .gt0_txdata_in      (gt0_txdata), 
    .gt1_txdata_in      (gt1_txdata), 
    .gt2_txdata_in      (gt2_txdata), 
    .gt3_txdata_in      (gt3_txdata),
    .gt4_txdata_in      (gt4_txdata), 
    .gt5_txdata_in      (gt5_txdata), 
    .gt6_txdata_in      (gt6_txdata), 
    .gt7_txdata_in      (gt7_txdata),
    .gt8_txdata_in      (gt8_txdata), 
    .gt9_txdata_in      (gt9_txdata), 
    .gt10_txdata_in     (gt10_txdata), 
    .gt11_txdata_in     (gt11_txdata),
    
// tx char is k
    .gt0_txcharisk_in   (gt0_txcharisk), 
    .gt1_txcharisk_in   (gt1_txcharisk), 
    .gt2_txcharisk_in   (gt2_txcharisk), 
    .gt3_txcharisk_in   (gt3_txcharisk),
    .gt4_txcharisk_in   (gt4_txcharisk), 
    .gt5_txcharisk_in   (gt5_txcharisk), 
    .gt6_txcharisk_in   (gt6_txcharisk), 
    .gt7_txcharisk_in   (gt7_txcharisk),
    .gt8_txcharisk_in   (gt8_txcharisk), 
    .gt9_txcharisk_in   (gt9_txcharisk), 
    .gt10_txcharisk_in  (gt10_txcharisk), 
    .gt11_txcharisk_in  (gt11_txcharisk),
    
// tx user clock    
    .gt_txusrclk2_out   (gt_txusrclk2),
        
//rx data out
    .gt0_rxdata_out     (gt0_rxdata), 
    .gt1_rxdata_out     (gt1_rxdata), 
    .gt2_rxdata_out     (gt2_rxdata), 
    .gt3_rxdata_out     (gt3_rxdata),
    .gt4_rxdata_out     (gt4_rxdata), 
    .gt5_rxdata_out     (gt5_rxdata), 
    .gt6_rxdata_out     (gt6_rxdata), 
    .gt7_rxdata_out     (gt7_rxdata),
    .gt8_rxdata_out     (gt8_rxdata), 
    .gt9_rxdata_out     (gt9_rxdata), 
    .gt10_rxdata_out    (gt10_rxdata), 
    .gt11_rxdata_out    (gt11_rxdata),
    
// rx char is k
    .gt0_rxcharisk_out  (gt0_rxcharisk), 
    .gt1_rxcharisk_out  (gt1_rxcharisk), 
    .gt2_rxcharisk_out  (gt2_rxcharisk), 
    .gt3_rxcharisk_out  (gt3_rxcharisk),
    .gt4_rxcharisk_out  (gt4_rxcharisk), 
    .gt5_rxcharisk_out  (gt5_rxcharisk), 
    .gt6_rxcharisk_out  (gt6_rxcharisk), 
    .gt7_rxcharisk_out  (gt7_rxcharisk),
    .gt8_rxcharisk_out  (gt8_rxcharisk), 
    .gt9_rxcharisk_out  (gt9_rxcharisk), 
    .gt10_rxcharisk_out (gt10_rxcharisk), 
    .gt11_rxcharisk_out (gt11_rxcharisk),
    
// rx user clock
    .gt0_rxusrclk2_out  (gt0_rxusrclk), 
    .gt1_rxusrclk2_out  (gt1_rxusrclk), 
    .gt2_rxusrclk2_out  (gt2_rxusrclk), 
    .gt3_rxusrclk2_out  (gt3_rxusrclk),
    .gt4_rxusrclk2_out  (gt4_rxusrclk), 
    .gt5_rxusrclk2_out  (gt5_rxusrclk), 
    .gt6_rxusrclk2_out  (gt6_rxusrclk), 
    .gt7_rxusrclk2_out  (gt7_rxusrclk),
    .gt8_rxusrclk2_out  (gt8_rxusrclk), 
    .gt9_rxusrclk2_out  (gt9_rxusrclk), 
    .gt10_rxusrclk2_out (gt10_rxusrclk), 
    .gt11_rxusrclk2_out (gt11_rxusrclk),
    
// gtx status
    .gt0_status_out     (gt0_status),
    .gt1_status_out     (gt1_status),
    .gt2_status_out     (gt2_status),
    .gt3_status_out     (gt3_status),
    .gt4_status_out     (gt4_status),
    .gt5_status_out     (gt5_status),
    .gt6_status_out     (gt6_status),
    .gt7_status_out     (gt7_status),
    .gt8_status_out     (gt8_status),
    .gt9_status_out     (gt9_status),
    .gt10_status_out    (gt10_status),
    .gt11_status_out    (gt11_status),

    .gt0_err_scaler_out (gt0_error_scaler),
    .gt1_err_scaler_out (gt1_error_scaler),
    .gt2_err_scaler_out (gt2_error_scaler),
    .gt3_err_scaler_out (gt3_error_scaler),
    .gt4_err_scaler_out (gt4_error_scaler),
    .gt5_err_scaler_out (gt5_error_scaler),
    .gt6_err_scaler_out (gt6_error_scaler),
    .gt7_err_scaler_out (gt7_error_scaler),
    .gt8_err_scaler_out (gt8_error_scaler),
    .gt9_err_scaler_out (gt9_error_scaler),
    .gt10_err_scaler_out(gt10_error_scaler),
    .gt11_err_scaler_out(gt11_error_scaler),
    
    .TX_DISABLE0_out    (TX_DISABLE0),
    .TX_DISABLE1_out    (TX_DISABLE1),
    .TX_DISABLE2_out    (TX_DISABLE2),
    .loopback_mode_in   (loopback_mode)    
);


// GTX Transmit Port (TX Port)
GTX_TX GTX_TX(
// 40MHz Clock in
    .CLK_in             (TTC_CLK),
    .reset_in           (RESET_TX),
    .TX_reset_in        (TX_reset),
    .TX_Logic_reset_in  (TX_Logic_reset),

// tx user clock in
    .gt_txusrclk_in     (gt_txusrclk2),

// TTC signals and ID information
    .BCID_in            (trig_BCID),    
    .ConverterID_in     (ConverterID),

// trigger data for tx
    .gtx0_in           (converted_glink_out0),
    .gtx1_in           (converted_glink_out1),
    .gtx2_in           (converted_glink_out2),
    .gtx3_in           (converted_glink_out3),
    .gtx4_in           (converted_glink_out4),
    .gtx5_in           (converted_glink_out5),
    .gtx6_in           (converted_glink_out6),
    .gtx7_in           (converted_glink_out7),
    .gtx8_in           (converted_glink_out8),
    .gtx9_in           (converted_glink_out9),
    .gtx10_in          (converted_glink_out10),
    .gtx11_in          (converted_glink_out11),

    
// tx data out
    .gt0_txdata_out(gt0_txdata), .gt1_txdata_out(gt1_txdata), .gt2_txdata_out(gt2_txdata), .gt3_txdata_out(gt3_txdata),
    .gt4_txdata_out(gt4_txdata), .gt5_txdata_out(gt5_txdata), .gt6_txdata_out(gt6_txdata), .gt7_txdata_out(gt7_txdata),
    .gt8_txdata_out(gt8_txdata), .gt9_txdata_out(gt9_txdata), .gt10_txdata_out(gt10_txdata), .gt11_txdata_out(gt11_txdata),

// tx char is k ouot    
    .gt0_txcharisk_out(gt0_txcharisk), .gt1_txcharisk_out(gt1_txcharisk), .gt2_txcharisk_out(gt2_txcharisk), .gt3_txcharisk_out(gt3_txcharisk),
    .gt4_txcharisk_out(gt4_txcharisk), .gt5_txcharisk_out(gt5_txcharisk), .gt6_txcharisk_out(gt6_txcharisk), .gt7_txcharisk_out(gt7_txcharisk),
    .gt8_txcharisk_out(gt8_txcharisk), .gt9_txcharisk_out(gt9_txcharisk), .gt10_txcharisk_out(gt10_txcharisk), .gt11_txcharisk_out(gt11_txcharisk)
);


// Output from TrackOutputGenerator for commissioning
assign track0_S0_out[15:0]    = TrackOutputGenerator_enb ? trackgenerator0_S0 : track0_S0;
assign track1_S0_out[15:0]    = TrackOutputGenerator_enb ? trackgenerator0_S0 : track1_S0;
assign track2_S0_out[15:0]    = TrackOutputGenerator_enb ? trackgenerator0_S0 : track2_S0;
assign track3_S0_out[15:0]    = TrackOutputGenerator_enb ? trackgenerator0_S0 : track3_S0;
assign flag_S0_out[3:0]       = TrackOutputGenerator_enb ? flag_trackgenerator_S0 : flag_S0;

assign track0_S1_out[15:0]    = TrackOutputGenerator_enb ? trackgenerator0_S1 : track0_S1;
assign track1_S1_out[15:0]    = TrackOutputGenerator_enb ? trackgenerator0_S1 : track1_S1;
assign track2_S1_out[15:0]    = TrackOutputGenerator_enb ? trackgenerator0_S1 : track2_S1;
assign track3_S1_out[15:0]    = TrackOutputGenerator_enb ? trackgenerator0_S1 : track3_S1;
assign flag_S1_out[3:0]       = TrackOutputGenerator_enb ? flag_trackgenerator_S1 : flag_S1;

wire [7:0] trackgenerator0_S0_ROI; 
wire [7:0] trackgenerator1_S0_ROI;
wire [7:0] trackgenerator2_S0_ROI;
wire [7:0] trackgenerator3_S0_ROI;
wire [3:0] trackgenerator0_S0_pT; 
wire [3:0] trackgenerator1_S0_pT; 
wire [3:0] trackgenerator2_S0_pT; 
wire [3:0] trackgenerator3_S0_pT; 
wire       trackgenerator0_S0_sign; 
wire       trackgenerator1_S0_sign; 
wire       trackgenerator2_S0_sign; 
wire       trackgenerator3_S0_sign; 
wire [2:0] trackgenerator0_S0_InnerCoincidenceFlag; 
wire [2:0] trackgenerator1_S0_InnerCoincidenceFlag; 
wire [2:0] trackgenerator2_S0_InnerCoincidenceFlag; 
wire [2:0] trackgenerator3_S0_InnerCoincidenceFlag;

wire [7:0] trackgenerator0_S1_ROI; 
wire [7:0] trackgenerator1_S1_ROI;
wire [7:0] trackgenerator2_S1_ROI;
wire [7:0] trackgenerator3_S1_ROI;
wire [3:0] trackgenerator0_S1_pT; 
wire [3:0] trackgenerator1_S1_pT; 
wire [3:0] trackgenerator2_S1_pT; 
wire [3:0] trackgenerator3_S1_pT; 
wire       trackgenerator0_S1_sign; 
wire       trackgenerator1_S1_sign; 
wire       trackgenerator2_S1_sign; 
wire       trackgenerator3_S1_sign; 
wire [2:0] trackgenerator0_S1_InnerCoincidenceFlag; 
wire [2:0] trackgenerator1_S1_InnerCoincidenceFlag; 
wire [2:0] trackgenerator2_S1_InnerCoincidenceFlag; 
wire [2:0] trackgenerator3_S1_InnerCoincidenceFlag;

TrackOutputGenerator(
    .CLK(TTC_CLK),
    .trackgenerator0_S0_ROI_in (trackgenerator0_S0_ROI),
    .trackgenerator1_S0_ROI_in (trackgenerator1_S0_ROI),
    .trackgenerator2_S0_ROI_in (trackgenerator2_S0_ROI),
    .trackgenerator3_S0_ROI_in (trackgenerator3_S0_ROI),
    .trackgenerator0_S0_pT_in  (trackgenerator0_S0_pT),
    .trackgenerator1_S0_pT_in  (trackgenerator1_S0_pT),
    .trackgenerator2_S0_pT_in  (trackgenerator2_S0_pT),
    .trackgenerator3_S0_pT_in  (trackgenerator3_S0_pT),
    .trackgenerator0_S0_sign_in (trackgenerator0_S0_sign),
    .trackgenerator1_S0_sign_in (trackgenerator1_S0_sign),
    .trackgenerator2_S0_sign_in (trackgenerator2_S0_sign),
    .trackgenerator3_S0_sign_in (trackgenerator3_S0_sign),
    .trackgenerator0_S0_InnerCoincidenceFlag_in (trackgenerator0_S0_InnerCoincidenceFlag),
    .trackgenerator1_S0_InnerCoincidenceFlag_in (trackgenerator1_S0_InnerCoincidenceFlag),
    .trackgenerator2_S0_InnerCoincidenceFlag_in (trackgenerator2_S0_InnerCoincidenceFlag),
    .trackgenerator3_S0_InnerCoincidenceFlag_in (trackgenerator3_S0_InnerCoincidenceFlag),  
    
    .trackgenerator0_S1_ROI_in (trackgenerator0_S1_ROI),
    .trackgenerator1_S1_ROI_in (trackgenerator1_S1_ROI),
    .trackgenerator2_S1_ROI_in (trackgenerator2_S1_ROI),
    .trackgenerator3_S1_ROI_in (trackgenerator3_S1_ROI),
    .trackgenerator0_S1_pT_in  (trackgenerator0_S1_pT),
    .trackgenerator1_S1_pT_in  (trackgenerator1_S1_pT),
    .trackgenerator2_S1_pT_in  (trackgenerator2_S1_pT),
    .trackgenerator3_S1_pT_in  (trackgenerator3_S1_pT),
    .trackgenerator0_S1_sign_in (trackgenerator0_S1_sign),
    .trackgenerator1_S1_sign_in (trackgenerator1_S1_sign),
    .trackgenerator2_S1_sign_in (trackgenerator2_S1_sign),
    .trackgenerator3_S1_sign_in (trackgenerator3_S1_sign),
    .trackgenerator0_S1_InnerCoincidenceFlag_in (trackgenerator0_S1_InnerCoincidenceFlag),
    .trackgenerator1_S1_InnerCoincidenceFlag_in (trackgenerator1_S1_InnerCoincidenceFlag),
    .trackgenerator2_S1_InnerCoincidenceFlag_in (trackgenerator2_S1_InnerCoincidenceFlag),
    .trackgenerator3_S1_InnerCoincidenceFlag_in (trackgenerator3_S1_InnerCoincidenceFlag),        
 
    .trackgenerator0_S0_out (trackgenerator0_S0),
    .trackgenerator1_S0_out (trackgenerator1_S0),
    .trackgenerator2_S0_out (trackgenerator2_S0),   
    .trackgenerator3_S0_out (trackgenerator3_S0),
    .trackgenerator0_S1_out (trackgenerator0_S1),
    .trackgenerator1_S1_out (trackgenerator1_S1),
    .trackgenerator2_S1_out (trackgenerator2_S1),   
    .trackgenerator3_S1_out (trackgenerator3_S1)    

);

// GTX Receive Port (RX Port)
GTX_RX GTX_RX(
// rx data in
    .gt0_rxdata_in(gt0_rxdata), .gt1_rxdata_in(gt1_rxdata), .gt2_rxdata_in(gt2_rxdata), .gt3_rxdata_in(gt3_rxdata),
    .gt4_rxdata_in(gt4_rxdata), .gt5_rxdata_in(gt5_rxdata), .gt6_rxdata_in(gt6_rxdata), .gt7_rxdata_in(gt7_rxdata),
    .gt8_rxdata_in(gt8_rxdata), .gt9_rxdata_in(gt9_rxdata), .gt10_rxdata_in(gt10_rxdata), .gt11_rxdata_in(gt11_rxdata),

//rx cha is k in    
    .gt0_rxcharisk_in(gt0_rxcharisk), .gt1_rxcharisk_in(gt1_rxcharisk), .gt2_rxcharisk_in(gt2_rxcharisk), .gt3_rxcharisk_in(gt3_rxcharisk),
    .gt4_rxcharisk_in(gt4_rxcharisk), .gt5_rxcharisk_in(gt5_rxcharisk), .gt6_rxcharisk_in(gt6_rxcharisk), .gt7_rxcharisk_in(gt7_rxcharisk),
    .gt8_rxcharisk_in(gt8_rxcharisk), .gt9_rxcharisk_in(gt9_rxcharisk), .gt10_rxcharisk_in(gt10_rxcharisk), .gt11_rxcharisk_in(gt11_rxcharisk),

//rx user clock in        
    .gt0_rxusrclk_in(gt0_rxusrclk), .gt1_rxusrclk_in(gt1_rxusrclk), .gt2_rxusrclk_in(gt2_rxusrclk), .gt3_rxusrclk_in(gt3_rxusrclk),
    .gt4_rxusrclk_in(gt4_rxusrclk), .gt5_rxusrclk_in(gt5_rxusrclk), .gt6_rxusrclk_in(gt6_rxusrclk), .gt7_rxusrclk_in(gt7_rxusrclk),
    .gt8_rxusrclk_in(gt8_rxusrclk), .gt9_rxusrclk_in(gt9_rxusrclk), .gt10_rxusrclk_in(gt10_rxusrclk), .gt11_rxusrclk_in(gt11_rxusrclk),

    .reset_in        (RESET_TTC),
    .RX_reset_in     (RX_reset),
    .rx0_data_out    (gtx0_data),
    .rx1_data_out    (gtx1_data),
    .rx2_data_out    (gtx2_data),
    .rx3_data_out    (gtx3_data),
    .rx4_data_out    (gtx4_data),
    .rx5_data_out    (gtx5_data),
    .rx6_data_out    (gtx6_data),
    .rx7_data_out    (gtx7_data),
    .rx8_data_out    (gtx8_data),
    .rx9_data_out    (gtx9_data),
    .rx10_data_out   (gtx10_data),
    .rx11_data_out   (gtx11_data)
);



//// Read-Out Line

// GTX Delay Port
GTX_Delay GTX_Delay(
    .CLK_in             (TTC_CLK),
    .CLK_in_0           (CLK_40_0),
    .CLK_in_1           (CLK_40_1),
    .CLK_in_2           (CLK_40_2),
    .CLK_in_3           (CLK_40_3),
    .reset_in           (RESET_TTC),
    .Delay_reset_in     (Delay_reset),
    
    .gtx0_data_in        (gtx0_data[111:0]),
    .gtx1_data_in        (gtx1_data[111:0]),
    .gtx2_data_in        (gtx2_data[111:0]),
    .gtx3_data_in        (gtx3_data[111:0]),
    .gtx4_data_in        (gtx4_data[111:0]),
    .gtx5_data_in        (gtx5_data[111:0]),
    .gtx6_data_in        (gtx6_data[111:0]),
    .gtx7_data_in        (gtx7_data[111:0]),
    .gtx8_data_in        (gtx8_data[111:0]),
    .gtx9_data_in        (gtx9_data[111:0]),
    .gtx10_data_in       (gtx10_data[111:0]),
    .gtx11_data_in       (gtx11_data[111:0]),

    .gtx0_delay_in       (Delay_gtx0),
    .gtx1_delay_in       (Delay_gtx1),
    .gtx2_delay_in       (Delay_gtx2),
    .gtx3_delay_in       (Delay_gtx3),
    .gtx4_delay_in       (Delay_gtx4),
    .gtx5_delay_in       (Delay_gtx5),
    .gtx6_delay_in       (Delay_gtx6),
    .gtx7_delay_in       (Delay_gtx7),
    .gtx8_delay_in       (Delay_gtx8),
    .gtx9_delay_in       (Delay_gtx9),
    .gtx10_delay_in      (Delay_gtx10),
    .gtx11_delay_in      (Delay_gtx11),

    .gtx0_phase_in       (gtx0_phase_select),
    .gtx1_phase_in       (gtx1_phase_select),
    .gtx2_phase_in       (gtx2_phase_select),
    .gtx3_phase_in       (gtx3_phase_select),
    .gtx4_phase_in       (gtx4_phase_select),
    .gtx5_phase_in       (gtx5_phase_select),
    .gtx6_phase_in       (gtx6_phase_select),
    .gtx7_phase_in       (gtx7_phase_select),
    .gtx8_phase_in       (gtx8_phase_select),
    .gtx9_phase_in       (gtx9_phase_select),
    .gtx10_phase_in      (gtx10_phase_select),
    .gtx11_phase_in      (gtx11_phase_select),

    .gtx0_phase_0_out      (gtx0_phase_monitor_0),
    .gtx0_phase_1_out      (gtx0_phase_monitor_1),
    .gtx0_phase_2_out      (gtx0_phase_monitor_2),
    .gtx0_phase_3_out      (gtx0_phase_monitor_3),

    .gtx1_phase_0_out      (gtx1_phase_monitor_0),
    .gtx1_phase_1_out      (gtx1_phase_monitor_1),
    .gtx1_phase_2_out      (gtx1_phase_monitor_2),
    .gtx1_phase_3_out      (gtx1_phase_monitor_3),

    .gtx2_phase_0_out      (gtx2_phase_monitor_0),
    .gtx2_phase_1_out      (gtx2_phase_monitor_1),
    .gtx2_phase_2_out      (gtx2_phase_monitor_2),
    .gtx2_phase_3_out      (gtx2_phase_monitor_3),

    .gtx3_phase_0_out      (gtx3_phase_monitor_0),
    .gtx3_phase_1_out      (gtx3_phase_monitor_1),
    .gtx3_phase_2_out      (gtx3_phase_monitor_2),
    .gtx3_phase_3_out      (gtx3_phase_monitor_3),

    .gtx4_phase_0_out      (gtx4_phase_monitor_0),
    .gtx4_phase_1_out      (gtx4_phase_monitor_1),
    .gtx4_phase_2_out      (gtx4_phase_monitor_2),
    .gtx4_phase_3_out      (gtx4_phase_monitor_3),

    .gtx5_phase_0_out      (gtx5_phase_monitor_0),
    .gtx5_phase_1_out      (gtx5_phase_monitor_1),
    .gtx5_phase_2_out      (gtx5_phase_monitor_2),
    .gtx5_phase_3_out      (gtx5_phase_monitor_3),

    .gtx6_phase_0_out      (gtx6_phase_monitor_0),
    .gtx6_phase_1_out      (gtx6_phase_monitor_1),
    .gtx6_phase_2_out      (gtx6_phase_monitor_2),
    .gtx6_phase_3_out      (gtx6_phase_monitor_3),

    .gtx7_phase_0_out      (gtx7_phase_monitor_0),
    .gtx7_phase_1_out      (gtx7_phase_monitor_1),
    .gtx7_phase_2_out      (gtx7_phase_monitor_2),
    .gtx7_phase_3_out      (gtx7_phase_monitor_3),

    .gtx8_phase_0_out      (gtx8_phase_monitor_0),
    .gtx8_phase_1_out      (gtx8_phase_monitor_1),
    .gtx8_phase_2_out      (gtx8_phase_monitor_2),
    .gtx8_phase_3_out      (gtx8_phase_monitor_3),

    .gtx9_phase_0_out      (gtx9_phase_monitor_0),
    .gtx9_phase_1_out      (gtx9_phase_monitor_1),
    .gtx9_phase_2_out      (gtx9_phase_monitor_2),
    .gtx9_phase_3_out      (gtx9_phase_monitor_3),

    .gtx10_phase_0_out     (gtx10_phase_monitor_0),
    .gtx10_phase_1_out     (gtx10_phase_monitor_1),
    .gtx10_phase_2_out     (gtx10_phase_monitor_2),
    .gtx10_phase_3_out     (gtx10_phase_monitor_3),

    .gtx11_phase_0_out     (gtx11_phase_monitor_0),
    .gtx11_phase_1_out     (gtx11_phase_monitor_1),
    .gtx11_phase_2_out     (gtx11_phase_monitor_2),
    .gtx11_phase_3_out     (gtx11_phase_monitor_3),

    .gtx0_delayed_out    (gtx0_delayed),
    .gtx1_delayed_out    (gtx1_delayed),
    .gtx2_delayed_out    (gtx2_delayed),
    .gtx3_delayed_out    (gtx3_delayed),
    .gtx4_delayed_out    (gtx4_delayed),
    .gtx5_delayed_out    (gtx5_delayed),
    .gtx6_delayed_out    (gtx6_delayed),
    .gtx7_delayed_out    (gtx7_delayed),
    .gtx8_delayed_out    (gtx8_delayed),
    .gtx9_delayed_out    (gtx9_delayed),
    .gtx10_delayed_out   (gtx10_delayed),
    .gtx11_delayed_out   (gtx11_delayed)    

);


//G-Link receive port
// G-Link Phase Align
GLink_PhaseAlign GLink_PhaseAlign(
    .CLK_in (TTC_CLK),
    .CLK_160 (CLK_160),
    .reset_in (RESET_TTC),

    .glink0_data_in     (GL0),
    .glink1_data_in     (GL1),
    .glink2_data_in     (GL2),
    .glink3_data_in     (GL3),
    .glink4_data_in     (GL4),
    .glink5_data_in     (GL5),
    .glink6_data_in     (GL6),
    .glink7_data_in     (GL7),
    .glink8_data_in     (GL8),
    .glink9_data_in     (GL9),
    .glink10_data_in    (GL10),
    .glink11_data_in    (GL11),
    .glink12_data_in    (GL12),
    .glink13_data_in    (GL13),

    .Phase_glink0   (Phase_glink0),
    .Phase_glink1   (Phase_glink1),
    .Phase_glink2   (Phase_glink2),
    .Phase_glink3   (Phase_glink3),
    .Phase_glink4   (Phase_glink4),
    .Phase_glink5   (Phase_glink5),
    .Phase_glink6   (Phase_glink6),
    .Phase_glink7   (Phase_glink7),
    .Phase_glink8   (Phase_glink8),
    .Phase_glink9   (Phase_glink9),
    .Phase_glink10  (Phase_glink10),
    .Phase_glink11  (Phase_glink11),
    .Phase_glink12  (Phase_glink12),
    .Phase_glink13  (Phase_glink13),

    .GL0_ifd           (GL0_ifd),
    .GL1_ifd           (GL1_ifd),
    .GL2_ifd           (GL2_ifd),
    .GL3_ifd           (GL3_ifd),
    .GL4_ifd           (GL4_ifd),
    .GL5_ifd           (GL5_ifd),
    .GL6_ifd           (GL6_ifd),
    .GL7_ifd           (GL7_ifd),
    .GL8_ifd           (GL8_ifd),
    .GL9_ifd           (GL9_ifd),
    .GL10_ifd          (GL10_ifd),
    .GL11_ifd          (GL11_ifd),
    .GL12_ifd          (GL12_ifd),
    .GL13_ifd          (GL13_ifd),

    .GL0_ifd_all       (GL0_ifd_all),
    .GL1_ifd_all       (GL1_ifd_all),
    .GL2_ifd_all       (GL2_ifd_all),
    .GL3_ifd_all       (GL3_ifd_all),
    .GL4_ifd_all       (GL4_ifd_all),
    .GL5_ifd_all       (GL5_ifd_all),
    .GL6_ifd_all       (GL6_ifd_all),
    .GL7_ifd_all       (GL7_ifd_all),
    .GL8_ifd_all       (GL8_ifd_all),
    .GL9_ifd_all       (GL9_ifd_all),
    .GL10_ifd_all      (GL10_ifd_all),
    .GL11_ifd_all      (GL11_ifd_all),
    .GL12_ifd_all      (GL12_ifd_all),
    .GL13_ifd_all      (GL13_ifd_all)

);



wire Reset_GLinkMonitor;
wire GL0_State_Reset, GL1_State_Reset, GL2_State_Reset, GL3_State_Reset,
      GL4_State_Reset, GL5_State_Reset, GL6_State_Reset, GL7_State_Reset,
      GL8_State_Reset, GL9_State_Reset, GL10_State_Reset, GL11_State_Reset,
      GL12_State_Reset, GL13_State_Reset;  
wire GL0_ErrorCounter_Reset, GL1_ErrorCounter_Reset, GL2_ErrorCounter_Reset, GL3_ErrorCounter_Reset,
      GL4_ErrorCounter_Reset, GL5_ErrorCounter_Reset, GL6_ErrorCounter_Reset, GL7_ErrorCounter_Reset,
      GL8_ErrorCounter_Reset, GL9_ErrorCounter_Reset, GL10_ErrorCounter_Reset, GL11_ErrorCounter_Reset,
      GL12_ErrorCounter_Reset, GL13_ErrorCounter_Reset;    
wire RX_DIVen;          
wire [1:0] GL0_RX_DIV_vme, GL1_RX_DIV_vme, GL2_RX_DIV_vme, GL3_RX_DIV_vme,
            GL4_RX_DIV_vme, GL5_RX_DIV_vme, GL6_RX_DIV_vme, GL7_RX_DIV_vme,
            GL8_RX_DIV_vme, GL9_RX_DIV_vme, GL10_RX_DIV_vme, GL11_RX_DIV_vme,
            GL12_RX_DIV_vme, GL13_RX_DIV_vme;
wire [15:0] GL0_State, GL1_State, GL2_State, GL3_State,
            GL4_State, GL5_State, GL6_State, GL7_State,
            GL8_State, GL9_State, GL10_State, GL11_State,
            GL12_State, GL13_State;                    
wire [15:0] GL0_ErrorCounter, GL1_ErrorCounter, GL2_ErrorCounter, GL3_ErrorCounter,
             GL4_ErrorCounter, GL5_ErrorCounter, GL6_ErrorCounter, GL7_ErrorCounter,
             GL8_ErrorCounter, GL9_ErrorCounter, GL10_ErrorCounter, GL11_ErrorCounter,
             GL12_ErrorCounter, GL13_ErrorCounter;      
wire [1:0] GLinkOK;

// G-Link Monitor
GLink_Monitor GLink_Monitor(
    .CLK_in(TTC_CLK),
    .Reset_in(RESET_TTC),
    .Reset_GLinkMonitor_in(Reset_GLinkMonitor), // from VME
    
    .GL0_monitor_in(GL0_ifd[20:17]),
    .GL1_monitor_in(GL1_ifd[20:17]),
    .GL2_monitor_in(GL2_ifd[20:17]),
    .GL3_monitor_in(GL3_ifd[20:17]),
    .GL4_monitor_in(GL4_ifd[20:17]),
    .GL5_monitor_in(GL5_ifd[20:17]),
    .GL6_monitor_in(GL6_ifd[20:17]),
    .GL7_monitor_in(GL7_ifd[20:17]),
    .GL8_monitor_in(GL8_ifd[20:17]),
    .GL9_monitor_in(GL9_ifd[20:17]),
    .GL10_monitor_in(GL10_ifd[20:17]),
    .GL11_monitor_in(GL11_ifd[20:17]),
    .GL12_monitor_in(GL12_ifd[20:17]),
    .GL13_monitor_in(GL13_ifd[20:17]),
    
    .GL0_State_Reset_in(GL0_State_Reset), // from VME
    .GL1_State_Reset_in(GL1_State_Reset),
    .GL2_State_Reset_in(GL2_State_Reset),
    .GL3_State_Reset_in(GL3_State_Reset),
    .GL4_State_Reset_in(GL4_State_Reset),
    .GL5_State_Reset_in(GL5_State_Reset),
    .GL6_State_Reset_in(GL6_State_Reset),
    .GL7_State_Reset_in(GL7_State_Reset),
    .GL8_State_Reset_in(GL8_State_Reset),
    .GL9_State_Reset_in(GL9_State_Reset),
    .GL10_State_Reset_in(GL10_State_Reset),
    .GL11_State_Reset_in(GL11_State_Reset),
    .GL12_State_Reset_in(GL12_State_Reset),
    .GL13_State_Reset_in(GL13_State_Reset),
    
    .GL0_ErrorCounter_Reset_in(GL0_ErrorCounter_Reset), // from VME
    .GL1_ErrorCounter_Reset_in(GL1_ErrorCounter_Reset),
    .GL2_ErrorCounter_Reset_in(GL2_ErrorCounter_Reset),
    .GL3_ErrorCounter_Reset_in(GL3_ErrorCounter_Reset),
    .GL4_ErrorCounter_Reset_in(GL4_ErrorCounter_Reset),
    .GL5_ErrorCounter_Reset_in(GL5_ErrorCounter_Reset),
    .GL6_ErrorCounter_Reset_in(GL6_ErrorCounter_Reset),
    .GL7_ErrorCounter_Reset_in(GL7_ErrorCounter_Reset),
    .GL8_ErrorCounter_Reset_in(GL8_ErrorCounter_Reset),
    .GL9_ErrorCounter_Reset_in(GL9_ErrorCounter_Reset),
    .GL10_ErrorCounter_Reset_in(GL10_ErrorCounter_Reset),
    .GL11_ErrorCounter_Reset_in(GL11_ErrorCounter_Reset),
    .GL12_ErrorCounter_Reset_in(GL12_ErrorCounter_Reset),
    .GL13_ErrorCounter_Reset_in(GL13_ErrorCounter_Reset),
    
    .RX_DIVen_in(RX_DIVen), // RXDIV from VME
    .GL0_RX_DIV_vme_in(GL0_RX_DIV_vme), // Set value of RX_DIV from VME 
    .GL1_RX_DIV_vme_in(GL1_RX_DIV_vme),
    .GL2_RX_DIV_vme_in(GL2_RX_DIV_vme),
    .GL3_RX_DIV_vme_in(GL3_RX_DIV_vme),
    .GL4_RX_DIV_vme_in(GL4_RX_DIV_vme),
    .GL5_RX_DIV_vme_in(GL5_RX_DIV_vme),
    .GL6_RX_DIV_vme_in(GL6_RX_DIV_vme),
    .GL7_RX_DIV_vme_in(GL7_RX_DIV_vme),
    .GL8_RX_DIV_vme_in(GL8_RX_DIV_vme),
    .GL9_RX_DIV_vme_in(GL9_RX_DIV_vme),
    .GL10_RX_DIV_vme_in(GL10_RX_DIV_vme),
    .GL11_RX_DIV_vme_in(GL11_RX_DIV_vme),
    .GL12_RX_DIV_vme_in(GL12_RX_DIV_vme),
    .GL13_RX_DIV_vme_in(GL13_RX_DIV_vme),
    
    .GL0_State_out(GL0_State), // To VME
    .GL1_State_out(GL1_State),
    .GL2_State_out(GL2_State),
    .GL3_State_out(GL3_State),
    .GL4_State_out(GL4_State),
    .GL5_State_out(GL5_State),
    .GL6_State_out(GL6_State), 
    .GL7_State_out(GL7_State),
    .GL8_State_out(GL8_State),
    .GL9_State_out(GL9_State),
    .GL10_State_out(GL10_State),
    .GL11_State_out(GL11_State),
    .GL12_State_out(GL12_State),     
    .GL13_State_out(GL13_State),
    
    .GL0_ErrorCounter_out(GL0_ErrorCounter), // To VME
    .GL1_ErrorCounter_out(GL1_ErrorCounter),
    .GL2_ErrorCounter_out(GL2_ErrorCounter),
    .GL3_ErrorCounter_out(GL3_ErrorCounter),
    .GL4_ErrorCounter_out(GL4_ErrorCounter),     
    .GL5_ErrorCounter_out(GL5_ErrorCounter),
    .GL6_ErrorCounter_out(GL6_ErrorCounter),
    .GL7_ErrorCounter_out(GL7_ErrorCounter),
    .GL8_ErrorCounter_out(GL8_ErrorCounter),
    .GL9_ErrorCounter_out(GL9_ErrorCounter),
    .GL10_ErrorCounter_out(GL10_ErrorCounter),    
    .GL11_ErrorCounter_out(GL11_ErrorCounter),
    .GL12_ErrorCounter_out(GL12_ErrorCounter),
    .GL13_ErrorCounter_out(GL13_ErrorCounter),
    .GLinkOK_out(GLinkOK),     // To GLink_Decorder in Run2. It has not been used here. Do we need to do anything in firmware? It should be done in online software?
    .RX_DIV0_out(GLink_RX_DIV0), 
    .RX_DIV1_out(GLink_RX_DIV)       
);


// G-Link Delay
GLink_Delay GLink_Delay(
    .CLK_in (TTC_CLK),
    .reset_in (RESET_TTC),
    .Delay_reset_in (Delay_reset),

    .glink0_data_in     (GL0_ifd),
    .glink1_data_in     (GL1_ifd),
    .glink2_data_in     (GL2_ifd),
    .glink3_data_in     (GL3_ifd),
    .glink4_data_in     (GL4_ifd),
    .glink5_data_in     (GL5_ifd),
    .glink6_data_in     (GL6_ifd),
    .glink7_data_in     (GL7_ifd),
    .glink8_data_in     (GL8_ifd),
    .glink9_data_in     (GL9_ifd),
    .glink10_data_in    (GL10_ifd),
    .glink11_data_in    (GL11_ifd),
    .glink12_data_in    (GL12_ifd),
    .glink13_data_in    (GL13_ifd),


    .glink0_delay_in   (Delay_glink0),
    .glink1_delay_in   (Delay_glink1),
    .glink2_delay_in   (Delay_glink2),
    .glink3_delay_in   (Delay_glink3),
    .glink4_delay_in   (Delay_glink4),
    .glink5_delay_in   (Delay_glink5),
    .glink6_delay_in   (Delay_glink6),
    .glink7_delay_in   (Delay_glink7),
    .glink8_delay_in   (Delay_glink8),
    .glink9_delay_in   (Delay_glink9),
    .glink10_delay_in  (Delay_glink10),
    .glink11_delay_in  (Delay_glink11),
    .glink12_delay_in  (Delay_glink12),
    .glink13_delay_in  (Delay_glink13),

    .glink0_delayed_out (glink0_delayed),
    .glink1_delayed_out (glink1_delayed),
    .glink2_delayed_out (glink2_delayed),
    .glink3_delayed_out (glink3_delayed),
    .glink4_delayed_out (glink4_delayed),
    .glink5_delayed_out (glink5_delayed),
    .glink6_delayed_out (glink6_delayed),
    .glink7_delayed_out (glink7_delayed),
    .glink8_delayed_out (glink8_delayed),
    .glink9_delayed_out (glink9_delayed),
    .glink10_delayed_out (glink10_delayed),
    .glink11_delayed_out (glink11_delayed),
    .glink12_delayed_out (glink12_delayed),
    .glink13_delayed_out (glink13_delayed)
);



//GTX RX/G-Link RX data mask
Mask Mask(
    .CLK_in          (TTC_CLK),
    .reset_in        (RESET_TTC),
    .gtx0_data_in    (gtx0_delayed),
    .gtx1_data_in    (gtx1_delayed),
    .gtx2_data_in    (gtx2_delayed),
    .gtx3_data_in    (gtx3_delayed),
    .gtx4_data_in    (gtx4_delayed),
    .gtx5_data_in    (gtx5_delayed),
    .gtx6_data_in    (gtx6_delayed),
    .gtx7_data_in    (gtx7_delayed),
    .gtx8_data_in    (gtx8_delayed),
    .gtx9_data_in    (gtx9_delayed),
    .gtx10_data_in   (gtx10_delayed),
    .gtx11_data_in   (gtx11_delayed),

    .glink0_data_in (glink0_delayed),
    .glink1_data_in (glink1_delayed),
    .glink2_data_in (glink2_delayed),
    .glink3_data_in (glink3_delayed),
    .glink4_data_in (glink4_delayed),
    .glink5_data_in (glink5_delayed),
    .glink6_data_in (glink6_delayed),
    .glink7_data_in (glink7_delayed),
    .glink8_data_in (glink8_delayed),
    .glink9_data_in (glink9_delayed),
    .glink10_data_in(glink10_delayed),
    .glink11_data_in(glink11_delayed),
    .glink12_data_in(glink12_delayed),
    .glink13_data_in(glink13_delayed),

    .Test_Pulse_Length  (Test_Pulse_Length),
    .Test_Pulse_Wait_Length  (Test_Pulse_Wait_Length),
    .Test_Pulse_Enable  (Test_Pulse_Enable),
    .test_pulse_out     (test_pulse),

    
    .Mask_gtx0_in   (Mask_gtx0),
    .Mask_gtx1_in   (Mask_gtx1),
    .Mask_gtx2_in   (Mask_gtx2),
    .Mask_gtx3_in   (Mask_gtx3),
    .Mask_gtx4_in   (Mask_gtx4),
    .Mask_gtx5_in   (Mask_gtx5),
    .Mask_gtx6_in   (Mask_gtx6),
    .Mask_gtx7_in   (Mask_gtx7),
    .Mask_gtx8_in   (Mask_gtx8),
    .Mask_gtx9_in   (Mask_gtx9),
    .Mask_gtx10_in  (Mask_gtx10),
    .Mask_gtx11_in  (Mask_gtx11),

    .Mask_glink0_in (Mask_glink0),
    .Mask_glink1_in (Mask_glink1),
    .Mask_glink2_in (Mask_glink2),
    .Mask_glink3_in (Mask_glink3),
    .Mask_glink4_in (Mask_glink4),
    .Mask_glink5_in (Mask_glink5),
    .Mask_glink6_in (Mask_glink6),
    .Mask_glink7_in (Mask_glink7),
    .Mask_glink8_in (Mask_glink8),
    .Mask_glink9_in (Mask_glink9),
    .Mask_glink10_in(Mask_glink10),
    .Mask_glink11_in(Mask_glink11),
    .Mask_glink12_in(Mask_glink12),
    .Mask_glink13_in(Mask_glink13),

    .gtx0_test_data_in   (gtx0_test_data),
    .gtx1_test_data_in   (gtx1_test_data),
    .gtx2_test_data_in   (gtx2_test_data),
    .gtx3_test_data_in   (gtx3_test_data),
    .gtx4_test_data_in   (gtx4_test_data),
    .gtx5_test_data_in   (gtx5_test_data),
    .gtx6_test_data_in   (gtx6_test_data),
    .gtx7_test_data_in   (gtx7_test_data),
    .gtx8_test_data_in   (gtx8_test_data),
    .gtx9_test_data_in   (gtx9_test_data),
    .gtx10_test_data_in  (gtx10_test_data),
    .gtx11_test_data_in  (gtx11_test_data),
    .glink0_test_data_in (glink0_test_data),
    .glink1_test_data_in (glink1_test_data),
    .glink2_test_data_in (glink2_test_data),
    .glink3_test_data_in (glink3_test_data),
    .glink4_test_data_in (glink4_test_data),
    .glink5_test_data_in (glink5_test_data),
    .glink6_test_data_in (glink6_test_data),
    .glink7_test_data_in (glink7_test_data),
    .glink8_test_data_in (glink8_test_data),
    .glink9_test_data_in (glink9_test_data),
    .glink10_test_data_in(glink10_test_data),
    .glink11_test_data_in(glink11_test_data),
    .glink12_test_data_in(glink12_test_data),
    .glink13_test_data_in(glink13_test_data),
    
    .gtx0_data_out       (gtx0_masked),
    .gtx1_data_out       (gtx1_masked),
    .gtx2_data_out       (gtx2_masked),
    .gtx3_data_out       (gtx3_masked),
    .gtx4_data_out       (gtx4_masked),
    .gtx5_data_out       (gtx5_masked),
    .gtx6_data_out       (gtx6_masked),
    .gtx7_data_out       (gtx7_masked),
    .gtx8_data_out       (gtx8_masked),
    .gtx9_data_out       (gtx9_masked),
    .gtx10_data_out      (gtx10_masked),
    .gtx11_data_out      (gtx11_masked),

    .glink0_data_out     (glink0_masked),
    .glink1_data_out     (glink1_masked),
    .glink2_data_out     (glink2_masked),
    .glink3_data_out     (glink3_masked),
    .glink4_data_out     (glink4_masked),
    .glink5_data_out     (glink5_masked),
    .glink6_data_out     (glink6_masked),
    .glink7_data_out     (glink7_masked),
    .glink8_data_out     (glink8_masked),
    .glink9_data_out     (glink9_masked),
    .glink10_data_out    (glink10_masked),
    .glink11_data_out    (glink11_masked),
    .glink12_data_out    (glink12_masked),
    .glink13_data_out    (glink13_masked),

    .trig_gtx0_data_out       (trig_gtx0_masked),
    .trig_gtx1_data_out       (trig_gtx1_masked),
    .trig_gtx2_data_out       (trig_gtx2_masked),
    .trig_gtx3_data_out       (trig_gtx3_masked),
    .trig_gtx4_data_out       (trig_gtx4_masked),
    .trig_gtx5_data_out       (trig_gtx5_masked),
    .trig_gtx6_data_out       (trig_gtx6_masked),
    .trig_gtx7_data_out       (trig_gtx7_masked),
    .trig_gtx8_data_out       (trig_gtx8_masked),
    .trig_gtx9_data_out       (trig_gtx9_masked),
    .trig_gtx10_data_out      (trig_gtx10_masked),
    .trig_gtx11_data_out      (trig_gtx11_masked),

    .trig_glink0_data_out    (trig_glink0_masked),
    .trig_glink1_data_out    (trig_glink1_masked),
    .trig_glink2_data_out    (trig_glink2_masked),
    .trig_glink3_data_out    (trig_glink3_masked),
    .trig_glink4_data_out    (trig_glink4_masked),
    .trig_glink5_data_out    (trig_glink5_masked),
    .trig_glink6_data_out    (trig_glink6_masked),
    .trig_glink7_data_out    (trig_glink7_masked),
    .trig_glink8_data_out    (trig_glink8_masked),
    .trig_glink9_data_out    (trig_glink9_masked),
    .trig_glink10_data_out   (trig_glink10_masked),
    .trig_glink11_data_out   (trig_glink11_masked),
    .trig_glink12_data_out   (trig_glink12_masked),
    .trig_glink13_data_out   (trig_glink13_masked)
);

//Converter
Converter Converter(
    .clk             (TTC_CLK),
    .rst             (RESET_TTC),
    .glink_rx0       (glink0_masked[16:0]),
    .glink_rx1       (glink1_masked[16:0]),
    .glink_rx2       (glink2_masked[16:0]),
    .glink_rx3       (glink3_masked[16:0]),
    .glink_rx4       (glink4_masked[16:0]),
    .glink_rx5       (glink5_masked[16:0]),
    .glink_rx6       (glink6_masked[16:0]),
    .glink_rx7       (glink7_masked[16:0]),
    .glink_rx8       (glink8_masked[16:0]),
    .glink_rx9       (glink9_masked[16:0]),
    .glink_rx10      (glink10_masked[16:0]),
    .glink_rx11      (glink11_masked[16:0]),
    .glink_rx12      (glink12_masked[16:0]),
    .glink_rx13      (glink13_masked[16:0]),
    
    .gtx_tx0         (converted_glink_out0),
    .gtx_tx1         (converted_glink_out1),
    .gtx_tx2         (converted_glink_out2),
    .gtx_tx3         (converted_glink_out3),
    .gtx_tx4         (converted_glink_out4),
    .gtx_tx5         (converted_glink_out5),
    .gtx_tx6         (converted_glink_out6),
    .gtx_tx7         (converted_glink_out7),
    .gtx_tx8         (converted_glink_out8),
    .gtx_tx9         (converted_glink_out9),
    .gtx_tx10        (converted_glink_out10),
    .gtx_tx11        (converted_glink_out11)
);



// TTC Delays
TTC_Delay TTC_Delay(
// Clock and resets
    .CLK_in             (TTC_CLK),
    .reset_in           (RESET_TTC),
    .Delay_reset_in     (Delay_reset),
// input signals
    .L1A_in             (L1A||L1A_manual),
    .BCR_in             (BCR),
    .ECR_in             (ECR),
    .TTC_RESET_in       (TTC_RESET),
    .TEST_PULSE_in      (TEST_PULSE_TRIGGER),

// TTC delay parameter, common for all TTC signals
    .L1A_delay_in           (Delay_L1A),
    .BCR_delay_in           (Delay_BCR),
    .trig_BCR_delay_in      (Delay_trig_BCR),
    .ECR_delay_in           (Delay_ECR),
    .TTC_RESET_delay_in     (Delay_TTC_RESET),
    .TEST_PULSE_delay_in    (Delay_TEST_PULSE),

// Output ports, Delayed TTC signals
    .L1A_delayed_out        (L1A_delayed),
    .BCR_delayed_out        (BCR_delayed),
    .trig_BCR_delayed_out   (trig_BCR_delayed),
    .ECR_delayed_out        (ECR_delayed),
    .TTC_RESET_delayed_out  (TTC_RESET_delayed),
    .TEST_PULSE_delayed_out (TEST_PULSE_delayed)
);

// Mask
assign L1A_masked = (Mask_L1A) ? L1A_manual : L1A_delayed;
assign BCR_masked = (Mask_BCR) ? 1'b0 : BCR_delayed;
assign trig_BCR_masked = (Mask_trig_BCR) ? 1'b0 : trig_BCR_delayed;
assign ECR_masked = (Mask_ECR) ? 1'b0 : ECR_delayed;
assign TTC_RESET_masked = (Mask_TTC_RESET) ? 1'b0 : TTC_RESET_delayed;
assign TEST_PULSE_masked = (Mask_TEST_PULSE) ? 1'b0 : TEST_PULSE_delayed;

// L1ID & BCID Counter
ID_Counter ID_Counter(
    .CLK_in             (TTC_CLK),
    .reset_in           (RESET_TTC),
    .L1A_in             (L1A_masked),
    .BCR_in             (BCR_masked),    
    .trig_BCR_in        (trig_BCR_masked),    
    .ECR_in             (ECR_masked),
    .BCID_out           (BCID),
    .trig_BCID_out      (trig_BCID),
    .L1ID_out           (L1ID),
    .L1A_Counter_out    (L1A_Counter)
);


Clock_Generator Clock_Generator(
    .FPGA_CLK_in     (FPGA_CLK),
    .FPGA_CLK40_in   (FPGA_CLK40),
    .reset_in        (CLK_reset),
    
    .TTC_CLK_out     (TTC_CLK),
    .CLK_40_VMEWRITE_out(CLK_40_VMEWRITE),
    .CLK_40_0_out    (CLK_40_0),
    .CLK_40_1_out    (CLK_40_1),
    .CLK_40_2_out    (CLK_40_2),
    .CLK_40_3_out    (CLK_40_3),
    .CLK_160_out     (CLK_160),
    .locked_out      (locked)
);


wire [67:0] gl_4phase_input = (lane_selector == 4'h0) ? {1'b0,GL0_ifd_all[78:63],1'b0,GL0_ifd_all[57:42],1'b0,GL0_ifd_all[36:21],1'b0,GL0_ifd_all[15:0]} :
                               (lane_selector == 4'h1) ? {1'b0,GL1_ifd_all[78:63],1'b0,GL1_ifd_all[57:42],1'b0,GL1_ifd_all[36:21],1'b0,GL1_ifd_all[15:0]} :
                               (lane_selector == 4'h2) ? {1'b0,GL2_ifd_all[78:63],1'b0,GL2_ifd_all[57:42],1'b0,GL2_ifd_all[36:21],1'b0,GL2_ifd_all[15:0]} :
                               (lane_selector == 4'h3) ? {1'b0,GL3_ifd_all[78:63],1'b0,GL3_ifd_all[57:42],1'b0,GL3_ifd_all[36:21],1'b0,GL3_ifd_all[15:0]} :
                               (lane_selector == 4'h4) ? {1'b0,GL4_ifd_all[78:63],1'b0,GL4_ifd_all[57:42],1'b0,GL4_ifd_all[36:21],1'b0,GL4_ifd_all[15:0]} :
                               (lane_selector == 4'h5) ? {1'b0,GL5_ifd_all[78:63],1'b0,GL5_ifd_all[57:42],1'b0,GL5_ifd_all[36:21],1'b0,GL5_ifd_all[15:0]} :
                               (lane_selector == 4'h6) ? {1'b0,GL6_ifd_all[78:63],1'b0,GL6_ifd_all[57:42],1'b0,GL6_ifd_all[36:21],1'b0,GL6_ifd_all[15:0]} :
                               (lane_selector == 4'h7) ? {1'b0,GL7_ifd_all[78:63],1'b0,GL7_ifd_all[57:42],1'b0,GL7_ifd_all[36:21],1'b0,GL7_ifd_all[15:0]} :
                               (lane_selector == 4'h8) ? {1'b0,GL8_ifd_all[78:63],1'b0,GL8_ifd_all[57:42],1'b0,GL8_ifd_all[36:21],1'b0,GL8_ifd_all[15:0]} :
                               (lane_selector == 4'h9) ? {1'b0,GL9_ifd_all[78:63],1'b0,GL9_ifd_all[57:42],1'b0,GL9_ifd_all[36:21],1'b0,GL9_ifd_all[15:0]} :
                               (lane_selector == 4'ha) ? {1'b0,GL10_ifd_all[78:63],1'b0,GL10_ifd_all[57:42],1'b0,GL10_ifd_all[36:21],1'b0,GL10_ifd_all[15:0]} :
                               (lane_selector == 4'hb) ? {1'b0,GL11_ifd_all[78:63],1'b0,GL11_ifd_all[57:42],1'b0,GL11_ifd_all[36:21],1'b0,GL11_ifd_all[15:0]} :
                               (lane_selector == 4'hc) ? {1'b0,GL12_ifd_all[78:63],1'b0,GL12_ifd_all[57:42],1'b0,GL12_ifd_all[36:21],1'b0,GL12_ifd_all[15:0]} :
                               (lane_selector == 4'hd) ? {1'b0,GL13_ifd_all[78:63],1'b0,GL13_ifd_all[57:42],1'b0,GL13_ifd_all[36:21],1'b0,GL13_ifd_all[15:0]} :
                               {1'b0,GL0_ifd_all[78:63],1'b0,GL0_ifd_all[57:42],1'b0,GL0_ifd_all[36:21],1'b0,GL0_ifd_all[15:0]};


wire [255:0] FIFO_data_input;

assign FIFO_data_input[111:0]  =  (lane_selector == 4'h0) ? gtx0_delayed :
                                  (lane_selector == 4'h1) ? gtx1_delayed :
                                  (lane_selector == 4'h2) ? gtx2_delayed :
                                  (lane_selector == 4'h3) ? gtx3_delayed :
                                  (lane_selector == 4'h4) ? gtx4_delayed :
                                  (lane_selector == 4'h5) ? gtx5_delayed :
                                  (lane_selector == 4'h6) ? gtx6_delayed :
                                  (lane_selector == 4'h7) ? gtx7_delayed :
                                  gtx0_delayed;
assign FIFO_data_input[179:112] = gl_4phase_input;



//assign FIFO_data_input[127:0] = (lane_selector == 4'h0) ? {16'h0, rx_data_masked[1581:1470]} :
//                                (lane_selector == 4'h1) ? {16'h0, rx_data_masked[1469:1358]} :
//                                (lane_selector == 4'h2) ? {16'h0, rx_data_masked[1357:1246]} :
//                                (lane_selector == 4'h3) ? {16'h0, rx_data_masked[1245:1134]} :
//                                (lane_selector == 4'h4) ? {16'h0, rx_data_masked[1133:1022]} :
//                                (lane_selector == 4'h5) ? {16'h0, rx_data_masked[1021:910]} :
//                                (lane_selector == 4'h6) ? {16'h0, rx_data_masked[909:798]} :
//                                (lane_selector == 4'h7) ? {16'h0, rx_data_masked[797:686]} :
//                                (lane_selector == 4'h8) ? {16'h0, rx_data_masked[685:574]} :
//                                (lane_selector == 4'h9) ? {16'h0, rx_data_masked[573:462]} :
//                                (lane_selector == 4'ha) ? {16'h0, rx_data_masked[461:350]} :
//                                (lane_selector == 4'hb) ? {16'h0, rx_data_masked[349:238]} :
//                                (lane_selector == 4'hc) ? {9'b0, rx_data_masked[237:119]} :
//                                (lane_selector == 4'hd) ? {9'b0, rx_data_masked[118:0]} : 128'h0;
//
//assign FIFO_data_input[239:128] = 112'b0;
//
//assign FIFO_data_input[255:240] = zero_data;


// GTX Monitoring FIFO (Only for Debug)
Monitoring Monitoring(
    .CLK_in         (TTC_CLK),
    .data_in        (FIFO_data_input),
    .RST_in         (RESET_160||FIFO_reset),
    .write_enb      (write_enb),
    .read_enb       (read_enb),    
    .FIFO_data_out  (FIFO_data)
);


// FPGA monitoring
wire [15:0] xadc_monitor_out = 16'h0;
wire Monitoring_reset;
wire xadc_mon_enable;
wire [6:0] xadc_mon_addr;
wire [15:0] debug_wire;


// VME Module
vme vme(
    .CLK_40            (CLK_40_VMEWRITE),
    .CLK_160            (CLK_160),
    .TXUSRCLK_in        (gt_txusrclk2),
    .OE                 (OE),
    .CE                 (CE),
    .WE                 (WE),
    .VME_A              (VME_A[11:0]),
    .VME_D              (VME_D[15:0]),
    
//  Reset       
    .Reset_TTC_out          (RESET_TTC),
    .Reset_160_out          (RESET_160),
    .Reset_TX_out           (RESET_TX),
    .Scaler_reset_out       (Scaler_reset),
    .GTX_TX_reset_out       (TX_reset),
    .GTX_RX_reset_out       (RX_reset),
    .Delay_reset_out        (Delay_reset),
    .CLK_reset_out          (CLK_reset),
    .FIFO_reset_out         (FIFO_reset),
    .TX_Logic_reset_out     (TX_Logic_reset),
    .Monitoring_reset_out   (Monitoring_reset),
    .gt0_rx_reset_out       (gt0_rx_reset),
    .gt1_rx_reset_out       (gt1_rx_reset),
    .gt2_rx_reset_out       (gt2_rx_reset),
    .gt3_rx_reset_out       (gt3_rx_reset),
    .gt4_rx_reset_out       (gt4_rx_reset),
    .gt5_rx_reset_out       (gt5_rx_reset),
    .gt6_rx_reset_out       (gt6_rx_reset),
    .gt7_rx_reset_out       (gt7_rx_reset),
    .gt8_rx_reset_out       (gt8_rx_reset),
    .gt9_rx_reset_out       (gt9_rx_reset),
    .gt10_rx_reset_out      (gt10_rx_reset),
    .gt11_rx_reset_out      (gt11_rx_reset),
    
// GTX status
    .Error_Scaler_gt_0     (gt0_error_scaler),
    .Error_Scaler_gt_1     (gt1_error_scaler),
    .Error_Scaler_gt_2     (gt2_error_scaler),
    .Error_Scaler_gt_3     (gt3_error_scaler),
    .Error_Scaler_gt_4     (gt4_error_scaler),
    .Error_Scaler_gt_5     (gt5_error_scaler),
    .Error_Scaler_gt_6     (gt6_error_scaler),
    .Error_Scaler_gt_7     (gt7_error_scaler),
    .Error_Scaler_gt_8     (gt8_error_scaler),
    .Error_Scaler_gt_9     (gt9_error_scaler),
    .Error_Scaler_gt_10    (gt10_error_scaler),
    .Error_Scaler_gt_11    (gt11_error_scaler),

    .Status_gt_0         ({10'h0, gt0_status}),
    .Status_gt_1         ({10'h0, gt1_status}),    
    .Status_gt_2         ({10'h0, gt2_status}),
    .Status_gt_3         ({10'h0, gt3_status}),
    .Status_gt_4         ({10'h0, gt4_status}),
    .Status_gt_5         ({10'h0, gt5_status}),
    .Status_gt_6         ({10'h0, gt6_status}),
    .Status_gt_7         ({10'h0, gt7_status}),
    .Status_gt_8         ({10'h0, gt8_status}),
    .Status_gt_9         ({10'h0, gt9_status}),
    .Status_gt_10         ({10'h0, gt10_status}),
    .Status_gt_11         ({10'h0, gt11_status}),

// G-Link Phase select
    .Phase_glink0         (Phase_glink0),
    .Phase_glink1         (Phase_glink1),
    .Phase_glink2         (Phase_glink2),
    .Phase_glink3         (Phase_glink3),
    .Phase_glink4         (Phase_glink4),
    .Phase_glink5         (Phase_glink5),
    .Phase_glink6         (Phase_glink6),
    .Phase_glink7         (Phase_glink7),
    .Phase_glink8         (Phase_glink8),
    .Phase_glink9         (Phase_glink9),
    .Phase_glink10        (Phase_glink10),
    .Phase_glink11        (Phase_glink11),
    .Phase_glink12        (Phase_glink12),
    .Phase_glink13        (Phase_glink13),
    
// G-Link Delay parameter
    .Delay_glink0         (Delay_glink0),
    .Delay_glink1         (Delay_glink1),
    .Delay_glink2         (Delay_glink2),
    .Delay_glink3         (Delay_glink3),
    .Delay_glink4         (Delay_glink4),
    .Delay_glink5         (Delay_glink5),
    .Delay_glink6         (Delay_glink6),
    .Delay_glink7         (Delay_glink7),
    .Delay_glink8         (Delay_glink8),
    .Delay_glink9         (Delay_glink9),
    .Delay_glink10        (Delay_glink10),
    .Delay_glink11        (Delay_glink11),
    .Delay_glink12        (Delay_glink12),
    .Delay_glink13        (Delay_glink13),
    
    .Delay_gtx0           (Delay_gtx0),
    .Delay_gtx1           (Delay_gtx1),
    .Delay_gtx2           (Delay_gtx2),
    .Delay_gtx3           (Delay_gtx3),
    .Delay_gtx4           (Delay_gtx4),
    .Delay_gtx5           (Delay_gtx5),
    .Delay_gtx6           (Delay_gtx6),
    .Delay_gtx7           (Delay_gtx7),
    .Delay_gtx8           (Delay_gtx8),
    .Delay_gtx9           (Delay_gtx9),
    .Delay_gtx10          (Delay_gtx10),
    .Delay_gtx11          (Delay_gtx11),

    .Delay_L1A              (Delay_L1A),
    .Delay_BCR              (Delay_BCR),
    .Delay_trig_BCR         (Delay_trig_BCR),
    .Delay_ECR              (Delay_ECR),
    .Delay_TTC_RESET        (Delay_TTC_RESET),
    .Delay_TEST_PULSE       (Delay_TEST_PULSE),

    
// Mask
    .Mask_gtx0           (Mask_gtx0),
    .Mask_gtx1           (Mask_gtx1),
    .Mask_gtx2           (Mask_gtx2),
    .Mask_gtx3           (Mask_gtx3),
    .Mask_gtx4           (Mask_gtx4),
    .Mask_gtx5           (Mask_gtx5),
    .Mask_gtx6           (Mask_gtx6),
    .Mask_gtx7           (Mask_gtx7),
    .Mask_gtx8           (Mask_gtx8),
    .Mask_gtx9           (Mask_gtx9),
    .Mask_gtx10          (Mask_gtx10),
    .Mask_gtx11          (Mask_gtx11),

    .Mask_L1A           (Mask_L1A),
    .Mask_BCR           (Mask_BCR),
    .Mask_trig_BCR      (Mask_trig_BCR),
    .Mask_ECR           (Mask_ECR),
    .Mask_TTC_RESET     (Mask_TTC_RESET),
    .Mask_TEST_PULSE    (Mask_TEST_PULSE),

    .Mask_glink0   (Mask_glink0),
    .Mask_glink1   (Mask_glink1),
    .Mask_glink2   (Mask_glink2),
    .Mask_glink3   (Mask_glink3),
    .Mask_glink4   (Mask_glink4),
    .Mask_glink5   (Mask_glink5),
    .Mask_glink6   (Mask_glink6),
    .Mask_glink7   (Mask_glink7),
    .Mask_glink8   (Mask_glink8),
    .Mask_glink9   (Mask_glink9),
    .Mask_glink10  (Mask_glink10),
    .Mask_glink11  (Mask_glink11),
    .Mask_glink12  (Mask_glink12),
    .Mask_glink13  (Mask_glink13),

//Test pulse
    .Test_Pulse_Length(Test_Pulse_Length),
    .Test_Pulse_Wait_Length(Test_Pulse_Wait_Length),
    .Test_Pulse_Enable(Test_Pulse_Enable),
    
// ID counters
    .L1A_Counter0       (L1A_Counter[15:0]),
    .L1A_Counter1       (L1A_Counter[31:16]),
    .L1ID               (L1ID),
    .BCID               (BCID),    
    .SLID               (SLID),

//Converter
    .CONVERTERID        (ConverterID),

    .monitoring_FIFO_out_0        (FIFO_data[15:0]),  
    .monitoring_FIFO_out_1        (FIFO_data[31:16]), 
    .monitoring_FIFO_out_2        (FIFO_data[47:32]), 
    .monitoring_FIFO_out_3        (FIFO_data[63:48]), 
    .monitoring_FIFO_out_4        (FIFO_data[79:64]), 
    .monitoring_FIFO_out_5        (FIFO_data[95:80]), 
    .monitoring_FIFO_out_6        (FIFO_data[111:96]),
    .monitoring_FIFO_out_7        (FIFO_data[127:112]),
    .monitoring_FIFO_out_8        (FIFO_data[143:128]),
    .monitoring_FIFO_out_9        (FIFO_data[159:144]),
    .monitoring_FIFO_out_10       (FIFO_data[175:160]),
    .monitoring_FIFO_out_11       (FIFO_data[191:176]),
    .monitoring_FIFO_out_12       (FIFO_data[207:192]),
    .monitoring_FIFO_out_13       (FIFO_data[223:208]),
    .monitoring_FIFO_out_14       (FIFO_data[239:224]),
    .monitoring_FIFO_out_15       (FIFO_data[255:240]),
    
    .monitoring_FIFO_wr_en     (write_enb),
    .monitoring_FIFO_rd_en     (read_enb),
    .lane_selector_out  ({loopback_mode[2:0], lane_selector[3:0]}),
    .glink_lane_selector_out    (glink_lane_selector),
    .xadc_mon_enable_out        (xadc_mon_enable),
    .xadc_mon_addr_out          (xadc_mon_addr),
    .xadc_monitor_in            (xadc_monitor_out),

    .L1A_manual_parameter       ({L1A_reset_value, dummy[2:0], L1A_manual_reset}),
    
    .gtx0_test_data1     (gtx0_test1),
    .gtx0_test_data2     (gtx0_test2),
    .gtx0_test_data3     (gtx0_test3),
    .gtx0_test_data4     (gtx0_test4),
    .gtx0_test_data5     (gtx0_test5),
    .gtx0_test_data6     (gtx0_test6),
    .gtx0_test_data7     (gtx0_test7),
    .gtx1_test_data1     (gtx1_test1),
    .gtx1_test_data2     (gtx1_test2),
    .gtx1_test_data3     (gtx1_test3),
    .gtx1_test_data4     (gtx1_test4),
    .gtx1_test_data5     (gtx1_test5),
    .gtx1_test_data6     (gtx1_test6),
    .gtx1_test_data7     (gtx1_test7),
    .gtx2_test_data1     (gtx2_test1),
    .gtx2_test_data2     (gtx2_test2),
    .gtx2_test_data3     (gtx2_test3),
    .gtx2_test_data4     (gtx2_test4),
    .gtx2_test_data5     (gtx2_test5),
    .gtx2_test_data6     (gtx2_test6),
    .gtx2_test_data7     (gtx2_test7),
    .gtx3_test_data1     (gtx3_test1),
    .gtx3_test_data2     (gtx3_test2),
    .gtx3_test_data3     (gtx3_test3),
    .gtx3_test_data4     (gtx3_test4),
    .gtx3_test_data5     (gtx3_test5),
    .gtx3_test_data6     (gtx3_test6),
    .gtx3_test_data7     (gtx3_test7),
    .gtx4_test_data1     (gtx4_test1),
    .gtx4_test_data2     (gtx4_test2),
    .gtx4_test_data3     (gtx4_test3),
    .gtx4_test_data4     (gtx4_test4),
    .gtx4_test_data5     (gtx4_test5),
    .gtx4_test_data6     (gtx4_test6),
    .gtx4_test_data7     (gtx4_test7),
    .gtx5_test_data1     (gtx5_test1),
    .gtx5_test_data2     (gtx5_test2),
    .gtx5_test_data3     (gtx5_test3),
    .gtx5_test_data4     (gtx5_test4),
    .gtx5_test_data5     (gtx5_test5),
    .gtx5_test_data6     (gtx5_test6),
    .gtx5_test_data7     (gtx5_test7),
    .gtx6_test_data1     (gtx6_test1),
    .gtx6_test_data2     (gtx6_test2),
    .gtx6_test_data3     (gtx6_test3),
    .gtx6_test_data4     (gtx6_test4),
    .gtx6_test_data5     (gtx6_test5),
    .gtx6_test_data6     (gtx6_test6),
    .gtx6_test_data7     (gtx6_test7),
    .gtx7_test_data1     (gtx7_test1),
    .gtx7_test_data2     (gtx7_test2),
    .gtx7_test_data3     (gtx7_test3),
    .gtx7_test_data4     (gtx7_test4),
    .gtx7_test_data5     (gtx7_test5),
    .gtx7_test_data6     (gtx7_test6),
    .gtx7_test_data7     (gtx7_test7),
    .gtx8_test_data1     (gtx8_test1),
    .gtx8_test_data2     (gtx8_test2),
    .gtx8_test_data3     (gtx8_test3),
    .gtx8_test_data4     (gtx8_test4),
    .gtx8_test_data5     (gtx8_test5),
    .gtx8_test_data6     (gtx8_test6),
    .gtx8_test_data7     (gtx8_test7),
    .gtx9_test_data1     (gtx9_test1),
    .gtx9_test_data2     (gtx9_test2),
    .gtx9_test_data3     (gtx9_test3),
    .gtx9_test_data4     (gtx9_test4),
    .gtx9_test_data5     (gtx9_test5),
    .gtx9_test_data6     (gtx9_test6),
    .gtx9_test_data7     (gtx9_test7),
    .gtx10_test_data1    (gtx10_test1),
    .gtx10_test_data2    (gtx10_test2),
    .gtx10_test_data3    (gtx10_test3),
    .gtx10_test_data4    (gtx10_test4),
    .gtx10_test_data5    (gtx10_test5),
    .gtx10_test_data6    (gtx10_test6),
    .gtx10_test_data7    (gtx10_test7),
    .gtx11_test_data1    (gtx11_test1),
    .gtx11_test_data2    (gtx11_test2),
    .gtx11_test_data3    (gtx11_test3),
    .gtx11_test_data4    (gtx11_test4),
    .gtx11_test_data5    (gtx11_test5),
    .gtx11_test_data6    (gtx11_test6),
    .gtx11_test_data7    (gtx11_test7),

    .gtx0_phase_select  (gtx0_phase_select),
    .gtx1_phase_select  (gtx1_phase_select),
    .gtx2_phase_select  (gtx2_phase_select),
    .gtx3_phase_select  (gtx3_phase_select),
    .gtx4_phase_select  (gtx4_phase_select),
    .gtx5_phase_select  (gtx5_phase_select),
    .gtx6_phase_select  (gtx6_phase_select),
    .gtx7_phase_select  (gtx7_phase_select),
    .gtx8_phase_select  (gtx8_phase_select),
    .gtx9_phase_select  (gtx9_phase_select),
    .gtx10_phase_select  (gtx10_phase_select),
    .gtx11_phase_select  (gtx11_phase_select),

    .gtx0_phase_monitor_0 (gtx0_phase_monitor_0),
    .gtx1_phase_monitor_0 (gtx1_phase_monitor_0),
    .gtx2_phase_monitor_0 (gtx2_phase_monitor_0),
    .gtx3_phase_monitor_0 (gtx3_phase_monitor_0),
    .gtx4_phase_monitor_0 (gtx4_phase_monitor_0),
    .gtx5_phase_monitor_0 (gtx5_phase_monitor_0),
    .gtx6_phase_monitor_0 (gtx6_phase_monitor_0),
    .gtx7_phase_monitor_0 (gtx7_phase_monitor_0),
    .gtx8_phase_monitor_0 (gtx8_phase_monitor_0),
    .gtx9_phase_monitor_0 (gtx9_phase_monitor_0),
    .gtx10_phase_monitor_0 (gtx10_phase_monitor_0),
    .gtx11_phase_monitor_0 (gtx11_phase_monitor_0),

    .gtx0_phase_monitor_1 (gtx0_phase_monitor_1),
    .gtx1_phase_monitor_1 (gtx1_phase_monitor_1),
    .gtx2_phase_monitor_1 (gtx2_phase_monitor_1),
    .gtx3_phase_monitor_1 (gtx3_phase_monitor_1),
    .gtx4_phase_monitor_1 (gtx4_phase_monitor_1),
    .gtx5_phase_monitor_1 (gtx5_phase_monitor_1),
    .gtx6_phase_monitor_1 (gtx6_phase_monitor_1),
    .gtx7_phase_monitor_1 (gtx7_phase_monitor_1),
    .gtx8_phase_monitor_1 (gtx8_phase_monitor_1),
    .gtx9_phase_monitor_1 (gtx9_phase_monitor_1),
    .gtx10_phase_monitor_1 (gtx10_phase_monitor_1),
    .gtx11_phase_monitor_1 (gtx11_phase_monitor_1),

    .gtx0_phase_monitor_2 (gtx0_phase_monitor_2),
    .gtx1_phase_monitor_2 (gtx1_phase_monitor_2),
    .gtx2_phase_monitor_2 (gtx2_phase_monitor_2),
    .gtx3_phase_monitor_2 (gtx3_phase_monitor_2),
    .gtx4_phase_monitor_2 (gtx4_phase_monitor_2),
    .gtx5_phase_monitor_2 (gtx5_phase_monitor_2),
    .gtx6_phase_monitor_2 (gtx6_phase_monitor_2),
    .gtx7_phase_monitor_2 (gtx7_phase_monitor_2),
    .gtx8_phase_monitor_2 (gtx8_phase_monitor_2),
    .gtx9_phase_monitor_2 (gtx9_phase_monitor_2),
    .gtx10_phase_monitor_2 (gtx10_phase_monitor_2),
    .gtx11_phase_monitor_2 (gtx11_phase_monitor_2),

    .gtx0_phase_monitor_3 (gtx0_phase_monitor_3),
    .gtx1_phase_monitor_3 (gtx1_phase_monitor_3),
    .gtx2_phase_monitor_3 (gtx2_phase_monitor_3),
    .gtx3_phase_monitor_3 (gtx3_phase_monitor_3),
    .gtx4_phase_monitor_3 (gtx4_phase_monitor_3),
    .gtx5_phase_monitor_3 (gtx5_phase_monitor_3),
    .gtx6_phase_monitor_3 (gtx6_phase_monitor_3),
    .gtx7_phase_monitor_3 (gtx7_phase_monitor_3),
    .gtx8_phase_monitor_3 (gtx8_phase_monitor_3),
    .gtx9_phase_monitor_3 (gtx9_phase_monitor_3),
    .gtx10_phase_monitor_3 (gtx10_phase_monitor_3),
    .gtx11_phase_monitor_3 (gtx11_phase_monitor_3),
        
    .glink0_test_data1  (glink0_test1),
    .glink0_test_data2  (glink0_test2),
    .glink1_test_data1  (glink1_test1),
    .glink1_test_data2  (glink1_test2),
    .glink2_test_data1  (glink2_test1),
    .glink2_test_data2  (glink2_test2),
    .glink3_test_data1  (glink3_test1),
    .glink3_test_data2  (glink3_test2),
    .glink4_test_data1  (glink4_test1),
    .glink4_test_data2  (glink4_test2),
    .glink5_test_data1  (glink5_test1),
    .glink5_test_data2  (glink5_test2),
    .glink6_test_data1  (glink6_test1),
    .glink6_test_data2  (glink6_test2),
    .glink7_test_data1  (glink7_test1),
    .glink7_test_data2  (glink7_test2),
    .glink8_test_data1  (glink8_test1),
    .glink8_test_data2  (glink8_test2),
    .glink9_test_data1  (glink9_test1),
    .glink9_test_data2  (glink9_test2),
    .glink10_test_data1 (glink10_test1),
    .glink10_test_data2 (glink10_test2),
    .glink11_test_data1 (glink11_test1),
    .glink11_test_data2 (glink11_test2),
    .glink12_test_data1 (glink12_test1),
    .glink12_test_data2 (glink12_test2),
    .glink13_test_data1 (glink13_test1),
    .glink13_test_data2 (glink13_test2),

    .TrackOutputGenerator_Enb(TrackOutputGenerator_enb),
    .Trackgenerator0_S0_ROI(trackgenerator0_S0_ROI),
    .Trackgenerator1_S0_ROI(trackgenerator1_S0_ROI),
    .Trackgenerator2_S0_ROI(trackgenerator2_S0_ROI),
    .Trackgenerator3_S0_ROI(trackgenerator3_S0_ROI),
    .Trackgenerator0_S0_pT(trackgenerator0_S0_pT),
    .Trackgenerator1_S0_pT(trackgenerator1_S0_pT),
    .Trackgenerator2_S0_pT(trackgenerator2_S0_pT),
    .Trackgenerator3_S0_pT(trackgenerator3_S0_pT),
    .Trackgenerator0_S0_sign(trackgenerator0_S0_sign),
    .Trackgenerator1_S0_sign(trackgenerator1_S0_sign),
    .Trackgenerator2_S0_sign(trackgenerator2_S0_sign),
    .Trackgenerator3_S0_sign(trackgenerator3_S0_sign),
    .Trackgenerator0_S0_InnerCoincidenceFlag(trackgenerator0_S0_InnerCoincidenceFlag),
    .Trackgenerator1_S0_InnerCoincidenceFlag(trackgenerator1_S0_InnerCoincidenceFlag),
    .Trackgenerator2_S0_InnerCoincidenceFlag(trackgenerator2_S0_InnerCoincidenceFlag),
    .Trackgenerator3_S0_InnerCoincidenceFlag(trackgenerator3_S0_InnerCoincidenceFlag),    
    .Flag_trackgenerator_S0(flag_trackgenerator_S0), 
    .Trackgenerator0_S1_ROI(trackgenerator0_S1_ROI),
    .Trackgenerator1_S1_ROI(trackgenerator1_S1_ROI),
    .Trackgenerator2_S1_ROI(trackgenerator2_S1_ROI),
    .Trackgenerator3_S1_ROI(trackgenerator3_S1_ROI),
    .Trackgenerator0_S1_pT(trackgenerator0_S1_pT),
    .Trackgenerator1_S1_pT(trackgenerator1_S1_pT),
    .Trackgenerator2_S1_pT(trackgenerator2_S1_pT),
    .Trackgenerator3_S1_pT(trackgenerator3_S1_pT),
    .Trackgenerator0_S1_sign(trackgenerator0_S1_sign),
    .Trackgenerator1_S1_sign(trackgenerator1_S1_sign),
    .Trackgenerator2_S1_sign(trackgenerator2_S1_sign),
    .Trackgenerator3_S1_sign(trackgenerator3_S1_sign),
    .Trackgenerator0_S1_InnerCoincidenceFlag(trackgenerator0_S1_InnerCoincidenceFlag),
    .Trackgenerator1_S1_InnerCoincidenceFlag(trackgenerator1_S1_InnerCoincidenceFlag),
    .Trackgenerator2_S1_InnerCoincidenceFlag(trackgenerator2_S1_InnerCoincidenceFlag),
    .Trackgenerator3_S1_InnerCoincidenceFlag(trackgenerator3_S1_InnerCoincidenceFlag),    
    .Flag_trackgenerator_S1(flag_trackgenerator_S1),

    .Reset_GLinkMonitor(Reset_GLinkMonitor),
    .GL0_State_Reset(GL0_State_Reset), 
    .GL1_State_Reset(GL1_State_Reset),
    .GL2_State_Reset(GL2_State_Reset),
    .GL3_State_Reset(GL3_State_Reset),
    .GL4_State_Reset(GL4_State_Reset),
    .GL5_State_Reset(GL5_State_Reset),
    .GL6_State_Reset(GL6_State_Reset),
    .GL7_State_Reset(GL7_State_Reset),
    .GL8_State_Reset(GL8_State_Reset),
    .GL9_State_Reset(GL9_State_Reset),
    .GL10_State_Reset(GL10_State_Reset),
    .GL11_State_Reset(GL11_State_Reset),
    .GL12_State_Reset(GL12_State_Reset),
    .GL13_State_Reset(GL13_State_Reset),

    .GL0_ErrorCounter_Reset(GL0_ErrorCounter_Reset), 
    .GL1_ErrorCounter_Reset(GL1_ErrorCounter_Reset),
    .GL2_ErrorCounter_Reset(GL2_ErrorCounter_Reset),
    .GL3_ErrorCounter_Reset(GL3_ErrorCounter_Reset),
    .GL4_ErrorCounter_Reset(GL4_ErrorCounter_Reset),
    .GL5_ErrorCounter_Reset(GL5_ErrorCounter_Reset),
    .GL6_ErrorCounter_Reset(GL6_ErrorCounter_Reset),
    .GL7_ErrorCounter_Reset(GL7_ErrorCounter_Reset),
    .GL8_ErrorCounter_Reset(GL8_ErrorCounter_Reset),
    .GL9_ErrorCounter_Reset(GL9_ErrorCounter_Reset),
    .GL10_ErrorCounter_Reset(GL10_ErrorCounter_Reset),
    .GL11_ErrorCounter_Reset(GL11_ErrorCounter_Reset),
    .GL12_ErrorCounter_Reset(GL12_ErrorCounter_Reset),
    .GL13_ErrorCounter_Reset(GL13_ErrorCounter_Reset),

    .RX_DIVen(RX_DIVen), 
    .GL0_RX_DIV_vme(GL0_RX_DIV_vme), 
    .GL1_RX_DIV_vme(GL1_RX_DIV_vme),
    .GL2_RX_DIV_vme(GL2_RX_DIV_vme),
    .GL3_RX_DIV_vme(GL3_RX_DIV_vme),
    .GL4_RX_DIV_vme(GL4_RX_DIV_vme),
    .GL5_RX_DIV_vme(GL5_RX_DIV_vme),
    .GL6_RX_DIV_vme(GL6_RX_DIV_vme),
    .GL7_RX_DIV_vme(GL7_RX_DIV_vme),
    .GL8_RX_DIV_vme(GL8_RX_DIV_vme),
    .GL9_RX_DIV_vme(GL9_RX_DIV_vme),
    .GL10_RX_DIV_vme(GL10_RX_DIV_vme),
    .GL11_RX_DIV_vme(GL11_RX_DIV_vme),
    .GL12_RX_DIV_vme(GL12_RX_DIV_vme),
    .GL13_RX_DIV_vme(GL13_RX_DIV_vme),
    
    .GL0_State(GL0_State), 
    .GL1_State(GL1_State),
    .GL2_State(GL2_State),
    .GL3_State(GL3_State),
    .GL4_State(GL4_State),
    .GL5_State(GL5_State),
    .GL6_State(GL6_State), 
    .GL7_State(GL7_State),
    .GL8_State(GL8_State),
    .GL9_State(GL9_State),
    .GL10_State(GL10_State),
    .GL11_State(GL11_State),
    .GL12_State(GL12_State),     
    .GL13_State(GL13_State),
    
    .GL0_ErrorCounter(GL0_ErrorCounter), 
    .GL1_ErrorCounter(GL1_ErrorCounter),
    .GL2_ErrorCounter(GL2_ErrorCounter),
    .GL3_ErrorCounter(GL3_ErrorCounter),
    .GL4_ErrorCounter(GL4_ErrorCounter),     
    .GL5_ErrorCounter(GL5_ErrorCounter),
    .GL6_ErrorCounter(GL6_ErrorCounter),
    .GL7_ErrorCounter(GL7_ErrorCounter),
    .GL8_ErrorCounter(GL8_ErrorCounter),
    .GL9_ErrorCounter(GL9_ErrorCounter),
    .GL10_ErrorCounter(GL10_ErrorCounter),    
    .GL11_ErrorCounter(GL11_ErrorCounter),
    .GL12_ErrorCounter(GL12_ErrorCounter),
    .GL13_ErrorCounter(GL13_ErrorCounter)
    
);  


endmodule
