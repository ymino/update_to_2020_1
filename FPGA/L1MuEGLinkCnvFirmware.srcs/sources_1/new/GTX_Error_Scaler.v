`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/08/24 06:49:11
// Design Name: 
// Module Name: GTX_Error_Scaler
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module GTX_Error_Scaler(
    input wire gt0_rxusrclk_in,
    input wire gt1_rxusrclk_in,
    input wire gt2_rxusrclk_in,
    input wire gt3_rxusrclk_in,
    input wire gt4_rxusrclk_in,
    input wire gt5_rxusrclk_in,
    input wire gt6_rxusrclk_in,
    input wire gt7_rxusrclk_in,
    input wire gt8_rxusrclk_in,
    input wire gt9_rxusrclk_in,
    input wire gt10_rxusrclk_in,
    input wire gt11_rxusrclk_in,
    input wire reset_in,
    input wire Scaler_reset_in,
    
    input wire Error_gt0_in,
    input wire Error_gt1_in,
    input wire Error_gt2_in,
    input wire Error_gt3_in,
    input wire Error_gt4_in,
    input wire Error_gt5_in,
    input wire Error_gt6_in,
    input wire Error_gt7_in,
    input wire Error_gt8_in,
    input wire Error_gt9_in,
    input wire Error_gt10_in,
    input wire Error_gt11_in,

    output wire [15:0] ErrorCount_0_out,
    output wire [15:0] ErrorCount_1_out,
    output wire [15:0] ErrorCount_2_out,
    output wire [15:0] ErrorCount_3_out,
    output wire [15:0] ErrorCount_4_out,
    output wire [15:0] ErrorCount_5_out,
    output wire [15:0] ErrorCount_6_out,
    output wire [15:0] ErrorCount_7_out,
    output wire [15:0] ErrorCount_8_out,
    output wire [15:0] ErrorCount_9_out,
    output wire [15:0] ErrorCount_10_out,
    output wire [15:0] ErrorCount_11_out
    );

wire RST_in = reset_in||Scaler_reset_in;
    
Scaler ErrorScaler_gt0(.CLK_in(gt0_rxusrclk_in), .reset_in(RST_in), .data_in(Error_gt0_in), .count_out(ErrorCount_0_out));
Scaler ErrorScaler_gt1(.CLK_in(gt1_rxusrclk_in), .reset_in(RST_in), .data_in(Error_gt1_in), .count_out(ErrorCount_1_out));
Scaler ErrorScaler_gt2(.CLK_in(gt2_rxusrclk_in), .reset_in(RST_in), .data_in(Error_gt2_in), .count_out(ErrorCount_2_out));
Scaler ErrorScaler_gt3(.CLK_in(gt3_rxusrclk_in), .reset_in(RST_in), .data_in(Error_gt3_in), .count_out(ErrorCount_3_out));
Scaler ErrorScaler_gt4(.CLK_in(gt4_rxusrclk_in), .reset_in(RST_in), .data_in(Error_gt4_in), .count_out(ErrorCount_4_out));
Scaler ErrorScaler_gt5(.CLK_in(gt5_rxusrclk_in), .reset_in(RST_in), .data_in(Error_gt5_in), .count_out(ErrorCount_5_out));
Scaler ErrorScaler_gt6(.CLK_in(gt6_rxusrclk_in), .reset_in(RST_in), .data_in(Error_gt6_in), .count_out(ErrorCount_6_out));
Scaler ErrorScaler_gt7(.CLK_in(gt7_rxusrclk_in), .reset_in(RST_in), .data_in(Error_gt7_in), .count_out(ErrorCount_7_out));
Scaler ErrorScaler_gt8(.CLK_in(gt8_rxusrclk_in), .reset_in(RST_in), .data_in(Error_gt8_in), .count_out(ErrorCount_8_out));
Scaler ErrorScaler_gt9(.CLK_in(gt9_rxusrclk_in), .reset_in(RST_in), .data_in(Error_gt9_in), .count_out(ErrorCount_9_out));
Scaler ErrorScaler_gt10(.CLK_in(gt10_rxusrclk_in), .reset_in(RST_in), .data_in(Error_gt10_in), .count_out(ErrorCount_10_out));
Scaler ErrorScaler_gt11(.CLK_in(gt11_rxusrclk_in), .reset_in(RST_in), .data_in(Error_gt11_in), .count_out(ErrorCount_11_out));

endmodule
