-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (win64) Build 2902540 Wed May 27 19:54:49 MDT 2020
-- Date        : Thu Sep 17 14:30:04 2020
-- Host        : koichiro-PC running 64-bit Service Pack 1  (build 7601)
-- Command     : write_vhdl -force -mode synth_stub
--               c:/Users/tsujikawa/SLFirmware/sandbox/update_2020_1/FPGA/L1MuEGLinkCnvFirmware.srcs/sources_1/ip/dcm40_to_4phases/dcm40_to_4phases_stub.vhdl
-- Design      : dcm40_to_4phases
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7k410tffg900-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity dcm40_to_4phases is
  Port ( 
    clk40_0 : out STD_LOGIC;
    clk40_1 : out STD_LOGIC;
    clk40_2 : out STD_LOGIC;
    clk40_3 : out STD_LOGIC;
    reset : in STD_LOGIC;
    locked : out STD_LOGIC;
    clk_in : in STD_LOGIC
  );

end dcm40_to_4phases;

architecture stub of dcm40_to_4phases is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk40_0,clk40_1,clk40_2,clk40_3,reset,locked,clk_in";
begin
end;
