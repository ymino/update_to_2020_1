// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Mon Jul  6 17:35:53 2020
// Host        : koichiro-PC running 64-bit Service Pack 1  (build 7601)
// Command     : write_verilog -force -mode synth_stub
//               C:/Users/tsujikawa/Downloads/L1MuEGLinkCnvFirmware-develop/FPGA/L1MuEGLinkCnvFirmware.srcs/sources_1/ip/GTX_6_4Gbps_12lane/GTX_6_4Gbps_12lane_stub.v
// Design      : GTX_6_4Gbps_12lane
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7k410tffg900-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "GTX_6_4Gbps_12lane,gtwizard_v3_6_11,{protocol_file=aurora_8b10b_multi_lane_4byte}" *)
module GTX_6_4Gbps_12lane(sysclk_in, soft_reset_tx_in, 
  soft_reset_rx_in, dont_reset_on_data_error_in, gt0_tx_fsm_reset_done_out, 
  gt0_rx_fsm_reset_done_out, gt0_data_valid_in, gt1_tx_fsm_reset_done_out, 
  gt1_rx_fsm_reset_done_out, gt1_data_valid_in, gt2_tx_fsm_reset_done_out, 
  gt2_rx_fsm_reset_done_out, gt2_data_valid_in, gt3_tx_fsm_reset_done_out, 
  gt3_rx_fsm_reset_done_out, gt3_data_valid_in, gt4_tx_fsm_reset_done_out, 
  gt4_rx_fsm_reset_done_out, gt4_data_valid_in, gt5_tx_fsm_reset_done_out, 
  gt5_rx_fsm_reset_done_out, gt5_data_valid_in, gt6_tx_fsm_reset_done_out, 
  gt6_rx_fsm_reset_done_out, gt6_data_valid_in, gt7_tx_fsm_reset_done_out, 
  gt7_rx_fsm_reset_done_out, gt7_data_valid_in, gt8_tx_fsm_reset_done_out, 
  gt8_rx_fsm_reset_done_out, gt8_data_valid_in, gt9_tx_fsm_reset_done_out, 
  gt9_rx_fsm_reset_done_out, gt9_data_valid_in, gt10_tx_fsm_reset_done_out, 
  gt10_rx_fsm_reset_done_out, gt10_data_valid_in, gt11_tx_fsm_reset_done_out, 
  gt11_rx_fsm_reset_done_out, gt11_data_valid_in, gt0_cpllfbclklost_out, 
  gt0_cplllock_out, gt0_cplllockdetclk_in, gt0_cpllreset_in, gt0_gtrefclk0_in, 
  gt0_gtrefclk1_in, gt0_drpaddr_in, gt0_drpclk_in, gt0_drpdi_in, gt0_drpdo_out, gt0_drpen_in, 
  gt0_drprdy_out, gt0_drpwe_in, gt0_dmonitorout_out, gt0_loopback_in, gt0_rxpd_in, 
  gt0_txpd_in, gt0_eyescanreset_in, gt0_rxuserrdy_in, gt0_eyescandataerror_out, 
  gt0_eyescantrigger_in, gt0_rxcdrhold_in, gt0_rxcdrovrden_in, gt0_rxusrclk_in, 
  gt0_rxusrclk2_in, gt0_rxdata_out, gt0_rxprbserr_out, gt0_rxprbssel_in, 
  gt0_rxprbscntreset_in, gt0_rxdisperr_out, gt0_rxnotintable_out, gt0_gtxrxp_in, 
  gt0_gtxrxn_in, gt0_rxbufreset_in, gt0_rxbufstatus_out, gt0_rxphmonitor_out, 
  gt0_rxphslipmonitor_out, gt0_rxbyteisaligned_out, gt0_rxbyterealign_out, 
  gt0_rxcommadet_out, gt0_rxmcommaalignen_in, gt0_rxpcommaalignen_in, 
  gt0_rxdfelpmreset_in, gt0_rxmonitorout_out, gt0_rxmonitorsel_in, gt0_rxoutclk_out, 
  gt0_rxoutclkfabric_out, gt0_gtrxreset_in, gt0_rxpcsreset_in, gt0_rxpmareset_in, 
  gt0_rxlpmen_in, gt0_rxpolarity_in, gt0_rxchariscomma_out, gt0_rxcharisk_out, 
  gt0_rxresetdone_out, gt0_txpostcursor_in, gt0_txprecursor_in, gt0_gttxreset_in, 
  gt0_txuserrdy_in, gt0_txchardispmode_in, gt0_txchardispval_in, gt0_txusrclk_in, 
  gt0_txusrclk2_in, gt0_txprbsforceerr_in, gt0_txbufstatus_out, gt0_txdiffctrl_in, 
  gt0_txmaincursor_in, gt0_txdata_in, gt0_gtxtxn_out, gt0_gtxtxp_out, gt0_txoutclk_out, 
  gt0_txoutclkfabric_out, gt0_txoutclkpcs_out, gt0_txcharisk_in, gt0_txpcsreset_in, 
  gt0_txpmareset_in, gt0_txresetdone_out, gt0_txpolarity_in, gt0_txprbssel_in, 
  gt1_cpllfbclklost_out, gt1_cplllock_out, gt1_cplllockdetclk_in, gt1_cpllreset_in, 
  gt1_gtrefclk0_in, gt1_gtrefclk1_in, gt1_drpaddr_in, gt1_drpclk_in, gt1_drpdi_in, 
  gt1_drpdo_out, gt1_drpen_in, gt1_drprdy_out, gt1_drpwe_in, gt1_dmonitorout_out, 
  gt1_loopback_in, gt1_rxpd_in, gt1_txpd_in, gt1_eyescanreset_in, gt1_rxuserrdy_in, 
  gt1_eyescandataerror_out, gt1_eyescantrigger_in, gt1_rxcdrhold_in, gt1_rxcdrovrden_in, 
  gt1_rxusrclk_in, gt1_rxusrclk2_in, gt1_rxdata_out, gt1_rxprbserr_out, gt1_rxprbssel_in, 
  gt1_rxprbscntreset_in, gt1_rxdisperr_out, gt1_rxnotintable_out, gt1_gtxrxp_in, 
  gt1_gtxrxn_in, gt1_rxbufreset_in, gt1_rxbufstatus_out, gt1_rxphmonitor_out, 
  gt1_rxphslipmonitor_out, gt1_rxbyteisaligned_out, gt1_rxbyterealign_out, 
  gt1_rxcommadet_out, gt1_rxmcommaalignen_in, gt1_rxpcommaalignen_in, 
  gt1_rxdfelpmreset_in, gt1_rxmonitorout_out, gt1_rxmonitorsel_in, gt1_rxoutclk_out, 
  gt1_rxoutclkfabric_out, gt1_gtrxreset_in, gt1_rxpcsreset_in, gt1_rxpmareset_in, 
  gt1_rxlpmen_in, gt1_rxpolarity_in, gt1_rxchariscomma_out, gt1_rxcharisk_out, 
  gt1_rxresetdone_out, gt1_txpostcursor_in, gt1_txprecursor_in, gt1_gttxreset_in, 
  gt1_txuserrdy_in, gt1_txchardispmode_in, gt1_txchardispval_in, gt1_txusrclk_in, 
  gt1_txusrclk2_in, gt1_txprbsforceerr_in, gt1_txbufstatus_out, gt1_txdiffctrl_in, 
  gt1_txmaincursor_in, gt1_txdata_in, gt1_gtxtxn_out, gt1_gtxtxp_out, gt1_txoutclk_out, 
  gt1_txoutclkfabric_out, gt1_txoutclkpcs_out, gt1_txcharisk_in, gt1_txpcsreset_in, 
  gt1_txpmareset_in, gt1_txresetdone_out, gt1_txpolarity_in, gt1_txprbssel_in, 
  gt2_cpllfbclklost_out, gt2_cplllock_out, gt2_cplllockdetclk_in, gt2_cpllreset_in, 
  gt2_gtrefclk0_in, gt2_gtrefclk1_in, gt2_drpaddr_in, gt2_drpclk_in, gt2_drpdi_in, 
  gt2_drpdo_out, gt2_drpen_in, gt2_drprdy_out, gt2_drpwe_in, gt2_dmonitorout_out, 
  gt2_loopback_in, gt2_rxpd_in, gt2_txpd_in, gt2_eyescanreset_in, gt2_rxuserrdy_in, 
  gt2_eyescandataerror_out, gt2_eyescantrigger_in, gt2_rxcdrhold_in, gt2_rxcdrovrden_in, 
  gt2_rxusrclk_in, gt2_rxusrclk2_in, gt2_rxdata_out, gt2_rxprbserr_out, gt2_rxprbssel_in, 
  gt2_rxprbscntreset_in, gt2_rxdisperr_out, gt2_rxnotintable_out, gt2_gtxrxp_in, 
  gt2_gtxrxn_in, gt2_rxbufreset_in, gt2_rxbufstatus_out, gt2_rxphmonitor_out, 
  gt2_rxphslipmonitor_out, gt2_rxbyteisaligned_out, gt2_rxbyterealign_out, 
  gt2_rxcommadet_out, gt2_rxmcommaalignen_in, gt2_rxpcommaalignen_in, 
  gt2_rxdfelpmreset_in, gt2_rxmonitorout_out, gt2_rxmonitorsel_in, gt2_rxoutclk_out, 
  gt2_rxoutclkfabric_out, gt2_gtrxreset_in, gt2_rxpcsreset_in, gt2_rxpmareset_in, 
  gt2_rxlpmen_in, gt2_rxpolarity_in, gt2_rxchariscomma_out, gt2_rxcharisk_out, 
  gt2_rxresetdone_out, gt2_txpostcursor_in, gt2_txprecursor_in, gt2_gttxreset_in, 
  gt2_txuserrdy_in, gt2_txchardispmode_in, gt2_txchardispval_in, gt2_txusrclk_in, 
  gt2_txusrclk2_in, gt2_txprbsforceerr_in, gt2_txbufstatus_out, gt2_txdiffctrl_in, 
  gt2_txmaincursor_in, gt2_txdata_in, gt2_gtxtxn_out, gt2_gtxtxp_out, gt2_txoutclk_out, 
  gt2_txoutclkfabric_out, gt2_txoutclkpcs_out, gt2_txcharisk_in, gt2_txpcsreset_in, 
  gt2_txpmareset_in, gt2_txresetdone_out, gt2_txpolarity_in, gt2_txprbssel_in, 
  gt3_cpllfbclklost_out, gt3_cplllock_out, gt3_cplllockdetclk_in, gt3_cpllreset_in, 
  gt3_gtrefclk0_in, gt3_gtrefclk1_in, gt3_drpaddr_in, gt3_drpclk_in, gt3_drpdi_in, 
  gt3_drpdo_out, gt3_drpen_in, gt3_drprdy_out, gt3_drpwe_in, gt3_dmonitorout_out, 
  gt3_loopback_in, gt3_rxpd_in, gt3_txpd_in, gt3_eyescanreset_in, gt3_rxuserrdy_in, 
  gt3_eyescandataerror_out, gt3_eyescantrigger_in, gt3_rxcdrhold_in, gt3_rxcdrovrden_in, 
  gt3_rxusrclk_in, gt3_rxusrclk2_in, gt3_rxdata_out, gt3_rxprbserr_out, gt3_rxprbssel_in, 
  gt3_rxprbscntreset_in, gt3_rxdisperr_out, gt3_rxnotintable_out, gt3_gtxrxp_in, 
  gt3_gtxrxn_in, gt3_rxbufreset_in, gt3_rxbufstatus_out, gt3_rxphmonitor_out, 
  gt3_rxphslipmonitor_out, gt3_rxbyteisaligned_out, gt3_rxbyterealign_out, 
  gt3_rxcommadet_out, gt3_rxmcommaalignen_in, gt3_rxpcommaalignen_in, 
  gt3_rxdfelpmreset_in, gt3_rxmonitorout_out, gt3_rxmonitorsel_in, gt3_rxoutclk_out, 
  gt3_rxoutclkfabric_out, gt3_gtrxreset_in, gt3_rxpcsreset_in, gt3_rxpmareset_in, 
  gt3_rxlpmen_in, gt3_rxpolarity_in, gt3_rxchariscomma_out, gt3_rxcharisk_out, 
  gt3_rxresetdone_out, gt3_txpostcursor_in, gt3_txprecursor_in, gt3_gttxreset_in, 
  gt3_txuserrdy_in, gt3_txchardispmode_in, gt3_txchardispval_in, gt3_txusrclk_in, 
  gt3_txusrclk2_in, gt3_txprbsforceerr_in, gt3_txbufstatus_out, gt3_txdiffctrl_in, 
  gt3_txmaincursor_in, gt3_txdata_in, gt3_gtxtxn_out, gt3_gtxtxp_out, gt3_txoutclk_out, 
  gt3_txoutclkfabric_out, gt3_txoutclkpcs_out, gt3_txcharisk_in, gt3_txpcsreset_in, 
  gt3_txpmareset_in, gt3_txresetdone_out, gt3_txpolarity_in, gt3_txprbssel_in, 
  gt4_cpllfbclklost_out, gt4_cplllock_out, gt4_cplllockdetclk_in, gt4_cpllreset_in, 
  gt4_gtrefclk0_in, gt4_gtrefclk1_in, gt4_drpaddr_in, gt4_drpclk_in, gt4_drpdi_in, 
  gt4_drpdo_out, gt4_drpen_in, gt4_drprdy_out, gt4_drpwe_in, gt4_dmonitorout_out, 
  gt4_loopback_in, gt4_rxpd_in, gt4_txpd_in, gt4_eyescanreset_in, gt4_rxuserrdy_in, 
  gt4_eyescandataerror_out, gt4_eyescantrigger_in, gt4_rxcdrhold_in, gt4_rxcdrovrden_in, 
  gt4_rxusrclk_in, gt4_rxusrclk2_in, gt4_rxdata_out, gt4_rxprbserr_out, gt4_rxprbssel_in, 
  gt4_rxprbscntreset_in, gt4_rxdisperr_out, gt4_rxnotintable_out, gt4_gtxrxp_in, 
  gt4_gtxrxn_in, gt4_rxbufreset_in, gt4_rxbufstatus_out, gt4_rxphmonitor_out, 
  gt4_rxphslipmonitor_out, gt4_rxbyteisaligned_out, gt4_rxbyterealign_out, 
  gt4_rxcommadet_out, gt4_rxmcommaalignen_in, gt4_rxpcommaalignen_in, 
  gt4_rxdfelpmreset_in, gt4_rxmonitorout_out, gt4_rxmonitorsel_in, gt4_rxoutclk_out, 
  gt4_rxoutclkfabric_out, gt4_gtrxreset_in, gt4_rxpcsreset_in, gt4_rxpmareset_in, 
  gt4_rxlpmen_in, gt4_rxpolarity_in, gt4_rxchariscomma_out, gt4_rxcharisk_out, 
  gt4_rxresetdone_out, gt4_txpostcursor_in, gt4_txprecursor_in, gt4_gttxreset_in, 
  gt4_txuserrdy_in, gt4_txchardispmode_in, gt4_txchardispval_in, gt4_txusrclk_in, 
  gt4_txusrclk2_in, gt4_txprbsforceerr_in, gt4_txbufstatus_out, gt4_txdiffctrl_in, 
  gt4_txmaincursor_in, gt4_txdata_in, gt4_gtxtxn_out, gt4_gtxtxp_out, gt4_txoutclk_out, 
  gt4_txoutclkfabric_out, gt4_txoutclkpcs_out, gt4_txcharisk_in, gt4_txpcsreset_in, 
  gt4_txpmareset_in, gt4_txresetdone_out, gt4_txpolarity_in, gt4_txprbssel_in, 
  gt5_cpllfbclklost_out, gt5_cplllock_out, gt5_cplllockdetclk_in, gt5_cpllreset_in, 
  gt5_gtrefclk0_in, gt5_gtrefclk1_in, gt5_drpaddr_in, gt5_drpclk_in, gt5_drpdi_in, 
  gt5_drpdo_out, gt5_drpen_in, gt5_drprdy_out, gt5_drpwe_in, gt5_dmonitorout_out, 
  gt5_loopback_in, gt5_rxpd_in, gt5_txpd_in, gt5_eyescanreset_in, gt5_rxuserrdy_in, 
  gt5_eyescandataerror_out, gt5_eyescantrigger_in, gt5_rxcdrhold_in, gt5_rxcdrovrden_in, 
  gt5_rxusrclk_in, gt5_rxusrclk2_in, gt5_rxdata_out, gt5_rxprbserr_out, gt5_rxprbssel_in, 
  gt5_rxprbscntreset_in, gt5_rxdisperr_out, gt5_rxnotintable_out, gt5_gtxrxp_in, 
  gt5_gtxrxn_in, gt5_rxbufreset_in, gt5_rxbufstatus_out, gt5_rxphmonitor_out, 
  gt5_rxphslipmonitor_out, gt5_rxbyteisaligned_out, gt5_rxbyterealign_out, 
  gt5_rxcommadet_out, gt5_rxmcommaalignen_in, gt5_rxpcommaalignen_in, 
  gt5_rxdfelpmreset_in, gt5_rxmonitorout_out, gt5_rxmonitorsel_in, gt5_rxoutclk_out, 
  gt5_rxoutclkfabric_out, gt5_gtrxreset_in, gt5_rxpcsreset_in, gt5_rxpmareset_in, 
  gt5_rxlpmen_in, gt5_rxpolarity_in, gt5_rxchariscomma_out, gt5_rxcharisk_out, 
  gt5_rxresetdone_out, gt5_txpostcursor_in, gt5_txprecursor_in, gt5_gttxreset_in, 
  gt5_txuserrdy_in, gt5_txchardispmode_in, gt5_txchardispval_in, gt5_txusrclk_in, 
  gt5_txusrclk2_in, gt5_txprbsforceerr_in, gt5_txbufstatus_out, gt5_txdiffctrl_in, 
  gt5_txmaincursor_in, gt5_txdata_in, gt5_gtxtxn_out, gt5_gtxtxp_out, gt5_txoutclk_out, 
  gt5_txoutclkfabric_out, gt5_txoutclkpcs_out, gt5_txcharisk_in, gt5_txpcsreset_in, 
  gt5_txpmareset_in, gt5_txresetdone_out, gt5_txpolarity_in, gt5_txprbssel_in, 
  gt6_cpllfbclklost_out, gt6_cplllock_out, gt6_cplllockdetclk_in, gt6_cpllreset_in, 
  gt6_gtrefclk0_in, gt6_gtrefclk1_in, gt6_drpaddr_in, gt6_drpclk_in, gt6_drpdi_in, 
  gt6_drpdo_out, gt6_drpen_in, gt6_drprdy_out, gt6_drpwe_in, gt6_dmonitorout_out, 
  gt6_loopback_in, gt6_rxpd_in, gt6_txpd_in, gt6_eyescanreset_in, gt6_rxuserrdy_in, 
  gt6_eyescandataerror_out, gt6_eyescantrigger_in, gt6_rxcdrhold_in, gt6_rxcdrovrden_in, 
  gt6_rxusrclk_in, gt6_rxusrclk2_in, gt6_rxdata_out, gt6_rxprbserr_out, gt6_rxprbssel_in, 
  gt6_rxprbscntreset_in, gt6_rxdisperr_out, gt6_rxnotintable_out, gt6_gtxrxp_in, 
  gt6_gtxrxn_in, gt6_rxbufreset_in, gt6_rxbufstatus_out, gt6_rxphmonitor_out, 
  gt6_rxphslipmonitor_out, gt6_rxbyteisaligned_out, gt6_rxbyterealign_out, 
  gt6_rxcommadet_out, gt6_rxmcommaalignen_in, gt6_rxpcommaalignen_in, 
  gt6_rxdfelpmreset_in, gt6_rxmonitorout_out, gt6_rxmonitorsel_in, gt6_rxoutclk_out, 
  gt6_rxoutclkfabric_out, gt6_gtrxreset_in, gt6_rxpcsreset_in, gt6_rxpmareset_in, 
  gt6_rxlpmen_in, gt6_rxpolarity_in, gt6_rxchariscomma_out, gt6_rxcharisk_out, 
  gt6_rxresetdone_out, gt6_txpostcursor_in, gt6_txprecursor_in, gt6_gttxreset_in, 
  gt6_txuserrdy_in, gt6_txchardispmode_in, gt6_txchardispval_in, gt6_txusrclk_in, 
  gt6_txusrclk2_in, gt6_txprbsforceerr_in, gt6_txbufstatus_out, gt6_txdiffctrl_in, 
  gt6_txmaincursor_in, gt6_txdata_in, gt6_gtxtxn_out, gt6_gtxtxp_out, gt6_txoutclk_out, 
  gt6_txoutclkfabric_out, gt6_txoutclkpcs_out, gt6_txcharisk_in, gt6_txpcsreset_in, 
  gt6_txpmareset_in, gt6_txresetdone_out, gt6_txpolarity_in, gt6_txprbssel_in, 
  gt7_cpllfbclklost_out, gt7_cplllock_out, gt7_cplllockdetclk_in, gt7_cpllreset_in, 
  gt7_gtrefclk0_in, gt7_gtrefclk1_in, gt7_drpaddr_in, gt7_drpclk_in, gt7_drpdi_in, 
  gt7_drpdo_out, gt7_drpen_in, gt7_drprdy_out, gt7_drpwe_in, gt7_dmonitorout_out, 
  gt7_loopback_in, gt7_rxpd_in, gt7_txpd_in, gt7_eyescanreset_in, gt7_rxuserrdy_in, 
  gt7_eyescandataerror_out, gt7_eyescantrigger_in, gt7_rxcdrhold_in, gt7_rxcdrovrden_in, 
  gt7_rxusrclk_in, gt7_rxusrclk2_in, gt7_rxdata_out, gt7_rxprbserr_out, gt7_rxprbssel_in, 
  gt7_rxprbscntreset_in, gt7_rxdisperr_out, gt7_rxnotintable_out, gt7_gtxrxp_in, 
  gt7_gtxrxn_in, gt7_rxbufreset_in, gt7_rxbufstatus_out, gt7_rxphmonitor_out, 
  gt7_rxphslipmonitor_out, gt7_rxbyteisaligned_out, gt7_rxbyterealign_out, 
  gt7_rxcommadet_out, gt7_rxmcommaalignen_in, gt7_rxpcommaalignen_in, 
  gt7_rxdfelpmreset_in, gt7_rxmonitorout_out, gt7_rxmonitorsel_in, gt7_rxoutclk_out, 
  gt7_rxoutclkfabric_out, gt7_gtrxreset_in, gt7_rxpcsreset_in, gt7_rxpmareset_in, 
  gt7_rxlpmen_in, gt7_rxpolarity_in, gt7_rxchariscomma_out, gt7_rxcharisk_out, 
  gt7_rxresetdone_out, gt7_txpostcursor_in, gt7_txprecursor_in, gt7_gttxreset_in, 
  gt7_txuserrdy_in, gt7_txchardispmode_in, gt7_txchardispval_in, gt7_txusrclk_in, 
  gt7_txusrclk2_in, gt7_txprbsforceerr_in, gt7_txbufstatus_out, gt7_txdiffctrl_in, 
  gt7_txmaincursor_in, gt7_txdata_in, gt7_gtxtxn_out, gt7_gtxtxp_out, gt7_txoutclk_out, 
  gt7_txoutclkfabric_out, gt7_txoutclkpcs_out, gt7_txcharisk_in, gt7_txpcsreset_in, 
  gt7_txpmareset_in, gt7_txresetdone_out, gt7_txpolarity_in, gt7_txprbssel_in, 
  gt8_cpllfbclklost_out, gt8_cplllock_out, gt8_cplllockdetclk_in, gt8_cpllreset_in, 
  gt8_gtrefclk0_in, gt8_gtrefclk1_in, gt8_drpaddr_in, gt8_drpclk_in, gt8_drpdi_in, 
  gt8_drpdo_out, gt8_drpen_in, gt8_drprdy_out, gt8_drpwe_in, gt8_dmonitorout_out, 
  gt8_loopback_in, gt8_rxpd_in, gt8_txpd_in, gt8_eyescanreset_in, gt8_rxuserrdy_in, 
  gt8_eyescandataerror_out, gt8_eyescantrigger_in, gt8_rxcdrhold_in, gt8_rxcdrovrden_in, 
  gt8_rxusrclk_in, gt8_rxusrclk2_in, gt8_rxdata_out, gt8_rxprbserr_out, gt8_rxprbssel_in, 
  gt8_rxprbscntreset_in, gt8_rxdisperr_out, gt8_rxnotintable_out, gt8_gtxrxp_in, 
  gt8_gtxrxn_in, gt8_rxbufreset_in, gt8_rxbufstatus_out, gt8_rxphmonitor_out, 
  gt8_rxphslipmonitor_out, gt8_rxbyteisaligned_out, gt8_rxbyterealign_out, 
  gt8_rxcommadet_out, gt8_rxmcommaalignen_in, gt8_rxpcommaalignen_in, 
  gt8_rxdfelpmreset_in, gt8_rxmonitorout_out, gt8_rxmonitorsel_in, gt8_rxoutclk_out, 
  gt8_rxoutclkfabric_out, gt8_gtrxreset_in, gt8_rxpcsreset_in, gt8_rxpmareset_in, 
  gt8_rxlpmen_in, gt8_rxpolarity_in, gt8_rxchariscomma_out, gt8_rxcharisk_out, 
  gt8_rxresetdone_out, gt8_txpostcursor_in, gt8_txprecursor_in, gt8_gttxreset_in, 
  gt8_txuserrdy_in, gt8_txchardispmode_in, gt8_txchardispval_in, gt8_txusrclk_in, 
  gt8_txusrclk2_in, gt8_txprbsforceerr_in, gt8_txbufstatus_out, gt8_txdiffctrl_in, 
  gt8_txmaincursor_in, gt8_txdata_in, gt8_gtxtxn_out, gt8_gtxtxp_out, gt8_txoutclk_out, 
  gt8_txoutclkfabric_out, gt8_txoutclkpcs_out, gt8_txcharisk_in, gt8_txpcsreset_in, 
  gt8_txpmareset_in, gt8_txresetdone_out, gt8_txpolarity_in, gt8_txprbssel_in, 
  gt9_cpllfbclklost_out, gt9_cplllock_out, gt9_cplllockdetclk_in, gt9_cpllreset_in, 
  gt9_gtrefclk0_in, gt9_gtrefclk1_in, gt9_drpaddr_in, gt9_drpclk_in, gt9_drpdi_in, 
  gt9_drpdo_out, gt9_drpen_in, gt9_drprdy_out, gt9_drpwe_in, gt9_dmonitorout_out, 
  gt9_loopback_in, gt9_rxpd_in, gt9_txpd_in, gt9_eyescanreset_in, gt9_rxuserrdy_in, 
  gt9_eyescandataerror_out, gt9_eyescantrigger_in, gt9_rxcdrhold_in, gt9_rxcdrovrden_in, 
  gt9_rxusrclk_in, gt9_rxusrclk2_in, gt9_rxdata_out, gt9_rxprbserr_out, gt9_rxprbssel_in, 
  gt9_rxprbscntreset_in, gt9_rxdisperr_out, gt9_rxnotintable_out, gt9_gtxrxp_in, 
  gt9_gtxrxn_in, gt9_rxbufreset_in, gt9_rxbufstatus_out, gt9_rxphmonitor_out, 
  gt9_rxphslipmonitor_out, gt9_rxbyteisaligned_out, gt9_rxbyterealign_out, 
  gt9_rxcommadet_out, gt9_rxmcommaalignen_in, gt9_rxpcommaalignen_in, 
  gt9_rxdfelpmreset_in, gt9_rxmonitorout_out, gt9_rxmonitorsel_in, gt9_rxoutclk_out, 
  gt9_rxoutclkfabric_out, gt9_gtrxreset_in, gt9_rxpcsreset_in, gt9_rxpmareset_in, 
  gt9_rxlpmen_in, gt9_rxpolarity_in, gt9_rxchariscomma_out, gt9_rxcharisk_out, 
  gt9_rxresetdone_out, gt9_txpostcursor_in, gt9_txprecursor_in, gt9_gttxreset_in, 
  gt9_txuserrdy_in, gt9_txchardispmode_in, gt9_txchardispval_in, gt9_txusrclk_in, 
  gt9_txusrclk2_in, gt9_txprbsforceerr_in, gt9_txbufstatus_out, gt9_txdiffctrl_in, 
  gt9_txmaincursor_in, gt9_txdata_in, gt9_gtxtxn_out, gt9_gtxtxp_out, gt9_txoutclk_out, 
  gt9_txoutclkfabric_out, gt9_txoutclkpcs_out, gt9_txcharisk_in, gt9_txpcsreset_in, 
  gt9_txpmareset_in, gt9_txresetdone_out, gt9_txpolarity_in, gt9_txprbssel_in, 
  gt10_cpllfbclklost_out, gt10_cplllock_out, gt10_cplllockdetclk_in, gt10_cpllreset_in, 
  gt10_gtrefclk0_in, gt10_gtrefclk1_in, gt10_drpaddr_in, gt10_drpclk_in, gt10_drpdi_in, 
  gt10_drpdo_out, gt10_drpen_in, gt10_drprdy_out, gt10_drpwe_in, gt10_dmonitorout_out, 
  gt10_loopback_in, gt10_rxpd_in, gt10_txpd_in, gt10_eyescanreset_in, gt10_rxuserrdy_in, 
  gt10_eyescandataerror_out, gt10_eyescantrigger_in, gt10_rxcdrhold_in, 
  gt10_rxcdrovrden_in, gt10_rxusrclk_in, gt10_rxusrclk2_in, gt10_rxdata_out, 
  gt10_rxprbserr_out, gt10_rxprbssel_in, gt10_rxprbscntreset_in, gt10_rxdisperr_out, 
  gt10_rxnotintable_out, gt10_gtxrxp_in, gt10_gtxrxn_in, gt10_rxbufreset_in, 
  gt10_rxbufstatus_out, gt10_rxphmonitor_out, gt10_rxphslipmonitor_out, 
  gt10_rxbyteisaligned_out, gt10_rxbyterealign_out, gt10_rxcommadet_out, 
  gt10_rxmcommaalignen_in, gt10_rxpcommaalignen_in, gt10_rxdfelpmreset_in, 
  gt10_rxmonitorout_out, gt10_rxmonitorsel_in, gt10_rxoutclk_out, 
  gt10_rxoutclkfabric_out, gt10_gtrxreset_in, gt10_rxpcsreset_in, gt10_rxpmareset_in, 
  gt10_rxlpmen_in, gt10_rxpolarity_in, gt10_rxchariscomma_out, gt10_rxcharisk_out, 
  gt10_rxresetdone_out, gt10_txpostcursor_in, gt10_txprecursor_in, gt10_gttxreset_in, 
  gt10_txuserrdy_in, gt10_txchardispmode_in, gt10_txchardispval_in, gt10_txusrclk_in, 
  gt10_txusrclk2_in, gt10_txprbsforceerr_in, gt10_txbufstatus_out, gt10_txdiffctrl_in, 
  gt10_txmaincursor_in, gt10_txdata_in, gt10_gtxtxn_out, gt10_gtxtxp_out, 
  gt10_txoutclk_out, gt10_txoutclkfabric_out, gt10_txoutclkpcs_out, gt10_txcharisk_in, 
  gt10_txpcsreset_in, gt10_txpmareset_in, gt10_txresetdone_out, gt10_txpolarity_in, 
  gt10_txprbssel_in, gt11_cpllfbclklost_out, gt11_cplllock_out, gt11_cplllockdetclk_in, 
  gt11_cpllreset_in, gt11_gtrefclk0_in, gt11_gtrefclk1_in, gt11_drpaddr_in, gt11_drpclk_in, 
  gt11_drpdi_in, gt11_drpdo_out, gt11_drpen_in, gt11_drprdy_out, gt11_drpwe_in, 
  gt11_dmonitorout_out, gt11_loopback_in, gt11_rxpd_in, gt11_txpd_in, gt11_eyescanreset_in, 
  gt11_rxuserrdy_in, gt11_eyescandataerror_out, gt11_eyescantrigger_in, 
  gt11_rxcdrhold_in, gt11_rxcdrovrden_in, gt11_rxusrclk_in, gt11_rxusrclk2_in, 
  gt11_rxdata_out, gt11_rxprbserr_out, gt11_rxprbssel_in, gt11_rxprbscntreset_in, 
  gt11_rxdisperr_out, gt11_rxnotintable_out, gt11_gtxrxp_in, gt11_gtxrxn_in, 
  gt11_rxbufreset_in, gt11_rxbufstatus_out, gt11_rxphmonitor_out, 
  gt11_rxphslipmonitor_out, gt11_rxbyteisaligned_out, gt11_rxbyterealign_out, 
  gt11_rxcommadet_out, gt11_rxmcommaalignen_in, gt11_rxpcommaalignen_in, 
  gt11_rxdfelpmreset_in, gt11_rxmonitorout_out, gt11_rxmonitorsel_in, gt11_rxoutclk_out, 
  gt11_rxoutclkfabric_out, gt11_gtrxreset_in, gt11_rxpcsreset_in, gt11_rxpmareset_in, 
  gt11_rxlpmen_in, gt11_rxpolarity_in, gt11_rxchariscomma_out, gt11_rxcharisk_out, 
  gt11_rxresetdone_out, gt11_txpostcursor_in, gt11_txprecursor_in, gt11_gttxreset_in, 
  gt11_txuserrdy_in, gt11_txchardispmode_in, gt11_txchardispval_in, gt11_txusrclk_in, 
  gt11_txusrclk2_in, gt11_txprbsforceerr_in, gt11_txbufstatus_out, gt11_txdiffctrl_in, 
  gt11_txmaincursor_in, gt11_txdata_in, gt11_gtxtxn_out, gt11_gtxtxp_out, 
  gt11_txoutclk_out, gt11_txoutclkfabric_out, gt11_txoutclkpcs_out, gt11_txcharisk_in, 
  gt11_txpcsreset_in, gt11_txpmareset_in, gt11_txresetdone_out, gt11_txpolarity_in, 
  gt11_txprbssel_in, gt0_qplloutclk_in, gt0_qplloutrefclk_in, gt1_qplloutclk_in, 
  gt1_qplloutrefclk_in, gt2_qplloutclk_in, gt2_qplloutrefclk_in)
/* synthesis syn_black_box black_box_pad_pin="sysclk_in,soft_reset_tx_in,soft_reset_rx_in,dont_reset_on_data_error_in,gt0_tx_fsm_reset_done_out,gt0_rx_fsm_reset_done_out,gt0_data_valid_in,gt1_tx_fsm_reset_done_out,gt1_rx_fsm_reset_done_out,gt1_data_valid_in,gt2_tx_fsm_reset_done_out,gt2_rx_fsm_reset_done_out,gt2_data_valid_in,gt3_tx_fsm_reset_done_out,gt3_rx_fsm_reset_done_out,gt3_data_valid_in,gt4_tx_fsm_reset_done_out,gt4_rx_fsm_reset_done_out,gt4_data_valid_in,gt5_tx_fsm_reset_done_out,gt5_rx_fsm_reset_done_out,gt5_data_valid_in,gt6_tx_fsm_reset_done_out,gt6_rx_fsm_reset_done_out,gt6_data_valid_in,gt7_tx_fsm_reset_done_out,gt7_rx_fsm_reset_done_out,gt7_data_valid_in,gt8_tx_fsm_reset_done_out,gt8_rx_fsm_reset_done_out,gt8_data_valid_in,gt9_tx_fsm_reset_done_out,gt9_rx_fsm_reset_done_out,gt9_data_valid_in,gt10_tx_fsm_reset_done_out,gt10_rx_fsm_reset_done_out,gt10_data_valid_in,gt11_tx_fsm_reset_done_out,gt11_rx_fsm_reset_done_out,gt11_data_valid_in,gt0_cpllfbclklost_out,gt0_cplllock_out,gt0_cplllockdetclk_in,gt0_cpllreset_in,gt0_gtrefclk0_in,gt0_gtrefclk1_in,gt0_drpaddr_in[8:0],gt0_drpclk_in,gt0_drpdi_in[15:0],gt0_drpdo_out[15:0],gt0_drpen_in,gt0_drprdy_out,gt0_drpwe_in,gt0_dmonitorout_out[7:0],gt0_loopback_in[2:0],gt0_rxpd_in[1:0],gt0_txpd_in[1:0],gt0_eyescanreset_in,gt0_rxuserrdy_in,gt0_eyescandataerror_out,gt0_eyescantrigger_in,gt0_rxcdrhold_in,gt0_rxcdrovrden_in,gt0_rxusrclk_in,gt0_rxusrclk2_in,gt0_rxdata_out[31:0],gt0_rxprbserr_out,gt0_rxprbssel_in[2:0],gt0_rxprbscntreset_in,gt0_rxdisperr_out[3:0],gt0_rxnotintable_out[3:0],gt0_gtxrxp_in,gt0_gtxrxn_in,gt0_rxbufreset_in,gt0_rxbufstatus_out[2:0],gt0_rxphmonitor_out[4:0],gt0_rxphslipmonitor_out[4:0],gt0_rxbyteisaligned_out,gt0_rxbyterealign_out,gt0_rxcommadet_out,gt0_rxmcommaalignen_in,gt0_rxpcommaalignen_in,gt0_rxdfelpmreset_in,gt0_rxmonitorout_out[6:0],gt0_rxmonitorsel_in[1:0],gt0_rxoutclk_out,gt0_rxoutclkfabric_out,gt0_gtrxreset_in,gt0_rxpcsreset_in,gt0_rxpmareset_in,gt0_rxlpmen_in,gt0_rxpolarity_in,gt0_rxchariscomma_out[3:0],gt0_rxcharisk_out[3:0],gt0_rxresetdone_out,gt0_txpostcursor_in[4:0],gt0_txprecursor_in[4:0],gt0_gttxreset_in,gt0_txuserrdy_in,gt0_txchardispmode_in[3:0],gt0_txchardispval_in[3:0],gt0_txusrclk_in,gt0_txusrclk2_in,gt0_txprbsforceerr_in,gt0_txbufstatus_out[1:0],gt0_txdiffctrl_in[3:0],gt0_txmaincursor_in[6:0],gt0_txdata_in[31:0],gt0_gtxtxn_out,gt0_gtxtxp_out,gt0_txoutclk_out,gt0_txoutclkfabric_out,gt0_txoutclkpcs_out,gt0_txcharisk_in[3:0],gt0_txpcsreset_in,gt0_txpmareset_in,gt0_txresetdone_out,gt0_txpolarity_in,gt0_txprbssel_in[2:0],gt1_cpllfbclklost_out,gt1_cplllock_out,gt1_cplllockdetclk_in,gt1_cpllreset_in,gt1_gtrefclk0_in,gt1_gtrefclk1_in,gt1_drpaddr_in[8:0],gt1_drpclk_in,gt1_drpdi_in[15:0],gt1_drpdo_out[15:0],gt1_drpen_in,gt1_drprdy_out,gt1_drpwe_in,gt1_dmonitorout_out[7:0],gt1_loopback_in[2:0],gt1_rxpd_in[1:0],gt1_txpd_in[1:0],gt1_eyescanreset_in,gt1_rxuserrdy_in,gt1_eyescandataerror_out,gt1_eyescantrigger_in,gt1_rxcdrhold_in,gt1_rxcdrovrden_in,gt1_rxusrclk_in,gt1_rxusrclk2_in,gt1_rxdata_out[31:0],gt1_rxprbserr_out,gt1_rxprbssel_in[2:0],gt1_rxprbscntreset_in,gt1_rxdisperr_out[3:0],gt1_rxnotintable_out[3:0],gt1_gtxrxp_in,gt1_gtxrxn_in,gt1_rxbufreset_in,gt1_rxbufstatus_out[2:0],gt1_rxphmonitor_out[4:0],gt1_rxphslipmonitor_out[4:0],gt1_rxbyteisaligned_out,gt1_rxbyterealign_out,gt1_rxcommadet_out,gt1_rxmcommaalignen_in,gt1_rxpcommaalignen_in,gt1_rxdfelpmreset_in,gt1_rxmonitorout_out[6:0],gt1_rxmonitorsel_in[1:0],gt1_rxoutclk_out,gt1_rxoutclkfabric_out,gt1_gtrxreset_in,gt1_rxpcsreset_in,gt1_rxpmareset_in,gt1_rxlpmen_in,gt1_rxpolarity_in,gt1_rxchariscomma_out[3:0],gt1_rxcharisk_out[3:0],gt1_rxresetdone_out,gt1_txpostcursor_in[4:0],gt1_txprecursor_in[4:0],gt1_gttxreset_in,gt1_txuserrdy_in,gt1_txchardispmode_in[3:0],gt1_txchardispval_in[3:0],gt1_txusrclk_in,gt1_txusrclk2_in,gt1_txprbsforceerr_in,gt1_txbufstatus_out[1:0],gt1_txdiffctrl_in[3:0],gt1_txmaincursor_in[6:0],gt1_txdata_in[31:0],gt1_gtxtxn_out,gt1_gtxtxp_out,gt1_txoutclk_out,gt1_txoutclkfabric_out,gt1_txoutclkpcs_out,gt1_txcharisk_in[3:0],gt1_txpcsreset_in,gt1_txpmareset_in,gt1_txresetdone_out,gt1_txpolarity_in,gt1_txprbssel_in[2:0],gt2_cpllfbclklost_out,gt2_cplllock_out,gt2_cplllockdetclk_in,gt2_cpllreset_in,gt2_gtrefclk0_in,gt2_gtrefclk1_in,gt2_drpaddr_in[8:0],gt2_drpclk_in,gt2_drpdi_in[15:0],gt2_drpdo_out[15:0],gt2_drpen_in,gt2_drprdy_out,gt2_drpwe_in,gt2_dmonitorout_out[7:0],gt2_loopback_in[2:0],gt2_rxpd_in[1:0],gt2_txpd_in[1:0],gt2_eyescanreset_in,gt2_rxuserrdy_in,gt2_eyescandataerror_out,gt2_eyescantrigger_in,gt2_rxcdrhold_in,gt2_rxcdrovrden_in,gt2_rxusrclk_in,gt2_rxusrclk2_in,gt2_rxdata_out[31:0],gt2_rxprbserr_out,gt2_rxprbssel_in[2:0],gt2_rxprbscntreset_in,gt2_rxdisperr_out[3:0],gt2_rxnotintable_out[3:0],gt2_gtxrxp_in,gt2_gtxrxn_in,gt2_rxbufreset_in,gt2_rxbufstatus_out[2:0],gt2_rxphmonitor_out[4:0],gt2_rxphslipmonitor_out[4:0],gt2_rxbyteisaligned_out,gt2_rxbyterealign_out,gt2_rxcommadet_out,gt2_rxmcommaalignen_in,gt2_rxpcommaalignen_in,gt2_rxdfelpmreset_in,gt2_rxmonitorout_out[6:0],gt2_rxmonitorsel_in[1:0],gt2_rxoutclk_out,gt2_rxoutclkfabric_out,gt2_gtrxreset_in,gt2_rxpcsreset_in,gt2_rxpmareset_in,gt2_rxlpmen_in,gt2_rxpolarity_in,gt2_rxchariscomma_out[3:0],gt2_rxcharisk_out[3:0],gt2_rxresetdone_out,gt2_txpostcursor_in[4:0],gt2_txprecursor_in[4:0],gt2_gttxreset_in,gt2_txuserrdy_in,gt2_txchardispmode_in[3:0],gt2_txchardispval_in[3:0],gt2_txusrclk_in,gt2_txusrclk2_in,gt2_txprbsforceerr_in,gt2_txbufstatus_out[1:0],gt2_txdiffctrl_in[3:0],gt2_txmaincursor_in[6:0],gt2_txdata_in[31:0],gt2_gtxtxn_out,gt2_gtxtxp_out,gt2_txoutclk_out,gt2_txoutclkfabric_out,gt2_txoutclkpcs_out,gt2_txcharisk_in[3:0],gt2_txpcsreset_in,gt2_txpmareset_in,gt2_txresetdone_out,gt2_txpolarity_in,gt2_txprbssel_in[2:0],gt3_cpllfbclklost_out,gt3_cplllock_out,gt3_cplllockdetclk_in,gt3_cpllreset_in,gt3_gtrefclk0_in,gt3_gtrefclk1_in,gt3_drpaddr_in[8:0],gt3_drpclk_in,gt3_drpdi_in[15:0],gt3_drpdo_out[15:0],gt3_drpen_in,gt3_drprdy_out,gt3_drpwe_in,gt3_dmonitorout_out[7:0],gt3_loopback_in[2:0],gt3_rxpd_in[1:0],gt3_txpd_in[1:0],gt3_eyescanreset_in,gt3_rxuserrdy_in,gt3_eyescandataerror_out,gt3_eyescantrigger_in,gt3_rxcdrhold_in,gt3_rxcdrovrden_in,gt3_rxusrclk_in,gt3_rxusrclk2_in,gt3_rxdata_out[31:0],gt3_rxprbserr_out,gt3_rxprbssel_in[2:0],gt3_rxprbscntreset_in,gt3_rxdisperr_out[3:0],gt3_rxnotintable_out[3:0],gt3_gtxrxp_in,gt3_gtxrxn_in,gt3_rxbufreset_in,gt3_rxbufstatus_out[2:0],gt3_rxphmonitor_out[4:0],gt3_rxphslipmonitor_out[4:0],gt3_rxbyteisaligned_out,gt3_rxbyterealign_out,gt3_rxcommadet_out,gt3_rxmcommaalignen_in,gt3_rxpcommaalignen_in,gt3_rxdfelpmreset_in,gt3_rxmonitorout_out[6:0],gt3_rxmonitorsel_in[1:0],gt3_rxoutclk_out,gt3_rxoutclkfabric_out,gt3_gtrxreset_in,gt3_rxpcsreset_in,gt3_rxpmareset_in,gt3_rxlpmen_in,gt3_rxpolarity_in,gt3_rxchariscomma_out[3:0],gt3_rxcharisk_out[3:0],gt3_rxresetdone_out,gt3_txpostcursor_in[4:0],gt3_txprecursor_in[4:0],gt3_gttxreset_in,gt3_txuserrdy_in,gt3_txchardispmode_in[3:0],gt3_txchardispval_in[3:0],gt3_txusrclk_in,gt3_txusrclk2_in,gt3_txprbsforceerr_in,gt3_txbufstatus_out[1:0],gt3_txdiffctrl_in[3:0],gt3_txmaincursor_in[6:0],gt3_txdata_in[31:0],gt3_gtxtxn_out,gt3_gtxtxp_out,gt3_txoutclk_out,gt3_txoutclkfabric_out,gt3_txoutclkpcs_out,gt3_txcharisk_in[3:0],gt3_txpcsreset_in,gt3_txpmareset_in,gt3_txresetdone_out,gt3_txpolarity_in,gt3_txprbssel_in[2:0],gt4_cpllfbclklost_out,gt4_cplllock_out,gt4_cplllockdetclk_in,gt4_cpllreset_in,gt4_gtrefclk0_in,gt4_gtrefclk1_in,gt4_drpaddr_in[8:0],gt4_drpclk_in,gt4_drpdi_in[15:0],gt4_drpdo_out[15:0],gt4_drpen_in,gt4_drprdy_out,gt4_drpwe_in,gt4_dmonitorout_out[7:0],gt4_loopback_in[2:0],gt4_rxpd_in[1:0],gt4_txpd_in[1:0],gt4_eyescanreset_in,gt4_rxuserrdy_in,gt4_eyescandataerror_out,gt4_eyescantrigger_in,gt4_rxcdrhold_in,gt4_rxcdrovrden_in,gt4_rxusrclk_in,gt4_rxusrclk2_in,gt4_rxdata_out[31:0],gt4_rxprbserr_out,gt4_rxprbssel_in[2:0],gt4_rxprbscntreset_in,gt4_rxdisperr_out[3:0],gt4_rxnotintable_out[3:0],gt4_gtxrxp_in,gt4_gtxrxn_in,gt4_rxbufreset_in,gt4_rxbufstatus_out[2:0],gt4_rxphmonitor_out[4:0],gt4_rxphslipmonitor_out[4:0],gt4_rxbyteisaligned_out,gt4_rxbyterealign_out,gt4_rxcommadet_out,gt4_rxmcommaalignen_in,gt4_rxpcommaalignen_in,gt4_rxdfelpmreset_in,gt4_rxmonitorout_out[6:0],gt4_rxmonitorsel_in[1:0],gt4_rxoutclk_out,gt4_rxoutclkfabric_out,gt4_gtrxreset_in,gt4_rxpcsreset_in,gt4_rxpmareset_in,gt4_rxlpmen_in,gt4_rxpolarity_in,gt4_rxchariscomma_out[3:0],gt4_rxcharisk_out[3:0],gt4_rxresetdone_out,gt4_txpostcursor_in[4:0],gt4_txprecursor_in[4:0],gt4_gttxreset_in,gt4_txuserrdy_in,gt4_txchardispmode_in[3:0],gt4_txchardispval_in[3:0],gt4_txusrclk_in,gt4_txusrclk2_in,gt4_txprbsforceerr_in,gt4_txbufstatus_out[1:0],gt4_txdiffctrl_in[3:0],gt4_txmaincursor_in[6:0],gt4_txdata_in[31:0],gt4_gtxtxn_out,gt4_gtxtxp_out,gt4_txoutclk_out,gt4_txoutclkfabric_out,gt4_txoutclkpcs_out,gt4_txcharisk_in[3:0],gt4_txpcsreset_in,gt4_txpmareset_in,gt4_txresetdone_out,gt4_txpolarity_in,gt4_txprbssel_in[2:0],gt5_cpllfbclklost_out,gt5_cplllock_out,gt5_cplllockdetclk_in,gt5_cpllreset_in,gt5_gtrefclk0_in,gt5_gtrefclk1_in,gt5_drpaddr_in[8:0],gt5_drpclk_in,gt5_drpdi_in[15:0],gt5_drpdo_out[15:0],gt5_drpen_in,gt5_drprdy_out,gt5_drpwe_in,gt5_dmonitorout_out[7:0],gt5_loopback_in[2:0],gt5_rxpd_in[1:0],gt5_txpd_in[1:0],gt5_eyescanreset_in,gt5_rxuserrdy_in,gt5_eyescandataerror_out,gt5_eyescantrigger_in,gt5_rxcdrhold_in,gt5_rxcdrovrden_in,gt5_rxusrclk_in,gt5_rxusrclk2_in,gt5_rxdata_out[31:0],gt5_rxprbserr_out,gt5_rxprbssel_in[2:0],gt5_rxprbscntreset_in,gt5_rxdisperr_out[3:0],gt5_rxnotintable_out[3:0],gt5_gtxrxp_in,gt5_gtxrxn_in,gt5_rxbufreset_in,gt5_rxbufstatus_out[2:0],gt5_rxphmonitor_out[4:0],gt5_rxphslipmonitor_out[4:0],gt5_rxbyteisaligned_out,gt5_rxbyterealign_out,gt5_rxcommadet_out,gt5_rxmcommaalignen_in,gt5_rxpcommaalignen_in,gt5_rxdfelpmreset_in,gt5_rxmonitorout_out[6:0],gt5_rxmonitorsel_in[1:0],gt5_rxoutclk_out,gt5_rxoutclkfabric_out,gt5_gtrxreset_in,gt5_rxpcsreset_in,gt5_rxpmareset_in,gt5_rxlpmen_in,gt5_rxpolarity_in,gt5_rxchariscomma_out[3:0],gt5_rxcharisk_out[3:0],gt5_rxresetdone_out,gt5_txpostcursor_in[4:0],gt5_txprecursor_in[4:0],gt5_gttxreset_in,gt5_txuserrdy_in,gt5_txchardispmode_in[3:0],gt5_txchardispval_in[3:0],gt5_txusrclk_in,gt5_txusrclk2_in,gt5_txprbsforceerr_in,gt5_txbufstatus_out[1:0],gt5_txdiffctrl_in[3:0],gt5_txmaincursor_in[6:0],gt5_txdata_in[31:0],gt5_gtxtxn_out,gt5_gtxtxp_out,gt5_txoutclk_out,gt5_txoutclkfabric_out,gt5_txoutclkpcs_out,gt5_txcharisk_in[3:0],gt5_txpcsreset_in,gt5_txpmareset_in,gt5_txresetdone_out,gt5_txpolarity_in,gt5_txprbssel_in[2:0],gt6_cpllfbclklost_out,gt6_cplllock_out,gt6_cplllockdetclk_in,gt6_cpllreset_in,gt6_gtrefclk0_in,gt6_gtrefclk1_in,gt6_drpaddr_in[8:0],gt6_drpclk_in,gt6_drpdi_in[15:0],gt6_drpdo_out[15:0],gt6_drpen_in,gt6_drprdy_out,gt6_drpwe_in,gt6_dmonitorout_out[7:0],gt6_loopback_in[2:0],gt6_rxpd_in[1:0],gt6_txpd_in[1:0],gt6_eyescanreset_in,gt6_rxuserrdy_in,gt6_eyescandataerror_out,gt6_eyescantrigger_in,gt6_rxcdrhold_in,gt6_rxcdrovrden_in,gt6_rxusrclk_in,gt6_rxusrclk2_in,gt6_rxdata_out[31:0],gt6_rxprbserr_out,gt6_rxprbssel_in[2:0],gt6_rxprbscntreset_in,gt6_rxdisperr_out[3:0],gt6_rxnotintable_out[3:0],gt6_gtxrxp_in,gt6_gtxrxn_in,gt6_rxbufreset_in,gt6_rxbufstatus_out[2:0],gt6_rxphmonitor_out[4:0],gt6_rxphslipmonitor_out[4:0],gt6_rxbyteisaligned_out,gt6_rxbyterealign_out,gt6_rxcommadet_out,gt6_rxmcommaalignen_in,gt6_rxpcommaalignen_in,gt6_rxdfelpmreset_in,gt6_rxmonitorout_out[6:0],gt6_rxmonitorsel_in[1:0],gt6_rxoutclk_out,gt6_rxoutclkfabric_out,gt6_gtrxreset_in,gt6_rxpcsreset_in,gt6_rxpmareset_in,gt6_rxlpmen_in,gt6_rxpolarity_in,gt6_rxchariscomma_out[3:0],gt6_rxcharisk_out[3:0],gt6_rxresetdone_out,gt6_txpostcursor_in[4:0],gt6_txprecursor_in[4:0],gt6_gttxreset_in,gt6_txuserrdy_in,gt6_txchardispmode_in[3:0],gt6_txchardispval_in[3:0],gt6_txusrclk_in,gt6_txusrclk2_in,gt6_txprbsforceerr_in,gt6_txbufstatus_out[1:0],gt6_txdiffctrl_in[3:0],gt6_txmaincursor_in[6:0],gt6_txdata_in[31:0],gt6_gtxtxn_out,gt6_gtxtxp_out,gt6_txoutclk_out,gt6_txoutclkfabric_out,gt6_txoutclkpcs_out,gt6_txcharisk_in[3:0],gt6_txpcsreset_in,gt6_txpmareset_in,gt6_txresetdone_out,gt6_txpolarity_in,gt6_txprbssel_in[2:0],gt7_cpllfbclklost_out,gt7_cplllock_out,gt7_cplllockdetclk_in,gt7_cpllreset_in,gt7_gtrefclk0_in,gt7_gtrefclk1_in,gt7_drpaddr_in[8:0],gt7_drpclk_in,gt7_drpdi_in[15:0],gt7_drpdo_out[15:0],gt7_drpen_in,gt7_drprdy_out,gt7_drpwe_in,gt7_dmonitorout_out[7:0],gt7_loopback_in[2:0],gt7_rxpd_in[1:0],gt7_txpd_in[1:0],gt7_eyescanreset_in,gt7_rxuserrdy_in,gt7_eyescandataerror_out,gt7_eyescantrigger_in,gt7_rxcdrhold_in,gt7_rxcdrovrden_in,gt7_rxusrclk_in,gt7_rxusrclk2_in,gt7_rxdata_out[31:0],gt7_rxprbserr_out,gt7_rxprbssel_in[2:0],gt7_rxprbscntreset_in,gt7_rxdisperr_out[3:0],gt7_rxnotintable_out[3:0],gt7_gtxrxp_in,gt7_gtxrxn_in,gt7_rxbufreset_in,gt7_rxbufstatus_out[2:0],gt7_rxphmonitor_out[4:0],gt7_rxphslipmonitor_out[4:0],gt7_rxbyteisaligned_out,gt7_rxbyterealign_out,gt7_rxcommadet_out,gt7_rxmcommaalignen_in,gt7_rxpcommaalignen_in,gt7_rxdfelpmreset_in,gt7_rxmonitorout_out[6:0],gt7_rxmonitorsel_in[1:0],gt7_rxoutclk_out,gt7_rxoutclkfabric_out,gt7_gtrxreset_in,gt7_rxpcsreset_in,gt7_rxpmareset_in,gt7_rxlpmen_in,gt7_rxpolarity_in,gt7_rxchariscomma_out[3:0],gt7_rxcharisk_out[3:0],gt7_rxresetdone_out,gt7_txpostcursor_in[4:0],gt7_txprecursor_in[4:0],gt7_gttxreset_in,gt7_txuserrdy_in,gt7_txchardispmode_in[3:0],gt7_txchardispval_in[3:0],gt7_txusrclk_in,gt7_txusrclk2_in,gt7_txprbsforceerr_in,gt7_txbufstatus_out[1:0],gt7_txdiffctrl_in[3:0],gt7_txmaincursor_in[6:0],gt7_txdata_in[31:0],gt7_gtxtxn_out,gt7_gtxtxp_out,gt7_txoutclk_out,gt7_txoutclkfabric_out,gt7_txoutclkpcs_out,gt7_txcharisk_in[3:0],gt7_txpcsreset_in,gt7_txpmareset_in,gt7_txresetdone_out,gt7_txpolarity_in,gt7_txprbssel_in[2:0],gt8_cpllfbclklost_out,gt8_cplllock_out,gt8_cplllockdetclk_in,gt8_cpllreset_in,gt8_gtrefclk0_in,gt8_gtrefclk1_in,gt8_drpaddr_in[8:0],gt8_drpclk_in,gt8_drpdi_in[15:0],gt8_drpdo_out[15:0],gt8_drpen_in,gt8_drprdy_out,gt8_drpwe_in,gt8_dmonitorout_out[7:0],gt8_loopback_in[2:0],gt8_rxpd_in[1:0],gt8_txpd_in[1:0],gt8_eyescanreset_in,gt8_rxuserrdy_in,gt8_eyescandataerror_out,gt8_eyescantrigger_in,gt8_rxcdrhold_in,gt8_rxcdrovrden_in,gt8_rxusrclk_in,gt8_rxusrclk2_in,gt8_rxdata_out[31:0],gt8_rxprbserr_out,gt8_rxprbssel_in[2:0],gt8_rxprbscntreset_in,gt8_rxdisperr_out[3:0],gt8_rxnotintable_out[3:0],gt8_gtxrxp_in,gt8_gtxrxn_in,gt8_rxbufreset_in,gt8_rxbufstatus_out[2:0],gt8_rxphmonitor_out[4:0],gt8_rxphslipmonitor_out[4:0],gt8_rxbyteisaligned_out,gt8_rxbyterealign_out,gt8_rxcommadet_out,gt8_rxmcommaalignen_in,gt8_rxpcommaalignen_in,gt8_rxdfelpmreset_in,gt8_rxmonitorout_out[6:0],gt8_rxmonitorsel_in[1:0],gt8_rxoutclk_out,gt8_rxoutclkfabric_out,gt8_gtrxreset_in,gt8_rxpcsreset_in,gt8_rxpmareset_in,gt8_rxlpmen_in,gt8_rxpolarity_in,gt8_rxchariscomma_out[3:0],gt8_rxcharisk_out[3:0],gt8_rxresetdone_out,gt8_txpostcursor_in[4:0],gt8_txprecursor_in[4:0],gt8_gttxreset_in,gt8_txuserrdy_in,gt8_txchardispmode_in[3:0],gt8_txchardispval_in[3:0],gt8_txusrclk_in,gt8_txusrclk2_in,gt8_txprbsforceerr_in,gt8_txbufstatus_out[1:0],gt8_txdiffctrl_in[3:0],gt8_txmaincursor_in[6:0],gt8_txdata_in[31:0],gt8_gtxtxn_out,gt8_gtxtxp_out,gt8_txoutclk_out,gt8_txoutclkfabric_out,gt8_txoutclkpcs_out,gt8_txcharisk_in[3:0],gt8_txpcsreset_in,gt8_txpmareset_in,gt8_txresetdone_out,gt8_txpolarity_in,gt8_txprbssel_in[2:0],gt9_cpllfbclklost_out,gt9_cplllock_out,gt9_cplllockdetclk_in,gt9_cpllreset_in,gt9_gtrefclk0_in,gt9_gtrefclk1_in,gt9_drpaddr_in[8:0],gt9_drpclk_in,gt9_drpdi_in[15:0],gt9_drpdo_out[15:0],gt9_drpen_in,gt9_drprdy_out,gt9_drpwe_in,gt9_dmonitorout_out[7:0],gt9_loopback_in[2:0],gt9_rxpd_in[1:0],gt9_txpd_in[1:0],gt9_eyescanreset_in,gt9_rxuserrdy_in,gt9_eyescandataerror_out,gt9_eyescantrigger_in,gt9_rxcdrhold_in,gt9_rxcdrovrden_in,gt9_rxusrclk_in,gt9_rxusrclk2_in,gt9_rxdata_out[31:0],gt9_rxprbserr_out,gt9_rxprbssel_in[2:0],gt9_rxprbscntreset_in,gt9_rxdisperr_out[3:0],gt9_rxnotintable_out[3:0],gt9_gtxrxp_in,gt9_gtxrxn_in,gt9_rxbufreset_in,gt9_rxbufstatus_out[2:0],gt9_rxphmonitor_out[4:0],gt9_rxphslipmonitor_out[4:0],gt9_rxbyteisaligned_out,gt9_rxbyterealign_out,gt9_rxcommadet_out,gt9_rxmcommaalignen_in,gt9_rxpcommaalignen_in,gt9_rxdfelpmreset_in,gt9_rxmonitorout_out[6:0],gt9_rxmonitorsel_in[1:0],gt9_rxoutclk_out,gt9_rxoutclkfabric_out,gt9_gtrxreset_in,gt9_rxpcsreset_in,gt9_rxpmareset_in,gt9_rxlpmen_in,gt9_rxpolarity_in,gt9_rxchariscomma_out[3:0],gt9_rxcharisk_out[3:0],gt9_rxresetdone_out,gt9_txpostcursor_in[4:0],gt9_txprecursor_in[4:0],gt9_gttxreset_in,gt9_txuserrdy_in,gt9_txchardispmode_in[3:0],gt9_txchardispval_in[3:0],gt9_txusrclk_in,gt9_txusrclk2_in,gt9_txprbsforceerr_in,gt9_txbufstatus_out[1:0],gt9_txdiffctrl_in[3:0],gt9_txmaincursor_in[6:0],gt9_txdata_in[31:0],gt9_gtxtxn_out,gt9_gtxtxp_out,gt9_txoutclk_out,gt9_txoutclkfabric_out,gt9_txoutclkpcs_out,gt9_txcharisk_in[3:0],gt9_txpcsreset_in,gt9_txpmareset_in,gt9_txresetdone_out,gt9_txpolarity_in,gt9_txprbssel_in[2:0],gt10_cpllfbclklost_out,gt10_cplllock_out,gt10_cplllockdetclk_in,gt10_cpllreset_in,gt10_gtrefclk0_in,gt10_gtrefclk1_in,gt10_drpaddr_in[8:0],gt10_drpclk_in,gt10_drpdi_in[15:0],gt10_drpdo_out[15:0],gt10_drpen_in,gt10_drprdy_out,gt10_drpwe_in,gt10_dmonitorout_out[7:0],gt10_loopback_in[2:0],gt10_rxpd_in[1:0],gt10_txpd_in[1:0],gt10_eyescanreset_in,gt10_rxuserrdy_in,gt10_eyescandataerror_out,gt10_eyescantrigger_in,gt10_rxcdrhold_in,gt10_rxcdrovrden_in,gt10_rxusrclk_in,gt10_rxusrclk2_in,gt10_rxdata_out[31:0],gt10_rxprbserr_out,gt10_rxprbssel_in[2:0],gt10_rxprbscntreset_in,gt10_rxdisperr_out[3:0],gt10_rxnotintable_out[3:0],gt10_gtxrxp_in,gt10_gtxrxn_in,gt10_rxbufreset_in,gt10_rxbufstatus_out[2:0],gt10_rxphmonitor_out[4:0],gt10_rxphslipmonitor_out[4:0],gt10_rxbyteisaligned_out,gt10_rxbyterealign_out,gt10_rxcommadet_out,gt10_rxmcommaalignen_in,gt10_rxpcommaalignen_in,gt10_rxdfelpmreset_in,gt10_rxmonitorout_out[6:0],gt10_rxmonitorsel_in[1:0],gt10_rxoutclk_out,gt10_rxoutclkfabric_out,gt10_gtrxreset_in,gt10_rxpcsreset_in,gt10_rxpmareset_in,gt10_rxlpmen_in,gt10_rxpolarity_in,gt10_rxchariscomma_out[3:0],gt10_rxcharisk_out[3:0],gt10_rxresetdone_out,gt10_txpostcursor_in[4:0],gt10_txprecursor_in[4:0],gt10_gttxreset_in,gt10_txuserrdy_in,gt10_txchardispmode_in[3:0],gt10_txchardispval_in[3:0],gt10_txusrclk_in,gt10_txusrclk2_in,gt10_txprbsforceerr_in,gt10_txbufstatus_out[1:0],gt10_txdiffctrl_in[3:0],gt10_txmaincursor_in[6:0],gt10_txdata_in[31:0],gt10_gtxtxn_out,gt10_gtxtxp_out,gt10_txoutclk_out,gt10_txoutclkfabric_out,gt10_txoutclkpcs_out,gt10_txcharisk_in[3:0],gt10_txpcsreset_in,gt10_txpmareset_in,gt10_txresetdone_out,gt10_txpolarity_in,gt10_txprbssel_in[2:0],gt11_cpllfbclklost_out,gt11_cplllock_out,gt11_cplllockdetclk_in,gt11_cpllreset_in,gt11_gtrefclk0_in,gt11_gtrefclk1_in,gt11_drpaddr_in[8:0],gt11_drpclk_in,gt11_drpdi_in[15:0],gt11_drpdo_out[15:0],gt11_drpen_in,gt11_drprdy_out,gt11_drpwe_in,gt11_dmonitorout_out[7:0],gt11_loopback_in[2:0],gt11_rxpd_in[1:0],gt11_txpd_in[1:0],gt11_eyescanreset_in,gt11_rxuserrdy_in,gt11_eyescandataerror_out,gt11_eyescantrigger_in,gt11_rxcdrhold_in,gt11_rxcdrovrden_in,gt11_rxusrclk_in,gt11_rxusrclk2_in,gt11_rxdata_out[31:0],gt11_rxprbserr_out,gt11_rxprbssel_in[2:0],gt11_rxprbscntreset_in,gt11_rxdisperr_out[3:0],gt11_rxnotintable_out[3:0],gt11_gtxrxp_in,gt11_gtxrxn_in,gt11_rxbufreset_in,gt11_rxbufstatus_out[2:0],gt11_rxphmonitor_out[4:0],gt11_rxphslipmonitor_out[4:0],gt11_rxbyteisaligned_out,gt11_rxbyterealign_out,gt11_rxcommadet_out,gt11_rxmcommaalignen_in,gt11_rxpcommaalignen_in,gt11_rxdfelpmreset_in,gt11_rxmonitorout_out[6:0],gt11_rxmonitorsel_in[1:0],gt11_rxoutclk_out,gt11_rxoutclkfabric_out,gt11_gtrxreset_in,gt11_rxpcsreset_in,gt11_rxpmareset_in,gt11_rxlpmen_in,gt11_rxpolarity_in,gt11_rxchariscomma_out[3:0],gt11_rxcharisk_out[3:0],gt11_rxresetdone_out,gt11_txpostcursor_in[4:0],gt11_txprecursor_in[4:0],gt11_gttxreset_in,gt11_txuserrdy_in,gt11_txchardispmode_in[3:0],gt11_txchardispval_in[3:0],gt11_txusrclk_in,gt11_txusrclk2_in,gt11_txprbsforceerr_in,gt11_txbufstatus_out[1:0],gt11_txdiffctrl_in[3:0],gt11_txmaincursor_in[6:0],gt11_txdata_in[31:0],gt11_gtxtxn_out,gt11_gtxtxp_out,gt11_txoutclk_out,gt11_txoutclkfabric_out,gt11_txoutclkpcs_out,gt11_txcharisk_in[3:0],gt11_txpcsreset_in,gt11_txpmareset_in,gt11_txresetdone_out,gt11_txpolarity_in,gt11_txprbssel_in[2:0],gt0_qplloutclk_in,gt0_qplloutrefclk_in,gt1_qplloutclk_in,gt1_qplloutrefclk_in,gt2_qplloutclk_in,gt2_qplloutrefclk_in" */;
  input sysclk_in;
  input soft_reset_tx_in;
  input soft_reset_rx_in;
  input dont_reset_on_data_error_in;
  output gt0_tx_fsm_reset_done_out;
  output gt0_rx_fsm_reset_done_out;
  input gt0_data_valid_in;
  output gt1_tx_fsm_reset_done_out;
  output gt1_rx_fsm_reset_done_out;
  input gt1_data_valid_in;
  output gt2_tx_fsm_reset_done_out;
  output gt2_rx_fsm_reset_done_out;
  input gt2_data_valid_in;
  output gt3_tx_fsm_reset_done_out;
  output gt3_rx_fsm_reset_done_out;
  input gt3_data_valid_in;
  output gt4_tx_fsm_reset_done_out;
  output gt4_rx_fsm_reset_done_out;
  input gt4_data_valid_in;
  output gt5_tx_fsm_reset_done_out;
  output gt5_rx_fsm_reset_done_out;
  input gt5_data_valid_in;
  output gt6_tx_fsm_reset_done_out;
  output gt6_rx_fsm_reset_done_out;
  input gt6_data_valid_in;
  output gt7_tx_fsm_reset_done_out;
  output gt7_rx_fsm_reset_done_out;
  input gt7_data_valid_in;
  output gt8_tx_fsm_reset_done_out;
  output gt8_rx_fsm_reset_done_out;
  input gt8_data_valid_in;
  output gt9_tx_fsm_reset_done_out;
  output gt9_rx_fsm_reset_done_out;
  input gt9_data_valid_in;
  output gt10_tx_fsm_reset_done_out;
  output gt10_rx_fsm_reset_done_out;
  input gt10_data_valid_in;
  output gt11_tx_fsm_reset_done_out;
  output gt11_rx_fsm_reset_done_out;
  input gt11_data_valid_in;
  output gt0_cpllfbclklost_out;
  output gt0_cplllock_out;
  input gt0_cplllockdetclk_in;
  input gt0_cpllreset_in;
  input gt0_gtrefclk0_in;
  input gt0_gtrefclk1_in;
  input [8:0]gt0_drpaddr_in;
  input gt0_drpclk_in;
  input [15:0]gt0_drpdi_in;
  output [15:0]gt0_drpdo_out;
  input gt0_drpen_in;
  output gt0_drprdy_out;
  input gt0_drpwe_in;
  output [7:0]gt0_dmonitorout_out;
  input [2:0]gt0_loopback_in;
  input [1:0]gt0_rxpd_in;
  input [1:0]gt0_txpd_in;
  input gt0_eyescanreset_in;
  input gt0_rxuserrdy_in;
  output gt0_eyescandataerror_out;
  input gt0_eyescantrigger_in;
  input gt0_rxcdrhold_in;
  input gt0_rxcdrovrden_in;
  input gt0_rxusrclk_in;
  input gt0_rxusrclk2_in;
  output [31:0]gt0_rxdata_out;
  output gt0_rxprbserr_out;
  input [2:0]gt0_rxprbssel_in;
  input gt0_rxprbscntreset_in;
  output [3:0]gt0_rxdisperr_out;
  output [3:0]gt0_rxnotintable_out;
  input gt0_gtxrxp_in;
  input gt0_gtxrxn_in;
  input gt0_rxbufreset_in;
  output [2:0]gt0_rxbufstatus_out;
  output [4:0]gt0_rxphmonitor_out;
  output [4:0]gt0_rxphslipmonitor_out;
  output gt0_rxbyteisaligned_out;
  output gt0_rxbyterealign_out;
  output gt0_rxcommadet_out;
  input gt0_rxmcommaalignen_in;
  input gt0_rxpcommaalignen_in;
  input gt0_rxdfelpmreset_in;
  output [6:0]gt0_rxmonitorout_out;
  input [1:0]gt0_rxmonitorsel_in;
  output gt0_rxoutclk_out;
  output gt0_rxoutclkfabric_out;
  input gt0_gtrxreset_in;
  input gt0_rxpcsreset_in;
  input gt0_rxpmareset_in;
  input gt0_rxlpmen_in;
  input gt0_rxpolarity_in;
  output [3:0]gt0_rxchariscomma_out;
  output [3:0]gt0_rxcharisk_out;
  output gt0_rxresetdone_out;
  input [4:0]gt0_txpostcursor_in;
  input [4:0]gt0_txprecursor_in;
  input gt0_gttxreset_in;
  input gt0_txuserrdy_in;
  input [3:0]gt0_txchardispmode_in;
  input [3:0]gt0_txchardispval_in;
  input gt0_txusrclk_in;
  input gt0_txusrclk2_in;
  input gt0_txprbsforceerr_in;
  output [1:0]gt0_txbufstatus_out;
  input [3:0]gt0_txdiffctrl_in;
  input [6:0]gt0_txmaincursor_in;
  input [31:0]gt0_txdata_in;
  output gt0_gtxtxn_out;
  output gt0_gtxtxp_out;
  output gt0_txoutclk_out;
  output gt0_txoutclkfabric_out;
  output gt0_txoutclkpcs_out;
  input [3:0]gt0_txcharisk_in;
  input gt0_txpcsreset_in;
  input gt0_txpmareset_in;
  output gt0_txresetdone_out;
  input gt0_txpolarity_in;
  input [2:0]gt0_txprbssel_in;
  output gt1_cpllfbclklost_out;
  output gt1_cplllock_out;
  input gt1_cplllockdetclk_in;
  input gt1_cpllreset_in;
  input gt1_gtrefclk0_in;
  input gt1_gtrefclk1_in;
  input [8:0]gt1_drpaddr_in;
  input gt1_drpclk_in;
  input [15:0]gt1_drpdi_in;
  output [15:0]gt1_drpdo_out;
  input gt1_drpen_in;
  output gt1_drprdy_out;
  input gt1_drpwe_in;
  output [7:0]gt1_dmonitorout_out;
  input [2:0]gt1_loopback_in;
  input [1:0]gt1_rxpd_in;
  input [1:0]gt1_txpd_in;
  input gt1_eyescanreset_in;
  input gt1_rxuserrdy_in;
  output gt1_eyescandataerror_out;
  input gt1_eyescantrigger_in;
  input gt1_rxcdrhold_in;
  input gt1_rxcdrovrden_in;
  input gt1_rxusrclk_in;
  input gt1_rxusrclk2_in;
  output [31:0]gt1_rxdata_out;
  output gt1_rxprbserr_out;
  input [2:0]gt1_rxprbssel_in;
  input gt1_rxprbscntreset_in;
  output [3:0]gt1_rxdisperr_out;
  output [3:0]gt1_rxnotintable_out;
  input gt1_gtxrxp_in;
  input gt1_gtxrxn_in;
  input gt1_rxbufreset_in;
  output [2:0]gt1_rxbufstatus_out;
  output [4:0]gt1_rxphmonitor_out;
  output [4:0]gt1_rxphslipmonitor_out;
  output gt1_rxbyteisaligned_out;
  output gt1_rxbyterealign_out;
  output gt1_rxcommadet_out;
  input gt1_rxmcommaalignen_in;
  input gt1_rxpcommaalignen_in;
  input gt1_rxdfelpmreset_in;
  output [6:0]gt1_rxmonitorout_out;
  input [1:0]gt1_rxmonitorsel_in;
  output gt1_rxoutclk_out;
  output gt1_rxoutclkfabric_out;
  input gt1_gtrxreset_in;
  input gt1_rxpcsreset_in;
  input gt1_rxpmareset_in;
  input gt1_rxlpmen_in;
  input gt1_rxpolarity_in;
  output [3:0]gt1_rxchariscomma_out;
  output [3:0]gt1_rxcharisk_out;
  output gt1_rxresetdone_out;
  input [4:0]gt1_txpostcursor_in;
  input [4:0]gt1_txprecursor_in;
  input gt1_gttxreset_in;
  input gt1_txuserrdy_in;
  input [3:0]gt1_txchardispmode_in;
  input [3:0]gt1_txchardispval_in;
  input gt1_txusrclk_in;
  input gt1_txusrclk2_in;
  input gt1_txprbsforceerr_in;
  output [1:0]gt1_txbufstatus_out;
  input [3:0]gt1_txdiffctrl_in;
  input [6:0]gt1_txmaincursor_in;
  input [31:0]gt1_txdata_in;
  output gt1_gtxtxn_out;
  output gt1_gtxtxp_out;
  output gt1_txoutclk_out;
  output gt1_txoutclkfabric_out;
  output gt1_txoutclkpcs_out;
  input [3:0]gt1_txcharisk_in;
  input gt1_txpcsreset_in;
  input gt1_txpmareset_in;
  output gt1_txresetdone_out;
  input gt1_txpolarity_in;
  input [2:0]gt1_txprbssel_in;
  output gt2_cpllfbclklost_out;
  output gt2_cplllock_out;
  input gt2_cplllockdetclk_in;
  input gt2_cpllreset_in;
  input gt2_gtrefclk0_in;
  input gt2_gtrefclk1_in;
  input [8:0]gt2_drpaddr_in;
  input gt2_drpclk_in;
  input [15:0]gt2_drpdi_in;
  output [15:0]gt2_drpdo_out;
  input gt2_drpen_in;
  output gt2_drprdy_out;
  input gt2_drpwe_in;
  output [7:0]gt2_dmonitorout_out;
  input [2:0]gt2_loopback_in;
  input [1:0]gt2_rxpd_in;
  input [1:0]gt2_txpd_in;
  input gt2_eyescanreset_in;
  input gt2_rxuserrdy_in;
  output gt2_eyescandataerror_out;
  input gt2_eyescantrigger_in;
  input gt2_rxcdrhold_in;
  input gt2_rxcdrovrden_in;
  input gt2_rxusrclk_in;
  input gt2_rxusrclk2_in;
  output [31:0]gt2_rxdata_out;
  output gt2_rxprbserr_out;
  input [2:0]gt2_rxprbssel_in;
  input gt2_rxprbscntreset_in;
  output [3:0]gt2_rxdisperr_out;
  output [3:0]gt2_rxnotintable_out;
  input gt2_gtxrxp_in;
  input gt2_gtxrxn_in;
  input gt2_rxbufreset_in;
  output [2:0]gt2_rxbufstatus_out;
  output [4:0]gt2_rxphmonitor_out;
  output [4:0]gt2_rxphslipmonitor_out;
  output gt2_rxbyteisaligned_out;
  output gt2_rxbyterealign_out;
  output gt2_rxcommadet_out;
  input gt2_rxmcommaalignen_in;
  input gt2_rxpcommaalignen_in;
  input gt2_rxdfelpmreset_in;
  output [6:0]gt2_rxmonitorout_out;
  input [1:0]gt2_rxmonitorsel_in;
  output gt2_rxoutclk_out;
  output gt2_rxoutclkfabric_out;
  input gt2_gtrxreset_in;
  input gt2_rxpcsreset_in;
  input gt2_rxpmareset_in;
  input gt2_rxlpmen_in;
  input gt2_rxpolarity_in;
  output [3:0]gt2_rxchariscomma_out;
  output [3:0]gt2_rxcharisk_out;
  output gt2_rxresetdone_out;
  input [4:0]gt2_txpostcursor_in;
  input [4:0]gt2_txprecursor_in;
  input gt2_gttxreset_in;
  input gt2_txuserrdy_in;
  input [3:0]gt2_txchardispmode_in;
  input [3:0]gt2_txchardispval_in;
  input gt2_txusrclk_in;
  input gt2_txusrclk2_in;
  input gt2_txprbsforceerr_in;
  output [1:0]gt2_txbufstatus_out;
  input [3:0]gt2_txdiffctrl_in;
  input [6:0]gt2_txmaincursor_in;
  input [31:0]gt2_txdata_in;
  output gt2_gtxtxn_out;
  output gt2_gtxtxp_out;
  output gt2_txoutclk_out;
  output gt2_txoutclkfabric_out;
  output gt2_txoutclkpcs_out;
  input [3:0]gt2_txcharisk_in;
  input gt2_txpcsreset_in;
  input gt2_txpmareset_in;
  output gt2_txresetdone_out;
  input gt2_txpolarity_in;
  input [2:0]gt2_txprbssel_in;
  output gt3_cpllfbclklost_out;
  output gt3_cplllock_out;
  input gt3_cplllockdetclk_in;
  input gt3_cpllreset_in;
  input gt3_gtrefclk0_in;
  input gt3_gtrefclk1_in;
  input [8:0]gt3_drpaddr_in;
  input gt3_drpclk_in;
  input [15:0]gt3_drpdi_in;
  output [15:0]gt3_drpdo_out;
  input gt3_drpen_in;
  output gt3_drprdy_out;
  input gt3_drpwe_in;
  output [7:0]gt3_dmonitorout_out;
  input [2:0]gt3_loopback_in;
  input [1:0]gt3_rxpd_in;
  input [1:0]gt3_txpd_in;
  input gt3_eyescanreset_in;
  input gt3_rxuserrdy_in;
  output gt3_eyescandataerror_out;
  input gt3_eyescantrigger_in;
  input gt3_rxcdrhold_in;
  input gt3_rxcdrovrden_in;
  input gt3_rxusrclk_in;
  input gt3_rxusrclk2_in;
  output [31:0]gt3_rxdata_out;
  output gt3_rxprbserr_out;
  input [2:0]gt3_rxprbssel_in;
  input gt3_rxprbscntreset_in;
  output [3:0]gt3_rxdisperr_out;
  output [3:0]gt3_rxnotintable_out;
  input gt3_gtxrxp_in;
  input gt3_gtxrxn_in;
  input gt3_rxbufreset_in;
  output [2:0]gt3_rxbufstatus_out;
  output [4:0]gt3_rxphmonitor_out;
  output [4:0]gt3_rxphslipmonitor_out;
  output gt3_rxbyteisaligned_out;
  output gt3_rxbyterealign_out;
  output gt3_rxcommadet_out;
  input gt3_rxmcommaalignen_in;
  input gt3_rxpcommaalignen_in;
  input gt3_rxdfelpmreset_in;
  output [6:0]gt3_rxmonitorout_out;
  input [1:0]gt3_rxmonitorsel_in;
  output gt3_rxoutclk_out;
  output gt3_rxoutclkfabric_out;
  input gt3_gtrxreset_in;
  input gt3_rxpcsreset_in;
  input gt3_rxpmareset_in;
  input gt3_rxlpmen_in;
  input gt3_rxpolarity_in;
  output [3:0]gt3_rxchariscomma_out;
  output [3:0]gt3_rxcharisk_out;
  output gt3_rxresetdone_out;
  input [4:0]gt3_txpostcursor_in;
  input [4:0]gt3_txprecursor_in;
  input gt3_gttxreset_in;
  input gt3_txuserrdy_in;
  input [3:0]gt3_txchardispmode_in;
  input [3:0]gt3_txchardispval_in;
  input gt3_txusrclk_in;
  input gt3_txusrclk2_in;
  input gt3_txprbsforceerr_in;
  output [1:0]gt3_txbufstatus_out;
  input [3:0]gt3_txdiffctrl_in;
  input [6:0]gt3_txmaincursor_in;
  input [31:0]gt3_txdata_in;
  output gt3_gtxtxn_out;
  output gt3_gtxtxp_out;
  output gt3_txoutclk_out;
  output gt3_txoutclkfabric_out;
  output gt3_txoutclkpcs_out;
  input [3:0]gt3_txcharisk_in;
  input gt3_txpcsreset_in;
  input gt3_txpmareset_in;
  output gt3_txresetdone_out;
  input gt3_txpolarity_in;
  input [2:0]gt3_txprbssel_in;
  output gt4_cpllfbclklost_out;
  output gt4_cplllock_out;
  input gt4_cplllockdetclk_in;
  input gt4_cpllreset_in;
  input gt4_gtrefclk0_in;
  input gt4_gtrefclk1_in;
  input [8:0]gt4_drpaddr_in;
  input gt4_drpclk_in;
  input [15:0]gt4_drpdi_in;
  output [15:0]gt4_drpdo_out;
  input gt4_drpen_in;
  output gt4_drprdy_out;
  input gt4_drpwe_in;
  output [7:0]gt4_dmonitorout_out;
  input [2:0]gt4_loopback_in;
  input [1:0]gt4_rxpd_in;
  input [1:0]gt4_txpd_in;
  input gt4_eyescanreset_in;
  input gt4_rxuserrdy_in;
  output gt4_eyescandataerror_out;
  input gt4_eyescantrigger_in;
  input gt4_rxcdrhold_in;
  input gt4_rxcdrovrden_in;
  input gt4_rxusrclk_in;
  input gt4_rxusrclk2_in;
  output [31:0]gt4_rxdata_out;
  output gt4_rxprbserr_out;
  input [2:0]gt4_rxprbssel_in;
  input gt4_rxprbscntreset_in;
  output [3:0]gt4_rxdisperr_out;
  output [3:0]gt4_rxnotintable_out;
  input gt4_gtxrxp_in;
  input gt4_gtxrxn_in;
  input gt4_rxbufreset_in;
  output [2:0]gt4_rxbufstatus_out;
  output [4:0]gt4_rxphmonitor_out;
  output [4:0]gt4_rxphslipmonitor_out;
  output gt4_rxbyteisaligned_out;
  output gt4_rxbyterealign_out;
  output gt4_rxcommadet_out;
  input gt4_rxmcommaalignen_in;
  input gt4_rxpcommaalignen_in;
  input gt4_rxdfelpmreset_in;
  output [6:0]gt4_rxmonitorout_out;
  input [1:0]gt4_rxmonitorsel_in;
  output gt4_rxoutclk_out;
  output gt4_rxoutclkfabric_out;
  input gt4_gtrxreset_in;
  input gt4_rxpcsreset_in;
  input gt4_rxpmareset_in;
  input gt4_rxlpmen_in;
  input gt4_rxpolarity_in;
  output [3:0]gt4_rxchariscomma_out;
  output [3:0]gt4_rxcharisk_out;
  output gt4_rxresetdone_out;
  input [4:0]gt4_txpostcursor_in;
  input [4:0]gt4_txprecursor_in;
  input gt4_gttxreset_in;
  input gt4_txuserrdy_in;
  input [3:0]gt4_txchardispmode_in;
  input [3:0]gt4_txchardispval_in;
  input gt4_txusrclk_in;
  input gt4_txusrclk2_in;
  input gt4_txprbsforceerr_in;
  output [1:0]gt4_txbufstatus_out;
  input [3:0]gt4_txdiffctrl_in;
  input [6:0]gt4_txmaincursor_in;
  input [31:0]gt4_txdata_in;
  output gt4_gtxtxn_out;
  output gt4_gtxtxp_out;
  output gt4_txoutclk_out;
  output gt4_txoutclkfabric_out;
  output gt4_txoutclkpcs_out;
  input [3:0]gt4_txcharisk_in;
  input gt4_txpcsreset_in;
  input gt4_txpmareset_in;
  output gt4_txresetdone_out;
  input gt4_txpolarity_in;
  input [2:0]gt4_txprbssel_in;
  output gt5_cpllfbclklost_out;
  output gt5_cplllock_out;
  input gt5_cplllockdetclk_in;
  input gt5_cpllreset_in;
  input gt5_gtrefclk0_in;
  input gt5_gtrefclk1_in;
  input [8:0]gt5_drpaddr_in;
  input gt5_drpclk_in;
  input [15:0]gt5_drpdi_in;
  output [15:0]gt5_drpdo_out;
  input gt5_drpen_in;
  output gt5_drprdy_out;
  input gt5_drpwe_in;
  output [7:0]gt5_dmonitorout_out;
  input [2:0]gt5_loopback_in;
  input [1:0]gt5_rxpd_in;
  input [1:0]gt5_txpd_in;
  input gt5_eyescanreset_in;
  input gt5_rxuserrdy_in;
  output gt5_eyescandataerror_out;
  input gt5_eyescantrigger_in;
  input gt5_rxcdrhold_in;
  input gt5_rxcdrovrden_in;
  input gt5_rxusrclk_in;
  input gt5_rxusrclk2_in;
  output [31:0]gt5_rxdata_out;
  output gt5_rxprbserr_out;
  input [2:0]gt5_rxprbssel_in;
  input gt5_rxprbscntreset_in;
  output [3:0]gt5_rxdisperr_out;
  output [3:0]gt5_rxnotintable_out;
  input gt5_gtxrxp_in;
  input gt5_gtxrxn_in;
  input gt5_rxbufreset_in;
  output [2:0]gt5_rxbufstatus_out;
  output [4:0]gt5_rxphmonitor_out;
  output [4:0]gt5_rxphslipmonitor_out;
  output gt5_rxbyteisaligned_out;
  output gt5_rxbyterealign_out;
  output gt5_rxcommadet_out;
  input gt5_rxmcommaalignen_in;
  input gt5_rxpcommaalignen_in;
  input gt5_rxdfelpmreset_in;
  output [6:0]gt5_rxmonitorout_out;
  input [1:0]gt5_rxmonitorsel_in;
  output gt5_rxoutclk_out;
  output gt5_rxoutclkfabric_out;
  input gt5_gtrxreset_in;
  input gt5_rxpcsreset_in;
  input gt5_rxpmareset_in;
  input gt5_rxlpmen_in;
  input gt5_rxpolarity_in;
  output [3:0]gt5_rxchariscomma_out;
  output [3:0]gt5_rxcharisk_out;
  output gt5_rxresetdone_out;
  input [4:0]gt5_txpostcursor_in;
  input [4:0]gt5_txprecursor_in;
  input gt5_gttxreset_in;
  input gt5_txuserrdy_in;
  input [3:0]gt5_txchardispmode_in;
  input [3:0]gt5_txchardispval_in;
  input gt5_txusrclk_in;
  input gt5_txusrclk2_in;
  input gt5_txprbsforceerr_in;
  output [1:0]gt5_txbufstatus_out;
  input [3:0]gt5_txdiffctrl_in;
  input [6:0]gt5_txmaincursor_in;
  input [31:0]gt5_txdata_in;
  output gt5_gtxtxn_out;
  output gt5_gtxtxp_out;
  output gt5_txoutclk_out;
  output gt5_txoutclkfabric_out;
  output gt5_txoutclkpcs_out;
  input [3:0]gt5_txcharisk_in;
  input gt5_txpcsreset_in;
  input gt5_txpmareset_in;
  output gt5_txresetdone_out;
  input gt5_txpolarity_in;
  input [2:0]gt5_txprbssel_in;
  output gt6_cpllfbclklost_out;
  output gt6_cplllock_out;
  input gt6_cplllockdetclk_in;
  input gt6_cpllreset_in;
  input gt6_gtrefclk0_in;
  input gt6_gtrefclk1_in;
  input [8:0]gt6_drpaddr_in;
  input gt6_drpclk_in;
  input [15:0]gt6_drpdi_in;
  output [15:0]gt6_drpdo_out;
  input gt6_drpen_in;
  output gt6_drprdy_out;
  input gt6_drpwe_in;
  output [7:0]gt6_dmonitorout_out;
  input [2:0]gt6_loopback_in;
  input [1:0]gt6_rxpd_in;
  input [1:0]gt6_txpd_in;
  input gt6_eyescanreset_in;
  input gt6_rxuserrdy_in;
  output gt6_eyescandataerror_out;
  input gt6_eyescantrigger_in;
  input gt6_rxcdrhold_in;
  input gt6_rxcdrovrden_in;
  input gt6_rxusrclk_in;
  input gt6_rxusrclk2_in;
  output [31:0]gt6_rxdata_out;
  output gt6_rxprbserr_out;
  input [2:0]gt6_rxprbssel_in;
  input gt6_rxprbscntreset_in;
  output [3:0]gt6_rxdisperr_out;
  output [3:0]gt6_rxnotintable_out;
  input gt6_gtxrxp_in;
  input gt6_gtxrxn_in;
  input gt6_rxbufreset_in;
  output [2:0]gt6_rxbufstatus_out;
  output [4:0]gt6_rxphmonitor_out;
  output [4:0]gt6_rxphslipmonitor_out;
  output gt6_rxbyteisaligned_out;
  output gt6_rxbyterealign_out;
  output gt6_rxcommadet_out;
  input gt6_rxmcommaalignen_in;
  input gt6_rxpcommaalignen_in;
  input gt6_rxdfelpmreset_in;
  output [6:0]gt6_rxmonitorout_out;
  input [1:0]gt6_rxmonitorsel_in;
  output gt6_rxoutclk_out;
  output gt6_rxoutclkfabric_out;
  input gt6_gtrxreset_in;
  input gt6_rxpcsreset_in;
  input gt6_rxpmareset_in;
  input gt6_rxlpmen_in;
  input gt6_rxpolarity_in;
  output [3:0]gt6_rxchariscomma_out;
  output [3:0]gt6_rxcharisk_out;
  output gt6_rxresetdone_out;
  input [4:0]gt6_txpostcursor_in;
  input [4:0]gt6_txprecursor_in;
  input gt6_gttxreset_in;
  input gt6_txuserrdy_in;
  input [3:0]gt6_txchardispmode_in;
  input [3:0]gt6_txchardispval_in;
  input gt6_txusrclk_in;
  input gt6_txusrclk2_in;
  input gt6_txprbsforceerr_in;
  output [1:0]gt6_txbufstatus_out;
  input [3:0]gt6_txdiffctrl_in;
  input [6:0]gt6_txmaincursor_in;
  input [31:0]gt6_txdata_in;
  output gt6_gtxtxn_out;
  output gt6_gtxtxp_out;
  output gt6_txoutclk_out;
  output gt6_txoutclkfabric_out;
  output gt6_txoutclkpcs_out;
  input [3:0]gt6_txcharisk_in;
  input gt6_txpcsreset_in;
  input gt6_txpmareset_in;
  output gt6_txresetdone_out;
  input gt6_txpolarity_in;
  input [2:0]gt6_txprbssel_in;
  output gt7_cpllfbclklost_out;
  output gt7_cplllock_out;
  input gt7_cplllockdetclk_in;
  input gt7_cpllreset_in;
  input gt7_gtrefclk0_in;
  input gt7_gtrefclk1_in;
  input [8:0]gt7_drpaddr_in;
  input gt7_drpclk_in;
  input [15:0]gt7_drpdi_in;
  output [15:0]gt7_drpdo_out;
  input gt7_drpen_in;
  output gt7_drprdy_out;
  input gt7_drpwe_in;
  output [7:0]gt7_dmonitorout_out;
  input [2:0]gt7_loopback_in;
  input [1:0]gt7_rxpd_in;
  input [1:0]gt7_txpd_in;
  input gt7_eyescanreset_in;
  input gt7_rxuserrdy_in;
  output gt7_eyescandataerror_out;
  input gt7_eyescantrigger_in;
  input gt7_rxcdrhold_in;
  input gt7_rxcdrovrden_in;
  input gt7_rxusrclk_in;
  input gt7_rxusrclk2_in;
  output [31:0]gt7_rxdata_out;
  output gt7_rxprbserr_out;
  input [2:0]gt7_rxprbssel_in;
  input gt7_rxprbscntreset_in;
  output [3:0]gt7_rxdisperr_out;
  output [3:0]gt7_rxnotintable_out;
  input gt7_gtxrxp_in;
  input gt7_gtxrxn_in;
  input gt7_rxbufreset_in;
  output [2:0]gt7_rxbufstatus_out;
  output [4:0]gt7_rxphmonitor_out;
  output [4:0]gt7_rxphslipmonitor_out;
  output gt7_rxbyteisaligned_out;
  output gt7_rxbyterealign_out;
  output gt7_rxcommadet_out;
  input gt7_rxmcommaalignen_in;
  input gt7_rxpcommaalignen_in;
  input gt7_rxdfelpmreset_in;
  output [6:0]gt7_rxmonitorout_out;
  input [1:0]gt7_rxmonitorsel_in;
  output gt7_rxoutclk_out;
  output gt7_rxoutclkfabric_out;
  input gt7_gtrxreset_in;
  input gt7_rxpcsreset_in;
  input gt7_rxpmareset_in;
  input gt7_rxlpmen_in;
  input gt7_rxpolarity_in;
  output [3:0]gt7_rxchariscomma_out;
  output [3:0]gt7_rxcharisk_out;
  output gt7_rxresetdone_out;
  input [4:0]gt7_txpostcursor_in;
  input [4:0]gt7_txprecursor_in;
  input gt7_gttxreset_in;
  input gt7_txuserrdy_in;
  input [3:0]gt7_txchardispmode_in;
  input [3:0]gt7_txchardispval_in;
  input gt7_txusrclk_in;
  input gt7_txusrclk2_in;
  input gt7_txprbsforceerr_in;
  output [1:0]gt7_txbufstatus_out;
  input [3:0]gt7_txdiffctrl_in;
  input [6:0]gt7_txmaincursor_in;
  input [31:0]gt7_txdata_in;
  output gt7_gtxtxn_out;
  output gt7_gtxtxp_out;
  output gt7_txoutclk_out;
  output gt7_txoutclkfabric_out;
  output gt7_txoutclkpcs_out;
  input [3:0]gt7_txcharisk_in;
  input gt7_txpcsreset_in;
  input gt7_txpmareset_in;
  output gt7_txresetdone_out;
  input gt7_txpolarity_in;
  input [2:0]gt7_txprbssel_in;
  output gt8_cpllfbclklost_out;
  output gt8_cplllock_out;
  input gt8_cplllockdetclk_in;
  input gt8_cpllreset_in;
  input gt8_gtrefclk0_in;
  input gt8_gtrefclk1_in;
  input [8:0]gt8_drpaddr_in;
  input gt8_drpclk_in;
  input [15:0]gt8_drpdi_in;
  output [15:0]gt8_drpdo_out;
  input gt8_drpen_in;
  output gt8_drprdy_out;
  input gt8_drpwe_in;
  output [7:0]gt8_dmonitorout_out;
  input [2:0]gt8_loopback_in;
  input [1:0]gt8_rxpd_in;
  input [1:0]gt8_txpd_in;
  input gt8_eyescanreset_in;
  input gt8_rxuserrdy_in;
  output gt8_eyescandataerror_out;
  input gt8_eyescantrigger_in;
  input gt8_rxcdrhold_in;
  input gt8_rxcdrovrden_in;
  input gt8_rxusrclk_in;
  input gt8_rxusrclk2_in;
  output [31:0]gt8_rxdata_out;
  output gt8_rxprbserr_out;
  input [2:0]gt8_rxprbssel_in;
  input gt8_rxprbscntreset_in;
  output [3:0]gt8_rxdisperr_out;
  output [3:0]gt8_rxnotintable_out;
  input gt8_gtxrxp_in;
  input gt8_gtxrxn_in;
  input gt8_rxbufreset_in;
  output [2:0]gt8_rxbufstatus_out;
  output [4:0]gt8_rxphmonitor_out;
  output [4:0]gt8_rxphslipmonitor_out;
  output gt8_rxbyteisaligned_out;
  output gt8_rxbyterealign_out;
  output gt8_rxcommadet_out;
  input gt8_rxmcommaalignen_in;
  input gt8_rxpcommaalignen_in;
  input gt8_rxdfelpmreset_in;
  output [6:0]gt8_rxmonitorout_out;
  input [1:0]gt8_rxmonitorsel_in;
  output gt8_rxoutclk_out;
  output gt8_rxoutclkfabric_out;
  input gt8_gtrxreset_in;
  input gt8_rxpcsreset_in;
  input gt8_rxpmareset_in;
  input gt8_rxlpmen_in;
  input gt8_rxpolarity_in;
  output [3:0]gt8_rxchariscomma_out;
  output [3:0]gt8_rxcharisk_out;
  output gt8_rxresetdone_out;
  input [4:0]gt8_txpostcursor_in;
  input [4:0]gt8_txprecursor_in;
  input gt8_gttxreset_in;
  input gt8_txuserrdy_in;
  input [3:0]gt8_txchardispmode_in;
  input [3:0]gt8_txchardispval_in;
  input gt8_txusrclk_in;
  input gt8_txusrclk2_in;
  input gt8_txprbsforceerr_in;
  output [1:0]gt8_txbufstatus_out;
  input [3:0]gt8_txdiffctrl_in;
  input [6:0]gt8_txmaincursor_in;
  input [31:0]gt8_txdata_in;
  output gt8_gtxtxn_out;
  output gt8_gtxtxp_out;
  output gt8_txoutclk_out;
  output gt8_txoutclkfabric_out;
  output gt8_txoutclkpcs_out;
  input [3:0]gt8_txcharisk_in;
  input gt8_txpcsreset_in;
  input gt8_txpmareset_in;
  output gt8_txresetdone_out;
  input gt8_txpolarity_in;
  input [2:0]gt8_txprbssel_in;
  output gt9_cpllfbclklost_out;
  output gt9_cplllock_out;
  input gt9_cplllockdetclk_in;
  input gt9_cpllreset_in;
  input gt9_gtrefclk0_in;
  input gt9_gtrefclk1_in;
  input [8:0]gt9_drpaddr_in;
  input gt9_drpclk_in;
  input [15:0]gt9_drpdi_in;
  output [15:0]gt9_drpdo_out;
  input gt9_drpen_in;
  output gt9_drprdy_out;
  input gt9_drpwe_in;
  output [7:0]gt9_dmonitorout_out;
  input [2:0]gt9_loopback_in;
  input [1:0]gt9_rxpd_in;
  input [1:0]gt9_txpd_in;
  input gt9_eyescanreset_in;
  input gt9_rxuserrdy_in;
  output gt9_eyescandataerror_out;
  input gt9_eyescantrigger_in;
  input gt9_rxcdrhold_in;
  input gt9_rxcdrovrden_in;
  input gt9_rxusrclk_in;
  input gt9_rxusrclk2_in;
  output [31:0]gt9_rxdata_out;
  output gt9_rxprbserr_out;
  input [2:0]gt9_rxprbssel_in;
  input gt9_rxprbscntreset_in;
  output [3:0]gt9_rxdisperr_out;
  output [3:0]gt9_rxnotintable_out;
  input gt9_gtxrxp_in;
  input gt9_gtxrxn_in;
  input gt9_rxbufreset_in;
  output [2:0]gt9_rxbufstatus_out;
  output [4:0]gt9_rxphmonitor_out;
  output [4:0]gt9_rxphslipmonitor_out;
  output gt9_rxbyteisaligned_out;
  output gt9_rxbyterealign_out;
  output gt9_rxcommadet_out;
  input gt9_rxmcommaalignen_in;
  input gt9_rxpcommaalignen_in;
  input gt9_rxdfelpmreset_in;
  output [6:0]gt9_rxmonitorout_out;
  input [1:0]gt9_rxmonitorsel_in;
  output gt9_rxoutclk_out;
  output gt9_rxoutclkfabric_out;
  input gt9_gtrxreset_in;
  input gt9_rxpcsreset_in;
  input gt9_rxpmareset_in;
  input gt9_rxlpmen_in;
  input gt9_rxpolarity_in;
  output [3:0]gt9_rxchariscomma_out;
  output [3:0]gt9_rxcharisk_out;
  output gt9_rxresetdone_out;
  input [4:0]gt9_txpostcursor_in;
  input [4:0]gt9_txprecursor_in;
  input gt9_gttxreset_in;
  input gt9_txuserrdy_in;
  input [3:0]gt9_txchardispmode_in;
  input [3:0]gt9_txchardispval_in;
  input gt9_txusrclk_in;
  input gt9_txusrclk2_in;
  input gt9_txprbsforceerr_in;
  output [1:0]gt9_txbufstatus_out;
  input [3:0]gt9_txdiffctrl_in;
  input [6:0]gt9_txmaincursor_in;
  input [31:0]gt9_txdata_in;
  output gt9_gtxtxn_out;
  output gt9_gtxtxp_out;
  output gt9_txoutclk_out;
  output gt9_txoutclkfabric_out;
  output gt9_txoutclkpcs_out;
  input [3:0]gt9_txcharisk_in;
  input gt9_txpcsreset_in;
  input gt9_txpmareset_in;
  output gt9_txresetdone_out;
  input gt9_txpolarity_in;
  input [2:0]gt9_txprbssel_in;
  output gt10_cpllfbclklost_out;
  output gt10_cplllock_out;
  input gt10_cplllockdetclk_in;
  input gt10_cpllreset_in;
  input gt10_gtrefclk0_in;
  input gt10_gtrefclk1_in;
  input [8:0]gt10_drpaddr_in;
  input gt10_drpclk_in;
  input [15:0]gt10_drpdi_in;
  output [15:0]gt10_drpdo_out;
  input gt10_drpen_in;
  output gt10_drprdy_out;
  input gt10_drpwe_in;
  output [7:0]gt10_dmonitorout_out;
  input [2:0]gt10_loopback_in;
  input [1:0]gt10_rxpd_in;
  input [1:0]gt10_txpd_in;
  input gt10_eyescanreset_in;
  input gt10_rxuserrdy_in;
  output gt10_eyescandataerror_out;
  input gt10_eyescantrigger_in;
  input gt10_rxcdrhold_in;
  input gt10_rxcdrovrden_in;
  input gt10_rxusrclk_in;
  input gt10_rxusrclk2_in;
  output [31:0]gt10_rxdata_out;
  output gt10_rxprbserr_out;
  input [2:0]gt10_rxprbssel_in;
  input gt10_rxprbscntreset_in;
  output [3:0]gt10_rxdisperr_out;
  output [3:0]gt10_rxnotintable_out;
  input gt10_gtxrxp_in;
  input gt10_gtxrxn_in;
  input gt10_rxbufreset_in;
  output [2:0]gt10_rxbufstatus_out;
  output [4:0]gt10_rxphmonitor_out;
  output [4:0]gt10_rxphslipmonitor_out;
  output gt10_rxbyteisaligned_out;
  output gt10_rxbyterealign_out;
  output gt10_rxcommadet_out;
  input gt10_rxmcommaalignen_in;
  input gt10_rxpcommaalignen_in;
  input gt10_rxdfelpmreset_in;
  output [6:0]gt10_rxmonitorout_out;
  input [1:0]gt10_rxmonitorsel_in;
  output gt10_rxoutclk_out;
  output gt10_rxoutclkfabric_out;
  input gt10_gtrxreset_in;
  input gt10_rxpcsreset_in;
  input gt10_rxpmareset_in;
  input gt10_rxlpmen_in;
  input gt10_rxpolarity_in;
  output [3:0]gt10_rxchariscomma_out;
  output [3:0]gt10_rxcharisk_out;
  output gt10_rxresetdone_out;
  input [4:0]gt10_txpostcursor_in;
  input [4:0]gt10_txprecursor_in;
  input gt10_gttxreset_in;
  input gt10_txuserrdy_in;
  input [3:0]gt10_txchardispmode_in;
  input [3:0]gt10_txchardispval_in;
  input gt10_txusrclk_in;
  input gt10_txusrclk2_in;
  input gt10_txprbsforceerr_in;
  output [1:0]gt10_txbufstatus_out;
  input [3:0]gt10_txdiffctrl_in;
  input [6:0]gt10_txmaincursor_in;
  input [31:0]gt10_txdata_in;
  output gt10_gtxtxn_out;
  output gt10_gtxtxp_out;
  output gt10_txoutclk_out;
  output gt10_txoutclkfabric_out;
  output gt10_txoutclkpcs_out;
  input [3:0]gt10_txcharisk_in;
  input gt10_txpcsreset_in;
  input gt10_txpmareset_in;
  output gt10_txresetdone_out;
  input gt10_txpolarity_in;
  input [2:0]gt10_txprbssel_in;
  output gt11_cpllfbclklost_out;
  output gt11_cplllock_out;
  input gt11_cplllockdetclk_in;
  input gt11_cpllreset_in;
  input gt11_gtrefclk0_in;
  input gt11_gtrefclk1_in;
  input [8:0]gt11_drpaddr_in;
  input gt11_drpclk_in;
  input [15:0]gt11_drpdi_in;
  output [15:0]gt11_drpdo_out;
  input gt11_drpen_in;
  output gt11_drprdy_out;
  input gt11_drpwe_in;
  output [7:0]gt11_dmonitorout_out;
  input [2:0]gt11_loopback_in;
  input [1:0]gt11_rxpd_in;
  input [1:0]gt11_txpd_in;
  input gt11_eyescanreset_in;
  input gt11_rxuserrdy_in;
  output gt11_eyescandataerror_out;
  input gt11_eyescantrigger_in;
  input gt11_rxcdrhold_in;
  input gt11_rxcdrovrden_in;
  input gt11_rxusrclk_in;
  input gt11_rxusrclk2_in;
  output [31:0]gt11_rxdata_out;
  output gt11_rxprbserr_out;
  input [2:0]gt11_rxprbssel_in;
  input gt11_rxprbscntreset_in;
  output [3:0]gt11_rxdisperr_out;
  output [3:0]gt11_rxnotintable_out;
  input gt11_gtxrxp_in;
  input gt11_gtxrxn_in;
  input gt11_rxbufreset_in;
  output [2:0]gt11_rxbufstatus_out;
  output [4:0]gt11_rxphmonitor_out;
  output [4:0]gt11_rxphslipmonitor_out;
  output gt11_rxbyteisaligned_out;
  output gt11_rxbyterealign_out;
  output gt11_rxcommadet_out;
  input gt11_rxmcommaalignen_in;
  input gt11_rxpcommaalignen_in;
  input gt11_rxdfelpmreset_in;
  output [6:0]gt11_rxmonitorout_out;
  input [1:0]gt11_rxmonitorsel_in;
  output gt11_rxoutclk_out;
  output gt11_rxoutclkfabric_out;
  input gt11_gtrxreset_in;
  input gt11_rxpcsreset_in;
  input gt11_rxpmareset_in;
  input gt11_rxlpmen_in;
  input gt11_rxpolarity_in;
  output [3:0]gt11_rxchariscomma_out;
  output [3:0]gt11_rxcharisk_out;
  output gt11_rxresetdone_out;
  input [4:0]gt11_txpostcursor_in;
  input [4:0]gt11_txprecursor_in;
  input gt11_gttxreset_in;
  input gt11_txuserrdy_in;
  input [3:0]gt11_txchardispmode_in;
  input [3:0]gt11_txchardispval_in;
  input gt11_txusrclk_in;
  input gt11_txusrclk2_in;
  input gt11_txprbsforceerr_in;
  output [1:0]gt11_txbufstatus_out;
  input [3:0]gt11_txdiffctrl_in;
  input [6:0]gt11_txmaincursor_in;
  input [31:0]gt11_txdata_in;
  output gt11_gtxtxn_out;
  output gt11_gtxtxp_out;
  output gt11_txoutclk_out;
  output gt11_txoutclkfabric_out;
  output gt11_txoutclkpcs_out;
  input [3:0]gt11_txcharisk_in;
  input gt11_txpcsreset_in;
  input gt11_txpmareset_in;
  output gt11_txresetdone_out;
  input gt11_txpolarity_in;
  input [2:0]gt11_txprbssel_in;
  input gt0_qplloutclk_in;
  input gt0_qplloutrefclk_in;
  input gt1_qplloutclk_in;
  input gt1_qplloutrefclk_in;
  input gt2_qplloutclk_in;
  input gt2_qplloutrefclk_in;
endmodule
